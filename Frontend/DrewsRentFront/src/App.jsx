import React, { useState, useEffect } from "react";
import "./App.css";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";

import Login from "./components/Login";
import SideBarMenu from "./components/SideBarMenu";
import Dashboard from "./pages/Dashboard/Dashboard";
import Monitoreo from "./pages/Monitoreo/Monitoreo";
import MonitoreoVehDetalle from "./pages/Monitoreo/MonitoreoVehDetalle.jsx";
import Tareo from "./pages/Tareos/Tareos";
import Alquileres from "./pages/Alquileres/Alquileres";
import AlquilerDetalle from "./pages/Alquileres/AlquilerDetalle.jsx";
import AlquilerEdit from "./pages/Alquileres/AlquilerEdit.jsx";
import Valorizaciones from "./pages/Valorizaciones/Valorizaciones";
import ValorizacionDetalle from "./pages/Valorizaciones/ValorizacionesDetalle.jsx";
import PagoProveedores from "./pages/PagoProveedores/PagosProveedores.jsx";
import FormCreatePagPro from "./pages/PagoProveedores/FormCreatePagPro.jsx";
import FormEditPagPro from "./pages/PagoProveedores/FormEditPagPro.jsx";
import PagoProveedorDetalle from "./pages/PagoProveedores/PagoProveedorDetalle.jsx";
import PagosOperadores from "./pages/PagoOperadores/PagosOperadores.jsx";
import PagoOperadorDetalle from "./pages/PagoOperadores/PagoOperadorDetalle.jsx";
import { FormCreatePagOpe } from "./pages/PagoOperadores/FormCreatePagOpe.jsx";
import { FormEditPagOpe } from "./pages/PagoOperadores/FormEditPagOpe.jsx";
import Proyectos from "./pages/Proyectos/Proyectos.jsx";
import ProyectoDetalle from "./pages/Proyectos/ProyectoDetalle.jsx";
import Empresas from "./pages/Empresas/Empresas.jsx";
import EmpresaDetalle from "./pages/Empresas/EmpresaDetalle.jsx";
import Areas from "./pages/Areas/Areas.jsx";
import Responsables from "./pages/Responsables/Responsables.jsx";
import ResponsableDetalle from "./pages/Responsables/ResponsableDetalle.jsx";
import Vehiculos from "./pages/Vehiculos/Vehiculos.jsx";
import Operadores from "./pages/Operadores/Operadores.jsx";
import OperadorDetalles from "./pages/Operadores/OperadorDetalles.jsx";
import Proveedores from "./pages/Proveedores/Proveedores.jsx";
import ProveedoresDetalle from "./pages/Proveedores/ProveedoresDetalle.jsx";
import Header from "./components/Header";
import { FormCreateVehiculo } from "./pages/Vehiculos/FormCreateVehiculo.jsx";
import { FormEditVehiculo } from "./pages/Vehiculos/FormEditVehiculo.jsx";

import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { ExportTareos } from "./pages/Tareos/ExportTareos.jsx";
import VehiculoDetalle from "./pages/Vehiculos/VehiculoDetalle.jsx";
import { useAuthStore } from "./store/auth";
import PrivateRoute from "./layouts/PrivateRoute";

function App() {
  const [isLoggedIn] = useAuthStore((state) => [state.isLoggedIn]);

  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route
            path="/*"
            element={
              isLoggedIn ? (
                <div className="app">
                  <SideBarMenu isLoggedIn={isLoggedIn}>
                    <Header />
                    <Routes>
                      {/*<Route path='/' element={<Monitoreo />} />*/}
                      <Route
                        path="/"
                        element={
                          <PrivateRoute>
                            <Dashboard />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/tareos"
                        element={
                          <PrivateRoute>
                            <Tareo />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/tareos/exportar"
                        element={
                          <PrivateRoute>
                            <ExportTareos />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/alquileres"
                        element={
                          <PrivateRoute>
                            <Alquileres />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/alquileres/:id"
                        element={
                          <PrivateRoute>
                            <AlquilerDetalle />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/alquileres/:id/edit"
                        element={
                          <PrivateRoute>
                            <AlquilerEdit />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/valorizaciones"
                        element={
                          <PrivateRoute>
                            <Valorizaciones />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/valorizaciones/:id"
                        element={
                          <PrivateRoute>
                            <ValorizacionDetalle />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/pagosProveedores"
                        element={
                          <PrivateRoute>
                            <PagoProveedores />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/pagosProveedores/create"
                        element={
                          <PrivateRoute>
                            <FormCreatePagPro />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/pagosProveedores/:id"
                        element={
                          <PrivateRoute>
                            <PagoProveedorDetalle />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/pagosProveedores/:id/update"
                        element={
                          <PrivateRoute>
                            <FormEditPagPro />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/pagosOperadores"
                        element={
                          <PrivateRoute>
                            <PagosOperadores />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/pagosOperadores/:id"
                        element={
                          <PrivateRoute>
                            <PagoOperadorDetalle />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/pagosOperadores/:id/edit"
                        element={
                          <PrivateRoute>
                            <FormEditPagOpe />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/pagosOperadores/create"
                        element={
                          <PrivateRoute>
                            <FormCreatePagOpe />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/proyectos"
                        element={
                          <PrivateRoute>
                            <Proyectos />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/proyectos/:id"
                        element={
                          <PrivateRoute>
                            <ProyectoDetalle />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/empresas"
                        element={
                          <PrivateRoute>
                            <Empresas />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/empresas/:id"
                        element={
                          <PrivateRoute>
                            <EmpresaDetalle />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/areas"
                        element={
                          <PrivateRoute>
                            <Areas />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/responsables"
                        element={
                          <PrivateRoute>
                            <Responsables />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/responsables/:id"
                        element={
                          <PrivateRoute>
                            <ResponsableDetalle />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/vehiculos"
                        element={
                          <PrivateRoute>
                            <Vehiculos />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/vehiculos/:id"
                        element={
                          <PrivateRoute>
                            <VehiculoDetalle />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/vehiculos/create"
                        element={
                          <PrivateRoute>
                            <FormCreateVehiculo />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/vehiculos/:id/edit"
                        element={
                          <PrivateRoute>
                            <FormEditVehiculo />
                          </PrivateRoute>
                        }
                      />

                      <Route
                        path="/operadores"
                        element={
                          <PrivateRoute>
                            <Operadores />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/operadores/:id"
                        element={
                          <PrivateRoute>
                            <OperadorDetalles />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/proveedores"
                        element={
                          <PrivateRoute>
                            <Proveedores />
                          </PrivateRoute>
                        }
                      />
                      <Route
                        path="/proveedores/:id"
                        element={
                          <PrivateRoute>
                            <ProveedoresDetalle />
                          </PrivateRoute>
                        }
                      />
                    </Routes>
                    <ToastContainer />
                  </SideBarMenu>
                </div>
              ) : (
                <Navigate to="/login" />
              )
            }
          />
        </Routes>
      </BrowserRouter>
    </>
  );
}
export default App;

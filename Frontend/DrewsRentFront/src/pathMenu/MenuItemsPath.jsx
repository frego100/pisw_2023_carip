const menuItems = () => [
    {
        path:"/",
        name:"Dashboard",
        iconWhite: "/src/assets/WhiteIcons/ChartIcon.png",
    },
    {
        path:"/tareos",
        name:"Tareos",
        iconWhite: "/src/assets/WhiteIcons/TareoIcon.png",
    },
    {
        path:"/alquileres",
        name:"Alquileres",
        iconWhite: "/src/assets/WhiteIcons/AlquilerIcon.png",
    },
    {
        path:"/valorizaciones",
        name:"Valorizaciones",
        iconWhite: "/src/assets/WhiteIcons/ValorizacionIcon.png",
    },
    {
        path:"/pagosProveedores",
        name:"Pagos proveedores",
        iconWhite: "/src/assets/WhiteIcons/PagoIcon.png",
    },  
    {
        path:"/pagosOperadores",
        name:"Pagos operadores",
        iconWhite: "/src/assets/WhiteIcons/Pago2Icon.png",
    },
    {
        path:"/proyectos",
        name:"Proyectos",
        iconWhite: "/src/assets/WhiteIcons/ProjectIcon.png",
    },
    {
        path:"/empresas",
        name:"Empresas",
        iconWhite: "/src/assets/WhiteIcons/CompanyIcon.png",
    },
    {
        path:"/areas",
        name:"Areas",
        iconWhite: "/src/assets/WhiteIcons/AreaIcon.png",
    },
    {
        path:"/responsables",
        name:"Responsables",
        iconWhite: "/src/assets/WhiteIcons/ManagersIcon.png",
    },
    {
        path:"/vehiculos",
        name:"Vehiculos",
        iconWhite: "/src/assets/WhiteIcons/CarsIcon.png",
    },
    {
        path:"/operadores",
        name:"Operadores",
        iconWhite: "/src/assets/WhiteIcons/OperatorIcon.png",
    },
    {
        path:"/proveedores",
        name:"Proveedores",
        iconWhite: "/src/assets/WhiteIcons/ProveedorIcon.png",
    }, 
    
];

export default menuItems;
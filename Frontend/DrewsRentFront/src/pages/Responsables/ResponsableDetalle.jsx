import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getResponsableById } from '../../api/Responsables.api';
import { toast } from 'react-toastify';
import { BsBuildingFill, BsPersonFill } from "react-icons/bs";


function ResponsableDetalle() {

  const params = useParams();
  /*Datos del alquiler*/ 
  const [responsable, setResponsable] = useState([]);

  const navigate = useNavigate();

  const handleRegresar = () => {
    navigate('/responsables');
  };

  useEffect(() => {
    const loadResponsable = async () => {
      try {
        const response = await getResponsableById(params.id);
        setResponsable(response.data);
      } catch (error) {
        toast.error(`Error al cargar al responsable: ${error.message}`);
      }
  };
  loadResponsable();
  }, []);

  return (
    <div className='contenedor-componente'>
      <h2>Responsable: {responsable.resnom} {responsable.resape}</h2>
      <div className='contenedor-detalle'>
        <div className='detalle-subtitle'>
          <BsPersonFill color='green' className=''/>
          <h5>Datos del responsable:</h5>
        </div>        
        <div className='detalle-datos'>
          <h6>Nombre</h6>
          <p> {responsable.resnom}</p>
          <h6>Apellido</h6>
          <p> {responsable.resape}</p>
          <h6>Cargo</h6>
          <p> {responsable.rescarcod?.carnom}</p>
          <h6>Email</h6>
          <p> {responsable.resema} </p>
          <h6>Telefono</h6>
          <p> {responsable.restel}</p>
        </div>
        <div className='detalle-subtitle'>
          <BsBuildingFill color='green' className=''/>
          <h5>Datos de la empresa a la que pertenece:</h5>
        </div>
        <div className='detalle-datos'>
          <h6>Area</h6>
          <p> {responsable.resarecod?.arenom}</p>
          <h6>Empresa</h6>
          <p> {responsable.resempcod?.empnom}</p>
          <h6>Razon Social</h6>
          <p> {responsable.resempcod?.emprazsoc}</p>
          <h6>RUC</h6>
          <p> {responsable.resempcod?.empruc}</p>
          <h6>Region</h6>
          <p> {responsable.resempcod?.empubicod.disprocod.proregcod.regnom}  -  {responsable.resempcod?.empubicod.disprocod.proregcod.regnomacro} </p>
          <h6>Provincia</h6>
          <p> {responsable.resempcod?.empubicod.disprocod.pronom}</p>
          <h6>Distrito</h6>
          <p> {responsable.resempcod?.empubicod.disnom}</p> 
          <h6>Direccion</h6>
          <p> {responsable.resempcod?.empdir}</p>         
        </div>        
      </div>
      <div className='contenedor-btn-detalle'>
        <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
      </div>      
    </div>
  );
}

export default ResponsableDetalle;

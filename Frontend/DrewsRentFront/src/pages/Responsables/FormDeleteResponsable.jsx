import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getResponsable, updateResponsable } from '../../api/Responsables.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';


function FormDeleteResponsable({ toggle, responsableId, loadResponsables }) {

    /*Datos del proyecto seleccionado*/ 
    const [responsable, setResponsable] = useState([]);
    
    /*Datos de proyectos para la tabla*/   
    const [estReg, setEstReg] = useState([]);

    /*Estado de los registros de mi formulario*/
    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    const onSubmit = handleSubmit(async (data) => {
        try {
            const res = await updateResponsable(responsable.rescod, data);
            toggle();
            loadResponsables();
            toast.success("Responsable eliminado con exito");
        } catch (error) {
            toast.error(`Error al eliminar el responsable: ${error.message}`);
        }
    });

    useEffect(() => {
        async function loadResponsable() {
            try {
                const res = await getResponsable(responsableId);
                setResponsable(res.data);
                setValue('resnom', res.data.resnom);
                setValue('resape', res.data.resape);
                setValue('resema', res.data.resema); 
                setValue('restel', res.data.restel);
                setValue('resarecod', res.data.resarecod.arecod); 
                setValue('rescarcod', res.data.rescarcod.carcod);
                setValue('resempcod', res.data.resempcod.empcod);
            } catch (error) {
                toast.error(`Error al cargar los responsables: ${error.message}`);
            }
        }
        async function loadEstReg() {
            try {
                const res = await getEstadoRegistro('Inactivo');
                if (res === null) {
                    console.log('El estado de registro no existe. AGREGAR ESTADO DE REGISTRO A LA BASE DE DATOS');
                } else {
                    setValue('resestreg', res);
                    setEstReg(res);
                }
            } catch (error) {
                toast.error(`Error al cargar los estados: ${error.message}`);
            }
        }
        loadResponsable();
        loadEstReg();
    }, [responsableId]
    );

    return (
        <>
        <h3>¿Está seguro que quiere eliminar el responsable "{responsable.resnom}"?</h3>
        <form onSubmit={onSubmit}>
            {/* Campo de selección para la región del proyecto */}
            <label>
                Estado de proyecto
                <select className='input-select' {...register('resestreg', { required: true })}>
                    <option value={estReg} >Inactivo</option>
                </select>
                {errors.resestreg?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
            </label>
            {/* Botones de cancelar/eliminar */}
            <div className='contenedor-btn'>
                <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
                <button className='btn-eliminar' type='submit'>Eliminar</button>
            </div>
        </form>
        </>
    );
}

export { FormDeleteResponsable };
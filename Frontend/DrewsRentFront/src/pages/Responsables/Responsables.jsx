import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllResponsablesActivos } from '../../api/Responsables.api';
import responsableColumns from '../../Utils/ComponentesUtils/responsableColumns';
import  Modal  from '../../components/Modal';
import { Table } from '../../components/Tabla/Table';
import { FormCreateResponsable } from './FormCreateResponsable';
import { FormEditResponsable } from './FormEditResponsable';
import { FormDeleteResponsable } from './FormDeleteResponsable';
import '../../styles/ContenedorComp.css';
import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from 'react-toastify';

function Responsables() {

  /*Datos de responsable para la tabla*/    
  const [responsables, setResponsables] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);
  
  /*Estado del idProyectoSeleccionado */
  const [selectedIdResponsable, setSelectedIdResponsable] = useState();
  
  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  const handleEdit = (idResponsable) => {
    setSelectedIdResponsable(idResponsable);
    setActiveFormEdit(!activeFormEdit);
  };
  const handleDelete = (idResponsable) => {
    setSelectedIdResponsable(idResponsable);
    setActiveFormDelete(!activeFormDelete);
  };
  
  const loadResponsables = async () => {
    try {
      const res = await getAllResponsablesActivos();
      setResponsables(res);
    } catch (error) {
      toast.error(`Error al cargar los responsables`);
    }
  };

  useEffect(() => {
    loadResponsables();
  }, []);


  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Responsables</h2>
        <div className='btn-agregar' onClick={toggle}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de responsables */}
      <Table
        nombreID={'rescod'}
        columns={responsableColumns()}
        data={responsables}
        nombre={'responsables'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={true}
      />         
    </div>

    <ToastContainer />

    {/* Modal para crear nuevo responsable */}
    <Modal active={activeFormCreate} toggle={toggle}>
      <FormCreateResponsable toggle={toggle} loadResponsables={loadResponsables}/>      
    </Modal>
    
    {/* Modal para editar responsable existente */}
    <Modal active={activeFormEdit} toggle={handleEdit}>
      <FormEditResponsable toggle={handleEdit} responsableId={selectedIdResponsable} loadResponsables={loadResponsables} />      
    </Modal>

    {/* Modal para eliminar responsable */}
    <Modal active={activeFormDelete} toggle={handleDelete}>
      <FormDeleteResponsable toggle={handleDelete} responsableId={selectedIdResponsable} loadResponsables={loadResponsables} /> 
    </Modal>
    </>
  )
}

export default Responsables
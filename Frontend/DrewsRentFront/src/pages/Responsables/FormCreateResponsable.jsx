import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createResponsable } from '../../api/Responsables.api';
import { getAllAreas } from '../../api/Area.api';
import { getAllCargos } from '../../api/Cargo.api';
import { getAllEmpresas } from '../../api/Empresa.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';


function FormCreateResponsable({toggle, loadResponsables}) {

  /*Datos de regiones*/ 
  const [areas, setAreas] = useState([]);
  const [cargos, setCargos] = useState([]);
  const [empresas, setEmpresas] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,  
    setValue,
    formState: { errors },
  } = useForm();

  const navigate = useNavigate();

  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

  // Función de validación de telefono
  const validateTelefono = (value) => {
    return /^\d{9}$/.test(value);
  };

  // Función llamada al enviar el formulario
  const onSubmit = handleSubmit(async data => {
    try {
      const res = await createResponsable(data);
      toggle();
      loadResponsables();
      toast.success('Responsable creado con éxito');
    } catch (error) {
      toast.error(`Error al crear al Responsable`);
    }
  });

  const loadEstReg = async () => {
    try {
      const resid = await getEstadoRegistro('Activo');
      if (resid === null) {
        console.log('El estado de registro no existe. AGREGAR ESTADO DE REGISTRO A LA BASE DE DATOS');
      } else {
        setValue('resestreg', resid);
      }
    } catch (error) {
      toast.error(`Error al cargar los estados`);
    }
  };
  const loadAreas = async () => {
    try {
      const res = await getAllAreas();
      setAreas(res);
    } catch (error) {
      toast.error(`Error al cargar las areas`);
    }
  };
  const loadCargos = async () => {
    try {
      const res = await getAllCargos();
      setCargos(res);
    } catch (error) {
      toast.error(`Error al cargar los cargos`);
    }
  };
  const loadEmpresas = async () => {
    try {
      const res = await getAllEmpresas();
      setEmpresas(res);
    } catch (error) {
      toast.error(`Error al cargar las empresas`);
    }
  };

  // Cargar las regiones
  useEffect(() => {
    loadEstReg();
    loadAreas();
    loadCargos();
    loadEmpresas();
  }, []
  );

  // Renderizar
  return (
    <>
    <h2>Registrar nuevo responsable</h2>
    <form onSubmit={onSubmit}>
      <label>
        Nombres
        <input
          className='input-text'
          type="text"
          placeholder='Nombres del Responsable'
          {...register('resnom', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.resnom?.type === 'required' && <p className='text-error'>*El campo nombre es requerido</p>}
        {errors.resnom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}  
      </label>
      <label>
        Apellidos
        <input
          className='input-text'
          type="text"
          placeholder='Apellidos del Responsable'
          {...register('resape', { 
            required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.resape?.type === 'required' && <p className='text-error'>*El campo apellido es requerido</p>}
        {errors.resape?.type === 'validate' && <p className='text-error'>*El campo apellido no debe superar los 45 caracteres</p>}
      </label>
      <label>
        Correo
        <input
          className='input-text'
          type="text"
          placeholder='Correo del Responsable'
          {...register('resema', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.resema?.type === 'required' && <p className='text-error'>*El campo correo es requerido</p>}
        {errors.resema?.type === 'validate' && <p className='text-error'>*El campo correo no debe superar los 45 caracteres</p>}
      </label>
      <label>
        Teléfono
        <input
          className='input-text'
          type="number"
          placeholder='Telefono del Responsable'
          {...register('restel', { required: true,
            validate: (value) => validateTelefono(value),
          })}
        />
        {errors.restel?.type === 'required' && <p className='text-error'>*El campo telefono es requerido</p>}
        {errors.restel?.type === 'validate' && <p className='text-error'>*El campo telefono debe ser numérico y de 9 caracteres</p>}  
      </label>
      <div className='form-fila'>
        <label className='flex1'>
          Empresa
          <select className='input-select'
            name="empresas"
            {...register('resempcod', { required: true })}
          >
            <option value='' >Seleccionar Empresa</option>
            {empresas && empresas.map(empresa =>(
              <option value={empresa.empcod} key={empresa.empcod}>{empresa.empnom}</option>
            ))}
          </select>
          {errors.resempcod?.type === 'required' && <p className='text-error'>*El campo empresa es requerido</p>}  
        </label>
        <label className='flex1'>
          Area
          <select className='input-select'
            name="areas"
            {...register('resarecod', { required: true })}
          >
            <option value='' >Seleccionar Area</option>
            {areas && areas.map(area =>(
                <option value={area.arecod} key={area.arecod}>{area.arenom}</option>
            ))}
          </select>
          {errors.resarecod?.type === 'required' && <p className='text-error'>*El campo area es requerido</p>}  
        </label>
      </div>
      <label>
        Cargo
        <select className='input-select'
          name="cargos"
          {...register('rescarcod', { required: true })}
        >
          <option value='' >Seleccionar Cargo</option>
          {cargos && cargos.map(cargo =>(
            <option value={cargo.carcod} key={cargo.carcod}>{cargo.carnom}</option>
          ))}
        </select>
        {errors.rescarcod?.type === 'required' && <p className='text-error'>*El campo cargo es requerido</p>}  
      </label>
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-registrar' type='submit'>Registrar</button>
      </div>
    </form>
    </>
  )
}

export { FormCreateResponsable };
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getResponsable, updateResponsable } from '../../api/Responsables.api';
import { getAllAreas } from '../../api/Area.api';
import { getAllCargos } from '../../api/Cargo.api';
import { getAllEmpresas } from '../../api/Empresa.api';
import { toast } from 'react-toastify';

function FormEditResponsable({ toggle, responsableId, loadResponsables }) {

    /*Datos del proyecto seleccionado*/ 
    const [responsable, setResponsable] = useState([]);
    
    /*Datos de cargos*/ 
    const [areas, setAreas] = useState([]);
    const [cargos, setCargos] = useState([]);
    const [empresas, setEmpresas] = useState([]);

    /*Estado de los registros de mi formulario*/
    const {
        register,
        handleSubmit,   
        setValue,
        formState: { errors },
    } = useForm();

    const validateMaxSize = (value) => {
        return value.length <= 45;
    };

    // Función de validación para RUC (solo números y tamaño 11)
    const validateTelefono = (value) => {
        return /^\d{9}$/.test(value);
    };

    const onSubmit = handleSubmit(async data => {
        try {
            const res = await updateResponsable(responsable.rescod, data);
            toggle();
            loadResponsables();
            toast.success('Responsable editado con éxito');
        } catch (error) {
            toast.error(`Error al editar responsable: ${error.message}`);
        }
    });

    useEffect(() => {
        async function loadResponsable() {
            try {
                const res = await getResponsable(responsableId);
                setResponsable(res.data)
                setValue('resnom', res.data.resnom);
                setValue('resape', res.data.resape);
                setValue('resema', res.data.resema); 
                setValue('restel', res.data.restel);
                setValue('resarecod', res.data.resarecod.arecod); 
                setValue('rescarcod', res.data.rescarcod.carcod);
                setValue('resempcod', res.data.resempcod.empcod);
                setValue('resestreg', res.data.resestreg.estregcod);
            } catch (error) {
                toast.error(`Error al cargar los responsables: ${error.message}`);
            }
        }
        async function loadAreas() {
            try {
                const res = await getAllAreas();
                setAreas(res);
            } catch (error) {
                toast.error(`Error al cargar las areas: ${error.message}`);
            }
        }
        async function loadCargos() {
            try {
                const res = await getAllCargos();
                setCargos(res);
            } catch (error) {
                toast.error(`Error al cargar los cargos: ${error.message}`);
            }
        }
        async function loadEmpresas() {
            const res = await getAllEmpresas();
            setEmpresas(res);
        }
        loadResponsable();
        loadAreas();
        loadCargos();
        loadEmpresas();
    }, [responsableId]
    );


    return (
        <>
        <h2>Editar Reponsable "{responsable?.resnom}" </h2>
        <form onSubmit={onSubmit}>
            {/* Campo de entrada para el nombre del proyecto */}
            <label>
                Nombre
                <input
                    className='input-text'
                    type="text"
                    placeholder='Nombre del proyecto'
                    {...register('resnom', { required: true,
                        validate: (value) => validateMaxSize(value),
                     })}
                />
                {errors.resnom?.type === 'required' && (<p className='text-error'>*El campo nombre es requerido</p>)}
                {errors.resnom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}  
            </label>
            <label>
                Apellidos
                <input
                    className='input-text'
                    type="text"
                    placeholder='Apellidos del Responsable'
                    {...register('resape', { required: true,
                        validate: (value) => validateMaxSize(value),
                     })}
                />
                {errors.resape?.type === 'required' && <p className='text-error'>*El campo apellido es requerido</p>}
                {errors.resape?.type === 'validate' && <p className='text-error'>*El campo apellido no debe superar los 45 caracteres</p>}    
            </label>
            <label>
                Correo
                <input
                    className='input-text'
                    type="text"
                    placeholder='Correo del Responsable'
                    {...register('resema', { required: true,
                        validate: (value) => validateMaxSize(value),
                     })}
                />
                {errors.resema?.type === 'required' && <p className='text-error'>*El campo correo es requerido</p>}
                {errors.resema?.type === 'validate' && <p className='text-error'>*El campo correo no debe superar los 45 caracteres</p>}    
            </label>
            <label>
                Telefono
                <input
                    className='input-text'
                    type="number"
                    placeholder='Telefono del Responsable'
                    {...register('restel', { required: true,
                        validate: (value) => validateTelefono(value),
                     })}
                />
                {errors.restel?.type === 'required' && <p className='text-error'>*El campo telefono es requerido</p>}
                {errors.restel?.type === 'validate' && <p className='text-error'>*El telefono solo debe ser numérico y tener exactamente 9 caracteres</p>}    
            </label>
            <label>
                Area
                <select className='input-select'
                    name="cargos"
                    {...register('resarecod', { required: true })}
                    >
                    {areas && areas.map(area =>(
                        <option value={area.arecod} key={area.arecod}>{area.arenom}</option>
                    ))}
                </select>
                {errors.resarecod?.type === 'required' && <p className='text-error'>*El campo area es requerido</p>}  
            </label>
            <label>
                Cargo
                <select className='input-select'
                    name="cargos"
                    {...register('rescarcod', { required: true })}
                    >
                    {cargos && cargos.map(cargo =>(
                        <option value={cargo.carcod} key={cargo.carcod}>{cargo.carnom}</option>
                    ))}
                </select>
                {errors.rescarcod?.type === 'required' && <p className='text-error'>*El campo cargo es requerido</p>}  
            </label>
            <label>
                Empresa
                <select className='input-select'
                    name="empresas"
                    {...register('resempcod', { required: true })}
                    >
                    {empresas && empresas.map(empresa =>(
                        <option value={empresa.empcod} key={empresa.empcod}>{empresa.empnom}</option>
                    ))}
                </select>
                {errors.resempcod?.type === 'required' && <p className='text-error'>*El campo empresa es requerido</p>}  
            </label>          
            {/* Botones de cancelar/guardar */}
            <div className='contenedor-btn'>
                <button className='btn-cancelar' type='button' onClick={ toggle }>Cancelar</button>
                <button className='btn-guardar' type='submit'>Guardar</button>
            </div>
        </form>
        </>
    );
}

export { FormEditResponsable };
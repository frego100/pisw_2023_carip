import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllValorizaciones } from '../../api/Valorizacion.api';
import valorizarColumns from '../../Utils/ComponentesUtils/valorizacionColumns';
import  Modal  from '../../components/Modal';
import { Table } from '../../components/Tabla/Table';
import { FormCreateValorizaciones } from './FormCreateValorizaciones';
import { FormEditValorizaciones } from './FormEditValorizaciones';
import { FormDeleteValorizaciones } from './FormDeleteValorizaciones';

import '../../styles/ContenedorComp.css'
import '../../styles/Valorizaciones.css'

function Valorizaciones() {

  /*Datos de Valorizaciones para la tabla*/    
  const [valorizaciones, setValorizaciones] = useState([]); 

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);

  /*Estado del idValorizacionSeleccionado */
  const [selectedIdValorizacion, setSelectedIdValorizacion] = useState();

  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  const handleEdit = (idProyecto) => {
    setSelectedIdValorizacion(idProyecto);
    setActiveFormEdit(!activeFormEdit);
  };
  const handleDelete = (idProyecto) => {
    setSelectedIdValorizacion(idProyecto);
    setActiveFormDelete(!activeFormDelete);
    console.log(idProyecto);
  };
  
  const loadValorizaciones = async () => {
    try {
      const res = await getAllValorizaciones();
      setValorizaciones(res); //res.filter((valorizacion) => valorizacion.valestvalcod.estregval === 'Activo')
    } catch (error) {
        console.error('Error al cargar la Valorizacion:', error);
    }
  };

  useEffect(() => {
    loadValorizaciones();
  }, []);

  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Valorizaciones</h2>
        <div className='btn-agregar' onClick={toggle}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de proyectos */}
      <Table
        nombreID={'valcod'}
        columns={valorizarColumns()}
        data={valorizaciones}
        nombre={'valorizaciones'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={true}
      />
    </div>

    {/* Modal para crear nueva Valorizacion */}
    <Modal active={activeFormCreate} toggle={toggle}>
      <FormCreateValorizaciones toggle={toggle} loadValorizaciones={loadValorizaciones}/>      
    </Modal>
    
    {/* Modal para editar valorizacion existente */}
    <Modal active={activeFormEdit} toggle={handleEdit}>
      <FormEditValorizaciones toggle={handleEdit} valorizacionId={selectedIdValorizacion} loadValorizaciones={loadValorizaciones} />
    </Modal>

    {/* Modal para eliminar valoriazcion */}
    <Modal active={activeFormDelete} toggle={handleDelete}>
      <FormDeleteValorizaciones toggle={handleDelete} valorizacionId={selectedIdValorizacion} loadValorizaciones={loadValorizaciones} /> 
    </Modal>
    </>
  )
}

export default Valorizaciones
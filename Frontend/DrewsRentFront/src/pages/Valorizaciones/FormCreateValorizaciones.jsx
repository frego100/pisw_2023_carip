import React, { useEffect, useState } from 'react';
import { useForm, useController, useFieldArray, useWatch } from 'react-hook-form';
import { createValorizacion } from '../../api/Valorizacion.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { createValorizacionItem} from '../../api/ValorizacionItem.api';
import { getAllResponsables, getResponsableById } from '../../api/Responsables.api';
import { getAllEstadoValorizaciones } from '../../api/ValorizacionPorEstado.api';
import { getAllEmpresas,getEmpresaById } from '../../api/Empresa.api';
import { getAllProyectos } from '../../api/Proyecto.api'
import { getAllAlquileres } from '../../api/Alquiler.api'
import { getByIdConteoTareoAlquiler } from '../../api/ConteoLeyendaAlquiler.api';
import { BsFillTrashFill } from "react-icons/bs";
import { FaCirclePlus } from "react-icons/fa6";

import Select from 'react-select';
import { zodResolver } from '@hookform/resolvers/zod';
import { z } from 'zod';

const schema = z.object({
    valrescod: z.number({required_error: "Campo Responsable es requerido"}).int(),
    valestvalcod: z.number({required_error: "Campo EstadoValorizacion es requerido"}).int(),
    valcli: z.string().optional(),
    valruc: z.string().length(11,{message:'RUC debe contener solo 11 caracteres'}),
    valdir: z.string().max(45, {message: 'Maximo 45 caracteres'}),
    valtel: z.string().max(14),
    valema: z.string().email().optional(),
    valpro: z.coerce.number(),
    valfec: z.string(), //with date, it'll be date obj
    valforpag: z.string(),
    valmon: z.string(),
    valfac: z.boolean(),
    valvenval: z.coerce.number(),
    valotrval: z.coerce.number(),
    valigvval: z.coerce.number(),
    valpreven: z.coerce.number(),
    initdate: z.string(),
    enddate:z.string(),
    valitems: z.array(
        z.object({
            // valalqcod: z.number(),
            valitedes: z.string().max(255), // Adjust the validation as needed
            valitecan: z.number(),
            valitepreuni: z.number(),
            valitepretol: z.number(),
        })
    ),
});


function getPreTotalOpe(payload){
    console.log(payload);
    return null;
}

// <GetPreTotal control={control} />

function GetPreTotal({control}){
    const valorizacion_values = useWatch({
        control,
        name:'valigvval'
    });
    console.log(valorizacion_values);
    return getPreTotalOpe(valorizacion_values);
}
function FormCreateValorizaciones({toggle, loadValorizaciones}) {
    const [responsables, setResponsables]  = useState([]);
    const [estadoValorizaciones, setEstadoValorizaciones] = useState([]);
    const [empresas,  setEmpresas ] = useState([]);
    const [proyectos, setProyectos] = useState([]);
    const [alquileres, setAlquileres] = useState([]);
    const [empresa, setEmpresa] = useState([]);
    const [responsable, setResponsable] = useState([]);

    // const [tableData, setTableData] = useState([{
    //     item:'0',
    //     description: 'Ninguna',
    //     amount:0,
    //     preunit:0,
    //     total:0,
    // }]);
    const valorizacionInit = {
        'valrescod':'',
        'valestvalcod': '',
        'valcli':'',
        'valruc': '',
        'valdir': '',
        'valtel':'',
        'valema':'',
        'valpro':'',
        'valfec':'',
        'valforpag':'',
        'valmon':'',
        'valfac':'',
        'valvenval':'',
        'valotrval':'',
        'valigvval':'',
        'valpreven':''
    };
    function display(){
        console.log(formState.errors);   
    }
    
    const {
        register,
        control,
        handleSubmit,
        setValue,
        getValues,
        formState,
        reset,
        watch
    } = useForm({resolver: zodResolver(schema)});

    // const {control: valitcontrol,...valitemsform} = useForm();
    
    // console.log('WATCH',watch());

    const { fields, append, remove } = useFieldArray({control: control, name:'valitems'});

    const {errors} = formState;

    const { field: fieldSelectResp } = useController({name:'valrescod', control});
    const { field: fieldSelectEstVal } = useController({name:'valestvalcod', control});
    const { field: fieldSelectProyecto} = useController({name:'valpro', control});
    const { field: fieldSelectValCli } = useController({name: 'valcli', control});
    // const onSubmitValItems = valitemsform.handleSubmit((formvalues) => {
    //     console.log(formvalues);
    // });

    const onSubmit = handleSubmit( async (formValues) => {
        
        const {enddate, initdate, valitems, ...valorizacion_obj} = formValues;
        // console.log('Valorizacion obj',valorizacion_obj);
        // console.log('Date range: ', {enddate, initdate});
        // console.log('valitems: ', valitems);

        try {
            const res = await createValorizacion(valorizacion_obj);
            for(const valitem of valitems){
                const newvalitem = {valitevalcod: res.data.valcod,...valitem};
                const valres = await createValorizacionItem(newvalitem);
            }
            toggle();
            loadValorizaciones();
        } catch (error) {
            console.error('Error al crear el Valorizacion:', error);
        }
        toggle();
        loadValorizaciones();
    });

    const loadEstVal = async () => {
        try {
            const res = await getAllEstadoValorizaciones();
            const data = res.map(estval => ({value: estval.estvalcod, label: estval.estvaldes}))
            // console.log('Estado de valorizaciones',res);
            setEstadoValorizaciones(data);
        } catch (error) {
            console.error('Error al cargar Estado Valorizaciones:', error);
        }
    };

    const loadResponsables = async () => {
        try {
          const res = await getAllResponsables();
          const data = res
            .filter((responsable) => responsable.resestreg.estregdes !== 'Eliminado')
            .map(resp => ({value:resp.rescod, label: `${resp.resnom} ${resp.resape}`}));
            setResponsables(data);
        } catch (error) {
        //   toast.error(`Error al cargar los responsables: ${error.message}`);
            console.log(error);
        }
    };

    const loadEmpresas = async () => {
        try{
            const res = await getAllEmpresas();
            const data = res
                .filter(empresa => empresa.empestreg.estregdes !== 'Inactivo')
                .map(empresa => ({value: empresa.empcod, label: empresa.emprazsoc}));
            setEmpresas(data);            
        }catch(error){
            console.log("Error al cargar Empresas-clientes", error);
        }
    }
    const loadProyectos = async () => {
        try{
            const res = await getAllProyectos();
            const data = res.filter((proy) => proy.proestreg.estregdes !== 'Eliminado')
            .map(proy => ({value: proy.procod, label:proy.pronom}));
            setProyectos(data);
            // console.log(data);
        }catch(error){
            console.log("LoadProyecto failed", error);
        }
    }
    const loadAlquileres = async () => {
        try{
            const res = await getAllAlquileres();
            const data = res.filter(alq => alq.alqestalqcod.estalqdes !== 'Eliminado');
            setAlquileres(data);
        }catch(error){
            console.log('LoadAlquileres failed', error);
        }
    }
    const loadTareosPerAlquiler = async (id) => {
        try{
            const res = await getByIdConteoTareoAlquiler(id);
            return res;
        }catch(error){
            console.log('LoadTareos failed', error);
        }
    }
    const loadEmpresaId = async (id) => {
        try{
            const res = await getEmpresaById(id);
            // console.log("LoadEmpresaId: ", res.data);
            // setEmpresa(res.data);
            return res.data;
        }catch(error){
            console.log('Failed to load Empresa data with id:', id)
        }
    };
    const loadResponsableId = async (id) => {
        try{
            const res = await getResponsableById(id);
            // console.log(res.data);
            // setResponsable(res.data);
            return res.data;
        }catch(error){
            console.log('Failed to load loadResponsableId data with id:', id);
        }
    }

    useEffect(() => {
        // loadEstReg();
        reset();
        
        loadEstVal();
        loadResponsables();
        loadEmpresas();
        loadProyectos();
        loadAlquileres();

        return () => {
            remove();
            setResponsables([]);
            setEstadoValorizaciones([]);
            setEmpresas([]);
            setProyectos([]);
            setAlquileres([]);
            reset()
        }
        // loadTareos();
      }, []
    );
    async function handleSelectChangeResp(option){
        const res = await loadResponsableId(option.value);
        setValue('valtel', res.restel);
        setValue('valema', res.resema);
        fieldSelectResp.onChange(option.value);
    }
    function handleSelectChangeEstVal(option){
        fieldSelectEstVal.onChange(option.value);
    }
    function handleSelectChangeProyecto(option){
        
        fieldSelectProyecto.onChange(option.value);
    }
    async function handleSelectChangeValCli(option){
        const res = await loadEmpresaId(option.value);
        setValue('valruc', res.empruc);
        setValue('valdir', res.empdir);
        fieldSelectValCli.onChange(option.label);
    }
    function onChangePreUnitCant(e,index){
        setValue(String(e.target.name), parseFloat(e.target.value));        
        const total = getValues(`valitems.${index}.valitecan`)*getValues(`valitems.${index}.valitepreuni`)
        let roundedTotal = Math.round(total*100)/100;
        setValue(`valitems.${index}.valitepretol`, roundedTotal);
    }
    function filtrar(e){
        remove();
        loadAlquileres();
        
        const filter = {
            'valpro': getValues('valpro') ,
            'valrescod': getValues('valrescod'),
            'initdate': getValues('initdate'),
            'enddate': getValues('enddate'),
        };
        if(!(filter['valpro'] && filter['valrescod'])){
            //( filter['initdate'] || filter['enddate'])
            console.log('Se necesita el proyecto y el responsable - filtrar function');
            return;
        }
        if(!alquileres){
            console.log("Alquileres state is empty");
            return;
        }
        let filtered = alquileres.filter(alq => alq.alqprocod.procod == String(filter['valpro']));
        filtered = filtered.filter(alq => alq.alqrescod.rescod == String(filter['valrescod']));
        
        if(filter['initdate']){
            let filter_date = new Date(filter['initdate']);
            filtered = filtered.filter(alq =>  new Date(alq.alqfecini) > filter_date);
        }
        if(filter['enddate']){
            let filter_date = new Date(filter['enddate']);
            filtered = filtered.filter(alq =>  new Date(alq.alqfecini) < filter_date);
        }
        if(filtered){
            // valitemsform.reset();
            setAlquileres(filtered);
            makeDataTable(filtered);
        }
        // && 
        // alq.alqrescod.rescod === String(filter['valrescod'])        
        console.log('filtered', filtered);
        // console.log('pepe');
        console.log(filter);
    }
    async function makeDataTable(filtered){
        // valitemsform.reset();
        
        //get all tareos, filter by alquiler ID and then put it in the table rows
        try{
            // const bufferTableData = [];
            let i = 1;
            for(const alq of filtered){
                const tareocounter =  await loadTareosPerAlquiler(alq.alqcod);
                // console.log(tareocounter);
                // console.log('TARECOUNTER',tareocounter);
                // tareocounter.then((res) => console.log('TareoCounter',res));
                if(!tareocounter){
                    console.log('No hay tareos para ese alquiler')
                }
                const row = {
                    item: i++,
                    // alqid: alq.alqcod,
                    description: `Vehiculo:${alq.alqvehcod.vehpla} Categoria:${alq.alqvehcod.vehcat} Marca:${alq.alqvehcod.vehmar} Modelo:${alq.alqvehcod.vehmod} Rango:${alq.alqfecini}-${alq.alqfecfin}`,
                    amount: tareocounter.dt | 0,
                    preunit: 166.77,
                    total: 0
                }                
                append(row);
                // bufferTableData.push(row);
            }
            
            // setTableData(beforetable => bufferTableData);

        }catch(error){
            console.log('Error making the table', error);
        }
    }
    return (
        <>
        <h2>Registrar nueva valorización</h2>
        <form onSubmit={onSubmit} className='create-valorizacion-form'>
            <fieldset id='form-filtrador'>
                <label>
                    Proyecto
                    <Select
                        value={proyectos.find(({value}) => value === fieldSelectProyecto.value)}
                        onChange={handleSelectChangeProyecto}
                        options={proyectos}
                    />
                    <div className='text-error'>{errors.valpro?.message}</div>
                    {/* {errors.valpro?.type === 'required' && <p className='text-error'>*El campo proyecto es requerido</p>} */}
                </label>
                <label>
                    Responsable
                    <Select
                        value={responsables.find(({value}) => value === fieldSelectResp.value)}
                        onChange={handleSelectChangeResp}
                        options={responsables}
                    />
                    <div className='text-error'>{errors.valrescod?.message}</div>
                </label>
                <label>Fecha inicio
                    <input type='date' className='input-text' {...register('initdate')}/>
                </label>
                <label>Fecha fin
                    <input type='date' className='input-text' {...register('enddate')}/>
                </label>
                <button type='button' onClick={filtrar}>Filtrar</button>
            </fieldset>
            

            
            <label>
                Estado de Valorizacion
                <Select
                    value={estadoValorizaciones.find(({value}) => value === fieldSelectEstVal.value)}
                    onChange={handleSelectChangeEstVal}
                    options={estadoValorizaciones}
                />
                <div className='text-error'>{errors.valestvalcod?.message}</div>
                {/* {errors.valestvalcod?.type === 'required' && <p className='text-error'>*El campo estado de valorizacion es requerido</p>} */}
            </label>
            <label>
                Empresa
                <Select
                    value={empresas.find(({value}) => value === fieldSelectValCli.value)}
                    onChange={handleSelectChangeValCli}
                    options={empresas}
                />
            </label>
            <label>
                RUC
                <input
                    className='input-text'
                    type="text"
                    placeholder='RUC'
                    {...register('valruc')}
                />
                {/* {errors.valruc?.type === 'required' && <p className='text-error'>*El campo RUC es requerido</p>} */}
                <div className='text-error'>{errors.valruc?.message}</div>
            </label>
            <label>
                Dirección
                <input
                    className='input-text'
                    type="text"
                    placeholder='Dirección'
                    {...register('valdir')}
                />
                {/* {errors.valdir?.type === 'required' && <p className='text-error'>*El campo dirección es requerido</p>} */}
                <div className='text-error'>{errors.valdir?.message}</div>
            </label>
            <label>
                Telefono
                <input
                    className='input-text'
                    type="text"
                    placeholder='Telefono'
                    {...register('valtel', { required: true })}
                />
                <div className='text-error'>{errors.valtel?.message}</div>
                {/* {errors.valtel?.type === 'required' && <p className='text-error'>*El campo telefono es requerido</p>} */}
            </label>
            <label>
                Email
                <input
                    className='input-text'
                    type="text"
                    placeholder='Email'
                    {...register('valema', { required: true })}
                />
                <div className='text-error'>{errors.valema?.message}</div>
                {/* {errors.valema?.type === 'required' && <p className='text-error'>*El campo email es requerido</p>} */}
            </label>

            <label>
                Fecha
                <input
                    className='input-text'
                    type="date"
                    placeholder='Fecha'
                    {...register('valfec', { required: true })}
                />
                <div className='text-error'>{errors.valfec?.message}</div>
                {/* {errors.valfec?.type === 'required' && <p className='text-error'>*El campo fecha es requerido</p>} */}
            </label>
            <label>
                Forma de Pago
                <input
                    className='input-text'
                    type="text"
                    placeholder='Forma de Pago'
                    {...register('valforpag', { required: true })}
                />
                <div className='text-error'>{errors.valforpag?.message}</div>
                {/* {errors.valforpag?.type === 'required' && <p className='text-error'>*El campo forma de pago es requerido</p>} */}
            </label>
            <label>
                Moneda
                <input
                    className='input-text'
                    type="text"
                    placeholder='Moneda'
                    {...register('valmon', { required: true })}
                />
                <div className='text-error'>{errors.valmon?.message}</div>
                {/* {errors.valmon?.type === 'required' && <p className='text-error'>*El campo moneda es requerido</p>} */}
            </label>
            <label>
                Factura
                <input
                    className='input-text'
                    type="checkbox"
                    placeholder='Factura'
                    {...register('valfac', { required: true })}
                />
                <div className='text-error'>{errors.valmon?.message}</div>
                {/* {errors.valfac?.type === 'required' && <p className='text-error'>*El campo factura es requerido</p>} */}
            </label>
            <label>
                Venta de Valorizacion
                <input
                    className='input-text'
                    type="number"
                    step="0.01"
                    placeholder='Venta de Valorizacion'
                    {...register('valvenval', { required: true })}
                />
                <div className='text-error'>{errors.valvenval?.message}</div>
                {/* {errors.valvenval?.type === 'required' && <p className='text-error'>*El campo venta de valorizacion es requerido</p>} */}
            </label>
            <label>
                Otros Valores
                <input
                    className='input-text'
                    type="number"
                    step="0.01"
                    placeholder='Otros Valores'
                    {...register('valotrval', { required: true })}
                />
                <div className='text-error'>{errors.valotrval?.message}</div>
                {/* {errors.valotrval?.type === 'required' && <p className='text-error'>*El campo otros valores es requerido</p>} */}
            </label>
            <label>
                IGV 18%
                <input
                    className='input-text'
                    type="number"
                    step="0.01"
                    placeholder='IGV'
                    {...register('valigvval', { required: true })}
                />
                <div className='text-error'>{errors.valigvval?.message}</div>
                {/* {errors.valigvval?.type === 'required' && <p className='text-error'>*El campo IGV es requerido</p>} */}
            </label>
            <label>
                Precio de Venta
                <input
                    className='input-text'
                    type="number"
                    step="0.01"
                    placeholder='Precio de Venta'
                    {...register('valpreven', { required: true })}
                />
                <div className='text-error'>{errors.valpreven?.message}</div>
                {/* {errors.valpreven?.type === 'required' && <p className='text-error'>*El campo precio de venta es requerido</p>} */}
            </label>
            <table id='form-formal-table'>
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Descripción</th>
                        <th>Cantidad</th>
                        <th>Precio unitario</th>
                        <th>Total</th>
                        <th>Operaciones</th>
                    </tr>
                </thead>
                <tbody>
                    {fields.map((field, index) => {
                        return (
                            <tr key={field.id}>
                                <td>{field.item}</td>
                                <td>
                                    <input
                                        type='text'
                                        className='input-text'
                                        {...register(`valitems.${index}.valitedes`,{value : field.description})}
                                    />
                                </td>
                                <td>
                                    <input 
                                        type="number" 
                                        className='input-text'
                                        {...register(`valitems.${index}.valitecan`,{
                                            value : field.amount,
                                            onChange: (e) => {
                                                onChangePreUnitCant(e, index);
                                            },
                                            valueAsNumber: true
                                        })}
                                        step='0.01'
                                    />
                                    <p>{errors.validitems?.message}</p>
                                </td>
                                <td>
                                    <input
                                        type='number'
                                        className='input-text'
                                        {...register(`valitems.${index}.valitepreuni`,{
                                            value : field.preunit,
                                            onChange: (e) => {
                                                onChangePreUnitCant(e, index);
                                            },
                                            valueAsNumber: true
                                        })}
                                        step='0.01'
                                    />
                                </td>
                                <td>
                                    <input
                                        type='number'
                                        className='input-text'
                                        {...register(`valitems.${index}.valitepretol`,{
                                            value : Math.round(field.preunit*field.amount*100)/100,
                                            valueAsNumber: true,
                                            
                                        })}
                                        step='0.01'
                                    />
                                </td>
                                <td>
                                    <div>
                                        <BsFillTrashFill
                                            color='green'
                                            className="btn-delete"
                                            onClick={() => remove(index)}
                                        />
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            {errors.valitems?.message}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Agregar <FaCirclePlus 
                                color='green'
                                className='btn-add'
                                onClick={() => append({
                                    valitedes: 'descripcion',
                                    valitecan: 0,
                                    valitepreuni: 0,
                                    valitepretol: 0,
                                })}
                            />
                        </td>
                    </tr>
                </tfoot>
            </table>
            <button type='button' onClick={() => display()}>Push me</button>
            <div className='contenedor-btn' id='form-buttons'>
                <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
                <button className='btn-registrar' type='submit'>Registrar</button>
            </div>
        </form>
        </>
    )
}
export { FormCreateValorizaciones };
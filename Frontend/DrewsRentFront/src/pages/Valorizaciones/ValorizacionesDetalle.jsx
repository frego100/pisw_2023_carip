import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getValorizacion } from '../../api/Valorizacion.api';
import { BsCurrencyDollar, BsPersonRolodex, BsBuildingFill, BsPersonFill, BsCarFrontFill, BsPersonBadgeFill } from "react-icons/bs";


function ValorizacionDetalle(props){

    const params = useParams();
    
    const [valorizacion, setValorizacion] = useState({});

    const navigate = useNavigate();
    
    const loadValorizacion = async () => {
        try{
            const res = await getValorizacion(params.id);
            setValorizacion(res.data);
            // console.log(res.data);
        }catch(error){
            console.log('Error al cargar Valorizacion ID:',params.id+'\n', error)
        }
    };
    
    const handleRegresar = () => {
        navigate('/valorizaciones');
     };
    
    useEffect(() => {
      loadValorizacion();  
    },[])
    return (
        <div className='contenedor-componente'>
            <h2>Valorización de Empresa {`${valorizacion.valcli} ${valorizacion.valfec}`}</h2>
            {valorizacion &&
                <div className='contenedor-detalle'>
                    <div className='detalle-datos'>
                        <h6>Estado Valorización</h6>
                        {valorizacion.valestvalcod && <p>{valorizacion.valestvalcod.estvaldes}</p>}
                    </div>
                    <div className='detalle-datos'>
                        <h6>Proyecto</h6>
                        <p>{valorizacion.valpro}</p>
                    </div>
                    <div className='detalle-datos'>
                        <h6>Fecha</h6>
                        <p>{valorizacion.valfec}</p>
                        <h6>Forma de pago</h6>
                        <p>{valorizacion.valforpag}</p>
                        <h6>Moneda</h6>
                        <p>{valorizacion.valmon}</p>
                        <h6>Facturable</h6>
                        <input type='checkbox' checked={valorizacion.valfac} value={valorizacion.valfac}/>
                    </div>
                    <div className='detalle-subtitle'>
                        <BsPersonFill color='green' className=''/>
                        <h4>Datos del responsable</h4>
                        <div className="linea"></div>
                    </div>           
                    <div className='detalle-datos'>
                        <h6>Responsable</h6>
                        {valorizacion.valrescod && <p>{`${valorizacion.valrescod.resnom} ${valorizacion.valrescod.resape}`}</p>}
                        <h6>Teléfono</h6>
                        <p> {valorizacion.valrescod?.restel} </p>
                        <h6>Email</h6>
                        <p> {valorizacion.valrescod?.resema} </p>
                    </div>
                    <div className='detalle-subtitle'>
                        <BsBuildingFill color='green' className=''/>
                        <h4>Datos del cliente</h4>
                        <div className="linea"></div>
                    </div>
                    <div className='detalle-datos'>
                        <h6>Nombre</h6>
                        <p>{valorizacion.valcli}</p>
                        <h6>RUC</h6>
                        <p>{valorizacion.valruc}</p>
                        <h6>Direccion</h6>
                        <p>{valorizacion.valdir}</p>
                        <h6>Teléfono</h6>
                        <p>{valorizacion.valtel}</p>
                        <h6>Email</h6>
                        <p>{valorizacion.valema}</p>
                    </div>
                    <div className='detalle-subtitle'>
                        <BsCurrencyDollar color='green' className=''/>
                        <h4>Costos totales de la valorización</h4>
                        <div className="linea"></div>
                    </div> 
                    <div className='detalle-datos'>
                        <h6>Valor de Venta</h6>
                        <p>{valorizacion.valvenval}</p>
                        <h6>Valor de Otros</h6>
                        <p>{valorizacion.valotrval}</p>
                        <h6>Valor de IGV</h6>
                        <p>{valorizacion.valigvval}</p>
                        <h6>Precio de venta</h6>
                        <p>{valorizacion.valpreven}</p>
                    </div>
                </div>
            }
            <div className='contenedor-btn-detalle'>
                <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
            </div> 
        </div>
    )
}

export default ValorizacionDetalle;
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getValorizacion, deleteValorizacion } from '../../api/Valorizacion.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from "react-hot-toast";


function FormDeleteValorizaciones ({ toggle, valorizacionId, loadValorizaciones }) {
    const [valorizacion, setValorizacion] = useState([]);


    const {
        register,
        handleSubmit,
        setValue,
        getValues,
        formState: { errors },
    } = useForm();

    const onSubmit = handleSubmit(async (data) => {
        try{
            const res = await deleteValorizacion(valorizacionId);
        }catch(err){
            console.log(err);
        }
        toggle();
        loadValorizaciones();
    });

    useEffect(() => {
        const loadValorizacion = async () => {
            try {
                const res = await getValorizacion(valorizacionId);
                const data = res.data;
                console.log('data', data);
                const fillValuesForm = {
                    ...data,
                    valrescod: data.valrescod.rescod,
                    valestvalcod: data.valestvalcod.estvalcod,
                    valpro: parseInt(data.valpro)
                }
                // Set default values to the form fields
                Object.keys(data).forEach((key) => {
                    setValue(key, fillValuesForm[key]);
                });
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        loadValorizacion();
      }, [valorizacionId]
    );
    return (
        <>
        <h3>¿Esta seguro de eliminar la valorizacion ?</h3>
        <form onSubmit={onSubmit}>
            <label>
                Cliente 
                <input
                    className='input-text'
                    type='text'
                    {...register('valcli')}
                />
            </label>
            <label>
                RUC 
                <input
                    className='input-text'
                    type='text'
                    {...register('valruc')}
                />
            </label>
            <label>
                Valor de venta
                <input
                    className='input-text'
                    type='text'
                    {...register('valvenval')}
                />
            </label>
            
            
            <div className='contenedor-btn'>
                <button className='btn-cancelar' onClick={toggle}>Cancelar</button>
                <button className='btn-confirmar' type='submit'>Confirmar</button>
            </div>
        </form>
        </>
    )
};


export { FormDeleteValorizaciones };

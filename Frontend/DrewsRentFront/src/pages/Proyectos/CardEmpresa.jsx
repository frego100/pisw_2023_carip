import React, { useEffect, useState } from 'react';
import { CardArea } from './CardArea';
import { BsBuildingFill, BsGeoAltFill, BsChevronDown, BsChevronRight } from "react-icons/bs";
import '../../styles/Proyectos.css'


function CardEmpresa() {

  //
  const [isExpanded, setIsExpanded] = useState(false);

  const handleToggleExpand = () => {
    setIsExpanded(!isExpanded);
  };


  return (
    <div className='card-empresa'>
      <div className='card-empresa-cabecera'>
        <div className='dato'>
          <BsBuildingFill 
            color='green'
            className=''
          />
          <h4>Nombre de la empresa:</h4>
          <p>Completar</p>
        </div>
        <div className='dato'>
          <BsGeoAltFill
            color='green'
            className=''/>
          <h4>Region:</h4>
          <p>completar</p>
        </div>        
        <div>
          {/* Icono para expandir/cerrar */}
          <span style={{ cursor: 'pointer' }} onClick={handleToggleExpand}>
            {isExpanded ? 
              <BsChevronRight color='green'/> : 
              <BsChevronDown color='green'/>
            }
          </span>
        </div>
      </div>
      {isExpanded && (
        <div className='card-empresa-expanded'>
          <CardArea></CardArea>
          <CardArea></CardArea>
        </div>
      )}
    </div>
  );
}

export { CardEmpresa };
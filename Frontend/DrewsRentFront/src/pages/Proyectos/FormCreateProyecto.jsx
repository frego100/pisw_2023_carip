import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createProyecto } from '../../api/Proyecto.api';
import { getAllRegiones } from '../../api/Region.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import DOMPurify from 'dompurify';


function FormCreateProyecto({toggle, loadProyectos}) {

  /*Datos de regiones*/ 
  const [regiones, setRegiones] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,  
    setValue,
    formState: { errors },
  } = useForm();

  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

  const navigate = useNavigate();

  // Función llamada al enviar el formulario
  const onSubmit = handleSubmit(async data => {
    try {
      const sanitizedData = Object.fromEntries(
        Object.entries(data).map(([key, value]) => [key, DOMPurify.sanitize(value)])
      );
      const res = await createProyecto(sanitizedData);
      toggle();
      loadProyectos();
      toast.success('Proyecto creado con éxito');
    } catch (error) {
      toast.error(`Error al crear el proyecto`);
    }
  });

  const loadEstReg = async () => {
    try {
      const resid = await getEstadoRegistro('Activo');
      if (resid === null) {
        toast.error(`El estado de registro Activo no existe en la Base de Datos`);
      } else {
        setValue('proestreg', resid);
      }
    } catch (error) {
      toast.error(`Error al cargar el estado de registro`);
    }
  };

  const loadRegiones = async () => {
    try {
      const res = await getAllRegiones();
      setRegiones(res);
    } catch (error) {
      toast.error(`Error al cargar las regiones`);
    }
  };

  // Cargar las regiones
  useEffect(() => {
    loadEstReg();
    loadRegiones();
    }, []
  );

  // Renderizar
  return (
    <>
    <h2>Registrar nuevo proyecto</h2>
    <form onSubmit={onSubmit}>
      <label>
        Nombre
        <input
          className='input-text'
          type="text"
          placeholder='Nombre del proyecto'
          {...register('pronom', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.pronom?.type === 'required' && <p className='text-error'>*El campo nombre es requerido</p>}
        {errors.pronom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}  
      </label>
      <label>
        Region
        <select className='input-select'
          name="regiones"
          {...register('proregcod', { required: true })}
        >
          <option value='' >Seleccionar región</option>
          {regiones && regiones.map(region =>(
            <option value={region.regcod} key={region.regcod}>
              {region.regnom}
            </option>
          ))}
        </select>
        {errors.proregcod?.type === 'required' && <p className='text-error'>*El campo región es requerido</p>}  
      </label>
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-registrar' type='submit'>Registrar</button>
      </div>
    </form>
    </>
  )
}

export { FormCreateProyecto };
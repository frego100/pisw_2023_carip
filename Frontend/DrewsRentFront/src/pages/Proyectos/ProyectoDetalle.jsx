import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { CardEmpresa } from './CardEmpresa';


function ProyectoDetalle() {

  const params = useParams();

  
  return (
    <div className='contenedor-componente'>
      <h2>Proyecto: {'completar'} </h2>
      <div className='contenedor-lista'>
        <h3>Empresas</h3>
        <CardEmpresa></CardEmpresa>
        <CardEmpresa></CardEmpresa>
      </div>
    </div>
  );
}

export default ProyectoDetalle;
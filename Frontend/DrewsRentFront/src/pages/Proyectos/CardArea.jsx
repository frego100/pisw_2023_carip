import React, { useEffect, useState } from 'react';
import { BsChevronDown, BsChevronRight, BsBoxes, BsPerson } from "react-icons/bs";
import { IoIosCar } from "react-icons/io";


function CardArea() {

  //
  const [isExpanded, setIsExpanded] = useState(false);

  const handleToggleExpand = () => {
    setIsExpanded(!isExpanded);
  };

  return (
    <div className='card-area'>
      <div className='card-area-cabecera'>
        <div className='dato'>
          <BsBoxes color='green'/>
          <h5>Nombre de la area:</h5>
          <p>Completar</p>
        </div>        
        <div>
          {/* Icono para expandir/cerrar */}
          <span style={{ cursor: 'pointer' }} onClick={handleToggleExpand}>
            {isExpanded ? 
              <BsChevronRight color='green'/> : 
              <BsChevronDown color='green'/>
            }
          </span>
        </div>
      </div>
      {isExpanded && (
        <div className='card-area-expanded'>
          <div className='card-area-1'>
            <Responsable></Responsable>
            <Operador></Operador>
          </div>      
          <div>
            <h6>Vehiculos</h6>
            <Vehiculo></Vehiculo>
          </div>
        </div> 
      )}     
    </div>
  );
}

function Responsable() {

  return ( 
    <div className='card-responsable'>
      <div className='icon'>
        <h6>Responsable</h6>
        <BsPerson color='green' />
      </div>      
      <div className='datos'>
        <p>Nombre:</p>
        <p>{'completar'}</p>
        <p>Apellidos:</p>
        <p>{'completar'}</p>
        <p>Email:</p>
        <p>{'completar'}</p>
        <p>Telefono:</p>
        <p>{'completar'}</p>
      </div>      
    </div>
  );
}

function Operador() {

  return ( 
    <div className='card-operador'>
      <div>
        <h6>Operador</h6>
      </div>      
      <div className='datos'>
        <p>Nombre:</p>
        <p>{'completar'}</p>
        <p>Apellidos:</p>
        <p>{'completar'}</p>
        <p>Telefono:</p>
        <p>{'completar'}</p>
      </div>
    </div>
  );
}

function Vehiculo() {

  return ( 
    <div className='card-vehiculo'>
      <div>
        <h6>Vehiculo</h6>
        <IoIosCar color='green'/>
      </div>      
      <div className='datos'>
        <p>Placa:</p>
        <p>{'completar'}</p>
        <p>F.Ingreso:</p>
        <p>{'completar'}</p>
        <p>FV. Soat:</p>
        <p>{'completar'}</p>
        <p>FV. Poliza:</p>
        <p>{'completar'}</p>
        <p>F. Salida:</p>
        <p>{'completar'}</p>
        <p>FV. Rtv:</p>
        <p>{'completar'}</p>
      </div>
      
    </div>
  );
}

export { CardArea };


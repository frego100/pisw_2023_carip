import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getProyecto, updateProyecto } from '../../api/Proyecto.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from "react-toastify";


function FormDeleteProyecto({ toggle, proyectoId, loadProyectos }) {

  /*Datos del proyecto seleccionado*/ 
  const [proyecto, setProyecto] = useState([]);   
  const [estReg, setEstReg] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = handleSubmit(async (data) => {
    try {
      const res = await updateProyecto(proyecto.procod, data);
      toggle();
      loadProyectos();
      toast.success('Proyecto eliminado con éxito');
    } catch (error) {
      toast.error(`Error al eliminar el proyecto`);
    }
  });

  useEffect(() => {
    async function loadProyecto() {
      try {
        const res = await getProyecto(proyectoId);
        setProyecto(res.data);
        setValue('pronom', res.data.pronom);
        setValue('proregcod', res.data.proregcod.regcod);
      } catch (error) {
        toast.error(`Error al cargar los proyectos`);
      }
    }
    async function loadEstReg() {
      try {
        const res = await getEstadoRegistro('Inactivo');
        if (res === null) {
          toast.warning('El estado de registro Inactivo no existe en la Base de datos');
        } else {
          setValue('proestreg', res);
          setEstReg(res);
        }
      } catch (error) {
        toast.error(`Error al cargar los estados`);
      }
    }
    loadProyecto();
    loadEstReg();
    }, [proyectoId]
  );

  return (
    <>
    <h3>¿Está seguro que quiere eliminar el proyecto "{proyecto.pronom}"?</h3>
    <form onSubmit={onSubmit}>
      <label>
        Estado de proyecto
        <select className='input-select' {...register('proestreg', { required: true })}>
          <option value={estReg} >Inactivo</option>
        </select>
        {errors.proestreg?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
      </label>
      {/* Botones de cancelar/eliminar */}
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-eliminar' type='submit'>Eliminar</button>
      </div>
    </form>
    </>
  );
}

export { FormDeleteProyecto };
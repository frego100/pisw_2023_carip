import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getProyecto, updateProyecto } from '../../api/Proyecto.api';
import { getAllRegiones } from '../../api/Region.api';
import { toast } from 'react-toastify';


function FormEditProyecto({ toggle, proyectoId, loadProyectos }) {

  /*Datos del proyecto seleccionado*/ 
  const [proyecto, setProyecto] = useState([]);
    
  /*Datos de regiones*/ 
  const [regiones, setRegiones] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,   
    setValue,
    formState: { errors },
  } = useForm();

  /*Validación de campos*/
  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

  const onSubmit = handleSubmit(async data => {
    try {
      const res = await updateProyecto(proyecto.procod, data);
      toggle();
      toast.success('Proyecto editado con éxito');
      loadProyectos();
    } catch (error) {
      toast.error(`Error al editar el proyecto`);
    }
  });

  useEffect(() => {
    async function loadProyecto() {
      try {
        const res = await getProyecto(proyectoId);
        setProyecto(res.data);
        setValue('pronom', res.data.pronom);
        setValue('proregcod', res.data.proregcod.regcod);
        setValue('proestreg', res.data.proestreg.estregcod);
      } catch (error) {
        toast.error(`Error al cargar los proyectos`);
      }
    }
    async function loadRegiones() {
      try {
        const res = await getAllRegiones();
        setRegiones(res);
      } catch (error) {
        toast.error(`Error al cargar las regiones`);
      }
    }
    loadProyecto();
    loadRegiones();
  }, [proyectoId]
  );


  return (
    <>
    <h2>Editar proyecto "{proyecto?.pronom}" </h2>
    <form onSubmit={onSubmit}>
      {/* Campo de entrada para el nombre del proyecto */}
      <label>
        Nombre
        <input
          className='input-text'
          type="text"
          placeholder='Nombre del proyecto'
          {...register('pronom', { required: true,
              validate: (value) => validateMaxSize(value)
          })}
        />
        {errors.pronom?.type === 'required' && <p className='text-error'>*El campo nombre es requerido</p>}
        {errors.pronom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}  
      </label>
      {/* Campo de selección para la región del proyecto */}
      <label>
        Region
        <select className='input-select'
          name="regiones"
          {...register('proregcod', { required: true })}
        >
          {regiones && regiones.map(region =>(
            <option value={region.regcod} key={region.regcod}>
              {region.regnom}
            </option>
          ))}
        </select>
        {errors.proregcod?.type === 'required' && (
          <p className='text-error'>*El campo región es requerido</p>
        )}  
      </label>            
      {/* Botones de cancelar/guardar */}
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={ toggle }>Cancelar</button>
        <button className='btn-guardar' type='submit'>Guardar</button>
      </div>
    </form>
    </>
  );
}

export { FormEditProyecto };
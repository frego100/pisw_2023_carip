import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllProyectosActivos } from '../../api/Proyecto.api';
import proyectoColumns from '../../Utils/ComponentesUtils/proyectoColumns';
import Modal from '../../components/Modal';
import { Table } from '../../components/Tabla/Table';
import { FormCreateProyecto } from './FormCreateProyecto';
import { FormEditProyecto } from './FormEditProyecto';
import { FormDeleteProyecto } from './FormDeleteProyecto';
import { toast, ToastContainer } from 'react-toastify';
import '../../styles/ContenedorComp.css';
import 'react-toastify/dist/ReactToastify.css';


function Proyectos() {

  /*Datos de proyectos para la tabla*/    
  const [proyectos, setProyectos] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);
  
  /*Estado del idProyectoSeleccionado */
  const [selectedIdProyecto, setSelectedIdProyecto] = useState();
  
  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  const handleEdit = (idProyecto) => {
    setSelectedIdProyecto(idProyecto);
    setActiveFormEdit(!activeFormEdit);
  };
  const handleDelete = (idProyecto) => {
    setSelectedIdProyecto(idProyecto);
    setActiveFormDelete(!activeFormDelete);
  };
  
  const loadProyectos = async () => {
    try {
      const res = await getAllProyectosActivos();
      setProyectos(res);
    } catch (error) {
      toast.error('Error al cargar los proyectos');
    }
  };

  useEffect(() => {
    loadProyectos();
  }, []);

  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Proyectos</h2>
        <div className='btn-agregar' onClick={toggle}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de proyectos */}
      <Table
        nombreID={'procod'}
        columns={proyectoColumns()}
        data={proyectos}
        nombre={'proyectos'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={false}
      />         
    </div>

    <ToastContainer />

    {/* Modal para crear nuevo proyecto */}
    <Modal active={activeFormCreate} toggle={toggle}>
      <FormCreateProyecto toggle={toggle} loadProyectos={loadProyectos}/>      
    </Modal>
    
    {/* Modal para editar proyecto existente */}
    <Modal active={activeFormEdit} toggle={handleEdit}>
      <FormEditProyecto toggle={handleEdit} proyectoId={selectedIdProyecto} loadProyectos={loadProyectos} />      
    </Modal>

    {/* Modal para eliminar proyecto */}
    <Modal active={activeFormDelete} toggle={handleDelete}>
      <FormDeleteProyecto toggle={handleDelete} proyectoId={selectedIdProyecto} loadProyectos={loadProyectos} /> 
    </Modal>
    </>
  )
}

export default Proyectos;
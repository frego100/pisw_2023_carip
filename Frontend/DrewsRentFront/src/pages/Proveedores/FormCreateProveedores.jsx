import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createProveedor } from '../../api/Proveedores.api';
import { getAllDistritos } from '../../api/Distrito.api';
import { getAllProvincias } from '../../api/Provincia.api';
import { getAllRegiones } from '../../api/Region.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import { BsBank2 } from "react-icons/bs";


function FormCreateProveedores({toggle, loadProveedores}) {

  const [regiones, setRegiones] = useState([]);
  const [provincias, setProvincias] = useState([]);
  const [distritos, setDistritos] = useState([]);

  const [selectedRegion, setSelectedRegion] = useState('');
  const [selectedProvincia, setSelectedProvincia] = useState('');
  const [selectedDistrito, setSelectedDistrito] = useState('');

  //const [proveedores, setProveedores] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,  
    setValue,
    formState: { errors },
  } = useForm();

  /*Valicaciones */
  const validateMaxSize = (value) => {
    return value.length <= 45;
  };
  // Función de validación para RUC (solo números y tamaño 11)
  const validateRUC = (value) => {
    return /^\d{11}$/.test(value);
  };

  const onSubmit = handleSubmit(async data => {
    try {
      const res = await createProveedor(data);
      toggle();
      loadProveedores();
      toast.success('Proveedor creado con éxito');
    } catch (error) {
      toast.error(`Error al cargar los proveedores`);
    }
  });

  const loadEstReg = async () => {
    try {
      const resid = await getEstadoRegistro('Activo');
      if (resid === null) {
        console.log('El estado de registro no existe. AGREGAR ESTADO DE REGISTRO A LA BASE DE DATOS',resid);
      } else {
        setValue('proestreg', resid);
      }
    } catch (error) {
      toast.error(`Error al cargar los estados: ${error.message}`);
    }
  };

  const loadDistritos = async () => {
    try {
      const res = await getAllDistritos();
      setDistritos(res);
    } catch (error) {
      toast.error(`Error al cargar los distritos: ${error.message}`);
    }
  };

  const loadProvincias = async () => {
    try {
      const res = await getAllProvincias();
      setProvincias(res);
    } catch (error) {
      toast.error(`Error al cargar las provincias: ${error.message}`);
    }
  };

  const loadRegiones = async () => {
    try {
      const res = await getAllRegiones();
      setRegiones(res);
    } catch (error) {
      toast.error(`Error al cargar las regiones: ${error.message}`);
    }
  };

  useEffect(() => {
    loadEstReg();
    loadRegiones();
    loadProvincias();
    loadDistritos();
    }, []
  );

  return (
    <>
    <h2>Registrar nuevo Proveedor</h2>
    <form onSubmit={onSubmit}>
      <label>
        RUC
        <input
          className='input-text'
          type="text"
          placeholder='RUC del Proveedor'
          {...register('proruc', { required: true,
            validate: (value) => validateRUC(value),
          })}
        />
          {errors.proruc?.type === 'required' && <p className='text-error'>*El campo RUC es requerido</p>}
          {errors.proruc?.type === 'validate' && <p className='text-error'>*El RUC solo debe ser numérico y tener exactamente 11 caracteres</p>}    
      </label>
      <label>
        Razon Social
        <input
          className='input-text'
          type="text"
          placeholder='Razon Social'
          {...register('prorazsoc', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.prorazsoc?.type === 'required' && <p className='text-error'>*El campo Razon Social es requerido</p>}
        {errors.prorazsoc?.type === 'validate' && <p className='text-error'>*El campo Razon social no debe superar los 45 caracteres</p>}    
      </label>
      <label>
        Telefono
        <input
          className='input-text'
          type="text"
          placeholder='Telefono del Proveedor'
          {...register('protel', { required: true })}
        />
        {errors.protel?.type === 'required' && <p className='text-error'>*El campo Telefono es requerido</p>}  
      </label>
      <label>
        Email
        <input
          className='input-text'
          type="text"
          placeholder='Email del Proveedor'
          {...register('proema', { required: true })}
        />
        {errors.proema?.type === 'required' && <p className='text-error'>*El campo Email es requerido</p>}  
      </label>
      <fieldset className='form-fila'>
        <label className='flex1'>
          Region
          <select className='input-select'
            name="regiones"
            {...register('disprocod.proregcod.regcod')}
            onChange={(e) => {
              setSelectedRegion(e.target.value);
              setSelectedProvincia('');
              setSelectedDistrito('');
            }}
          >
            <option value='' >Seleccionar Region</option>
            {regiones && regiones.map( (region) =>(
                <option value={region.regcod} key={region.regcod}>{region.regnom}</option>
            ))}
          </select>
        </label>
        <label className='flex1'>
          Provincia
          <select 
            style={{
              backgroundColor: !selectedRegion ? '#f2f2f2' : 'inherit',
              color: !selectedRegion ? '#999' : 'inherit',
            }}
            className='input-select'
            name="provincias"
            disabled={!selectedRegion}
            {...register('proubi.disprocod')}
            onChange={(e) => {
              setSelectedProvincia(e.target.value);
              setSelectedDistrito('');
            }}
          >
          <option value='' >Seleccionar Provincia</option>
          {provincias && provincias
            .filter((provincia) => provincia.proregcod.regcod === parseInt(selectedRegion))
            .map((provincia) =>(
                <option value={provincia.procod} key={provincia.procod}>{provincia.pronom}</option>
            ))}
          </select>
        </label>   
        <label className='flex1'>
          Distrito
          <select 
            style={{
              backgroundColor: !selectedProvincia ? '#f2f2f2' : 'inherit',
              color: !selectedProvincia ? '#999' : 'inherit',
            }}
            className='input-select'
            name="distritos"
            disabled={!selectedProvincia}
            {...register('proubi', { required: true })}
          >
            <option value='' >Seleccionar Distrito</option>
            {distritos && distritos
              .filter((distrito) => distrito.disprocod.procod === parseInt(selectedProvincia))
              .map((distrito) => (
                <option value={distrito.discod} key={distrito.discod}>{distrito.disnom}</option>
              ))
            }
          </select>
          {errors.proubicod?.type === 'required' && <p className='text-error'>*El campo distrito es requerido</p>}  
        </label>  
      </fieldset>
      <label>
        Dirección
        <input
          className='input-text'
          type="text"
          placeholder='Dirección del Proveedor'
          {...register('prodir', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.prodir?.type === 'required' && <p className='text-error'>*El campo Dirección es requerido</p>}
        {errors.prodir?.type === 'validate' && <p className='text-error'>*El campo Direccion no debe superar los 45 caracteres</p>}      
      </label>
      <div className='detalle-subtitle'>
        <BsBank2 color='green'/>
        <h5>Datos de la cuenta de depósito:</h5>
      </div>      
      <div className='form-fila'>      
        <label className='flex1'>
          Nombre del banco
          <input
            className='input-text'
            type="text"
            placeholder='Nombre del banco'
            {...register('probandep', { required: true })}
          />
          {errors.probandep?.type === 'required' && <p className='text-error'>*El campo Banco del Proveedor es requerido</p>}  
        </label>
        <label className='flex1'>
          Tipo de cuenta
          <input
            className='input-text'
            type="text"
            placeholder='Tipo de cuenta'
            {...register('protipcuedep', { required: true })}
          />
          {errors.protipcuedep?.type === 'required' && <p className='text-error'>*El campo Tipo de Cuenta de Proveedor es requerido</p>}  
        </label>
      </div>
      <div className='form-fila'>
      <label className='flex1'>
          Nro. de cuenta
          <input
            className='input-text'
            type="text"
            placeholder='Numero de la cuenta'
            {...register('pronumcuedep', { required: true })}
          />
          {errors.pronumcuedep?.type === 'required' && <p className='text-error'>*El campo Numero de Cuenta de Deposito del Proveedor es requerido</p>}  
        </label>
        <label className='flex1'>
          CCI
          <input
            className='input-text'
            type="number"
            placeholder='CCI Deposito'
            {...register('proccidep', { required: true })}
          />
          {errors.proccidep?.type === 'required' && <p className='text-error'>*El campo CCI del Proveedor es requerido</p>}  
        </label>
      </div>
      <div className='detalle-subtitle'>
        <BsBank2 color='green' className=''/>
        <h5>Datos de la cuenta de detracción:</h5>
      </div>
      <div className='form-fila'>
        <label className='flex1'>
          Nombre del banco
          <input
            className='input-text'
            type="text"
            placeholder='Nombre del banco'
            {...register('probandet', { required: true })}
          />
          {errors.probandet?.type === 'required' && <p className='text-error'>*El campo Banco Determinado del Proveedor es requerido</p>}  
        </label>
        <label className='flex1'>
          Tipo de cuenta
          <input
            className='input-text'
            type="text"
            placeholder='Tipo de cuenta'
            {...register('protipcuedet', { required: true })}
          />
          {errors.protipcuedet?.type === 'required' && <p className='text-error'>*El campo Tipo de Cuenta Determinada del Proveedor es requerido</p>}
        </label>
        <label className='flex1'>
          Numero de la cuenta
          <input
            className='input-text'
            type="number"
            placeholder='Numero de la cuenta'
            {...register('pronumcuedet', { required: true })}
          />
          {errors.pronumcuedet?.type === 'required' && <p className='text-error'>*El campo Numero de Cuenta Determinada del Proveedor es requerido</p>}
        </label>
      </div>           
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-registrar' type='submit'>Registrar</button>
      </div>
    </form>
    </>
  )
}

export { FormCreateProveedores };
import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getProveedorById } from '../../api/Proveedores.api';
import { toast } from 'react-toastify';
import { BsBuildingFill, BsBank2 } from "react-icons/bs";


function ProveedoresDetalle() {

  const params = useParams();

  const [proveedor, setProveedor] = useState([]);
  
  const navigate = useNavigate();

  const handleRegresar = () => {
    navigate('/proveedores');
  };
 

  useEffect(() => {
    const loadProveedor = async () => {
      try {
        const response = await getProveedorById(params.id);
        setProveedor(response.data);
      } catch (error) {
        toast.error(`Error al cargar el proveedores`);
      }
  };
  loadProveedor();
  }, []);

  

  return (
    <div className='contenedor-componente'>
      <h2>Proveedor: {proveedor.prorazsoc} </h2>
      <div className='contenedor-detalle'>
        <div className='detalle-subtitle'>
          <BsBuildingFill color='green' className=''/>
          <h5>Datos del proveedor:</h5>
        </div>
        <div className='detalle-datos'>
          <h6>RUC</h6>
          <p> {proveedor.proruc} </p>
          <h6>Razon Social</h6>
          <p> {proveedor.prorazsoc}</p>
          <h6>Telefono</h6>
          <p> {proveedor.protel} </p>
          <h6>Email</h6>
          <p> {proveedor.proema} </p>          
          <h6>Direccion</h6>
          <p> {proveedor.prodir}</p>
        </div>
        <div className='detalle-subtitle'>
          <BsBank2 color='green' className=''/>
          <h5>Datos de la cuenta bancaria para depósitos:</h5>
        </div>
        <div className='detalle-datos'>          
          <h6>Banco Deposito</h6>
          <p> {proveedor.probandep} </p>
          <h6>Tipo Cuenta Deposito</h6>
          <p> {proveedor.protipcuedep} </p>
          <h6>Numero Cuenta Deposito</h6>
          <p> {proveedor.pronumcuedep} </p>
          <h6>CCI Deposito</h6>
          <p> {proveedor.proccidep} </p>          
        </div>
        <div className='detalle-subtitle'>
          <BsBank2 color='green' className=''/>
          <h5>Datos de la cuenta bancaria para detracciones:</h5>
        </div>
        <div className='detalle-datos'>          
          <h6>Banco Detraccion</h6>
          <p> {proveedor.probandet} </p>
          <h6>Tipo Cuenta Detraccion</h6>
          <p> {proveedor.protipcuedet} </p>
          <h6>Numero Cuenta Detraccion</h6>
          <p> {proveedor.pronumcuedet} </p>
        </div>
      </div>
      <div className='contenedor-btn-detalle'>
        <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
      </div>      
    </div>
  );
}

export default ProveedoresDetalle;

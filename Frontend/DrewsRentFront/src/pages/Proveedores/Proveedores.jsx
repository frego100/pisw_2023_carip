import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllProveedoresActivos } from '../../api/Proveedores.api';
import provedorColumns from '../../Utils/ComponentesUtils/proveedorColumns';
import  Modal  from '../../components/Modal';
import { Table } from '../../components/Tabla/Table';
import { FormCreateProveedores } from './FormCreateProveedores';
import { FormEditProveedores } from './FormEditProveedores';
import { FormDeleteProveedores } from './FormDeleteProveedores';
import { toast, ToastContainer } from 'react-toastify';
import '../../styles/ContenedorComp.css'
import 'react-toastify/dist/ReactToastify.css';


function Proveedores() {

  /*Datos de proveedores para la tabla*/    
  const [proveedores, setProveedores] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);
  
  /*Estado del idProveedorSeleccionado */
  const [selectedIdProveedor, setSelectedIdProveedor] = useState();  
  
  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  const handleEdit = (idProveedor) => {
    setSelectedIdProveedor(idProveedor);
    setActiveFormEdit(!activeFormEdit);
  };
  const handleDelete = (idProveedor) => {
    setSelectedIdProveedor(idProveedor);
    setActiveFormDelete(!activeFormDelete);
  };
  
  const loadProveedores = async () => {
    try {
      const res = await getAllProveedoresActivos();
      setProveedores(res);
    } catch (error) {
      toast.error('Error al cargar los proveedores');
    }
  };

  useEffect(() => {
    loadProveedores();
  }, []);

  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Proveedores</h2>
        <div className='btn-agregar' onClick={toggle}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de proveedor */}
      <Table
        nombreID={'procod'}
        columns={provedorColumns()}
        data={proveedores}
        nombre={'proveedores'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={true}
      />         
    </div>

    <ToastContainer />

    {/* Modal para crear nuevo proveedor */}
    <Modal active={activeFormCreate} toggle={toggle}>
      <FormCreateProveedores toggle={toggle} loadProveedores={loadProveedores}/>      
    </Modal>
    
    {/* Modal para editar proveedor existente */}
    <Modal active={activeFormEdit} toggle={handleEdit}>
      <FormEditProveedores toggle={handleEdit} proveedorId={selectedIdProveedor} loadProveedores={loadProveedores} />      
    </Modal>

    {/* Modal para eliminar proveedor */}
    <Modal active={activeFormDelete} toggle={handleDelete}>
      <FormDeleteProveedores toggle={handleDelete} proveedorId={selectedIdProveedor} loadProveedores={loadProveedores} /> 
    </Modal>
    </>
  )
}
export default Proveedores;
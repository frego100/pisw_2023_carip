import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getProveedor, updateProveedor } from '../../api/Proveedores.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';


function FormDeleteProveedores ({ toggle, proveedorId, loadProveedores }){

  /*Datos del proveedor seleccionado*/ 
  const [proveedor, setProveedor] = useState([]);  
  const [estReg, setEstReg] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = handleSubmit(async (data) => {
    try {
      const res = await updateProveedor(proveedor.procod, data);
      toggle();
      loadProveedores();
      toast.success("Proveedor eliminado con éxito");
    } catch (error) {
      toast.error('Error al eliminar proveedor');
    } 
  });

  useEffect(() => {
    async function loadProveedor() {
      try{
        const res = await getProveedor(proveedorId);
        setProveedor(res.data);
        setValue('proubi', res.data.proubi.discod);
        setValue('prorazsoc', res.data.prorazsoc);
        setValue('prodir', res.data.prodir);
        setValue('protel', res.data.protel);
        setValue('proema', res.data.proema);
        setValue('proruc', res.data.proruc);
        setValue('probandep', res.data.probandep);
        setValue('protipcuedep', res.data.protipcuedep);
        setValue('pronumcuedep', res.data.pronumcuedep);
        setValue('proccidep', res.data.proccidep);
        setValue('probandet', res.data.probandet);
        setValue('protipcuedet', res.data.protipcuedet);
        setValue('pronumcuedet', res.data.pronumcuedet);
      }catch (error) {
        toast.error(`Error al cargar el proveedor`);
      }
    }
    async function loadEstReg() {
      try {
        const res = await getEstadoRegistro('Inactivo');
        if (res === null) {
          console.log('El estado de registro Inactivo no existe en la Base de datos');
        } else {
          setValue('proestreg', res);
          setEstReg(res);
        }
      } catch (error) {
        toast.error(`Error al cargar los estados`);
      }
    }
    loadProveedor();
    loadEstReg();
    }, [proveedorId]
  );
  return (
    <>
    <h3>¿Está seguro que quiere eliminar el proveedor "{proveedor.prorazsoc}"?</h3>
    <form onSubmit={onSubmit}>
      {/* Campo de selección para la región del proyecto */}
      <label>
        Estado del Proveedor
        <select className='input-select' {...register('proestreg', { required: true })}>
          <option value={estReg} >Inactivo</option>
        </select>
          {errors.proestreg?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
      </label>
      {/* Botones de cancelar/eliminar */}
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-eliminar' type='submit'>Eliminar</button>
      </div>
    </form>
    </>
  );
};

export { FormDeleteProveedores };
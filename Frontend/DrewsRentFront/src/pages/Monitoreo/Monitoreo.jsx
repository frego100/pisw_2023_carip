import React, { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { useTable, useSortBy, useGlobalFilter, usePagination } from 'react-table'
import { BiSearchAlt } from 'react-icons/bi'
import { MdKeyboardArrowRight, MdKeyboardArrowLeft } from "react-icons/md";

import '../../styles/ControllerBar.css'
import '../../styles/Monitoreo.css'
import '../../styles/ContenedorComp.css'

import useColumns from '../../Utils/AlquilerUtils/useColumns'
import { getAllAlquiler } from '../../api/Alquiler.api'

function Monitoreo() {

  //Buscador
  const [searchKey, setSearchKey] = useState("");

  const handleSearchInputChange = (e) => {
    setSearchKey(e.target.value);
  }

  //para el comboBox de los departamentos
  const [department, setDepartment] = useState("Todos");
  /*para la tabla*/
  const [tableData, setTableData] = useState([]);
  const columns = useColumns();

  useEffect(() => {
    async function loadData() {
      const res = await getAllAlquiler();
      setTableData(res.data);
      // console.log(res.data);
    }
    loadData();
  }, []);

  const table = useTable(
    {
      columns,
      data: tableData,
      initialState: {
        pageSize: 10,
        pageIndex: 0
      }
    },
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    setGlobalFilter,
    state,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    nextPage,
    previousPage,
  } = table;

  const { pageIndex } = state

  const handleFilter = () => {
    setGlobalFilter(searchKey);
  };

  /*seleccion de filas*/
  const navigate = useNavigate();

  const handleRowClick = (row) => {
    navigate(`/monitoreo/${row.original.id}`);
  }


  return (
    <div className='contenedor-componente'>
      <h2 className='title-section_monitoreo'>Monitoreo</h2>
      <div className='contenedor-filtro'>
        <div className='container_search'>
          <div className='container-input'>
            <span><BiSearchAlt className='iconSearch' /></span>
            <input
              type='text'
              placeholder='Buscar...'
              value={searchKey || ""}
              onChange={handleSearchInputChange}
              className='input-search'
            />
          </div>

        </div>

        <div className='container_button'>
          <button
            className='button_search_controller_monitoreo'
            onClick={handleFilter}
          >
            Buscar
          </button>
        </div>

        <p className='m-0 fw-bold'>Región</p>

        <div className='container_filter'>
          <select value={department} onChange={(e) => setDepartment(e.target.value)} className='list-city_controller'>
            <option value="Todos">Todos</option>
            <option value="Arequipa">Arequipa</option>
            <option value="Puno">Puno</option>
            <option value="Moquegua">Moquegua</option>
            <option value="Ica">Ica</option>
            <option value="Cusco">Cusco</option>
          </select>
        </div>
      </div>
      <div className='contenedor-lista'>
        <div className="title-table_container">
          <h3 className="titleTable">Monitoreo de Vehiculos</h3>
        </div>
        <table {...getTableProps()}>
          <thead>
            {
              headerGroups.map(headerGroup => (
                //<tr {...headerGroup.getHeaderGroupProps()} >
                <tr {...headerGroup.getHeaderGroupProps()} className='table-header'>
                  {headerGroup.headers.map((column, columnIndex) => {
                    return (
                      <th
                        {...column.getHeaderProps(
                          columnIndex !== headerGroup.headers.length - 1
                            ? column.getSortByToggleProps()
                            : {}
                        )}
                        className={
                          column.isSorted
                            ? column.isSortedDesc
                              ? 'desc'
                              : 'asc'
                            : ''
                        }
                      >
                        {column.render('Header')}
                      </th>
                    );
                  })}
                </tr>
              ))
            }
          </thead>
          <tbody {...getTableBodyProps()}>
            {
              page.map((row, rowIndex) => {
                prepareRow(row);
                return (
                  <tr
                    {...row.getRowProps()}
                    key={rowIndex}
                    className='row-table-monitoreo'
                    onClick={() => handleRowClick(row)}
                  >
                    {
                      row.cells.map((cell, cellIndex) => {
                        return (
                          <td {...cell.getCellProps()} key={cellIndex}>
                            {
                              cell.render("Cell")
                            }
                          </td>
                        );
                        // if (cell.column.id === 'acciones') {
                        //   return (
                        //     <td {...cell.getCellProps()} className='actions-cell' key={cellIndex}>
                        //       <div className='actions-container'>
                        //         <img src="/src/assets/BlackIcons/EditPenIcon.png" alt="edit-icon" className='edit-button' />
                        //         <img src="/src/assets/BlackIcons/TrashIcon.png" alt="trash-icon" onClick={() => {
                        //           setComp(row.cells[0].value);
                        //           setDeleteModal(true);
                        //         }} className='delete-button' />
                        //       </div>
                        //     </td>
                        //   );
                        // }
                        // else {
                        //   return (
                        //     <td {...cell.getCellProps()} key={cellIndex}>
                        //       {
                        //         cell.render("Cell")
                        //       }
                        //     </td>
                        //   );
                        // }
                      })
                    }
                  </tr>
                );
              })
            }
          </tbody>
        </table>
      </div>
      <div className="pagination">
        <button onClick={() => previousPage()} disabled={!canPreviousPage} className='paginationController-button'>
          <MdKeyboardArrowLeft className="page-controller" />
        </button>
        <strong>
          {pageIndex + 1} de {pageOptions.length}
        </strong>
        <button onClick={() => nextPage()} disabled={!canNextPage} className='paginationController-button'>
          <MdKeyboardArrowRight className="page-controller" />
        </button>
      </div>
    </div>
  );
}

export default Monitoreo

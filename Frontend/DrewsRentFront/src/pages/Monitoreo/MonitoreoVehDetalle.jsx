import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getAlquilerById } from '../../api/Alquiler.api'

export default function MonitoreoVehDetalle() {

  const {id} = useParams();
  const [monitoreoVehData, setMonitoreoVehData] = useState({});

  useEffect(() => {
    getAlquilerById(id)
      .then((response) => {
        setMonitoreoVehData(response.data);
      })
      .catch((error) => {
        console.error('Error al obtener los datos del alquiler:', error);
      });
  }, [id]); 

  return (
    <div>
        <h1>Proyecto: {monitoreoVehData?.alqprocod?.pronom}</h1>

        <div></div>
    </div>
  )
}

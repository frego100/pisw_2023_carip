import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getPagoOperador } from '../../api/PagoOperador.api';
import { toast } from 'react-toastify';
import { BsBuildingFill, BsPersonFill, BsCalendar2Date, BsConeStriped,BsCreditCardFill, BsCurrencyDollar, BsClipboard2CheckFill } from "react-icons/bs";

function PagoOperadorDetalle() {

  const params = useParams();
  /*Datos del alquiler*/ 
  const [pagOpe, setPagOpe] = useState([]);

  const navigate = useNavigate();

  const handleRegresar = () => {
    navigate('/pagosOperadores');
  };
  const handleEditar = () => {
    navigate(`/pagosOperadores/${params.id}/edit`);
  };

  useEffect(() => {
    const loadPagoOperador = async () => {
      try {
        const response = await getPagoOperador(params.id);
        setPagOpe(response.data);
      } catch (error) {
        toast.error(`Error al cargar el pago: ${error.message}`);
      }
  };
  loadPagoOperador();
  }, []);

  const styles = {
    separador: {
      color: '#000000',
      fontWeight: 'bold',
    },
  };
  
  return (
    <div className='contenedor-componente'>
      <h2>Detalle del pago al operador: </h2>
      <div className='contenedor-detalle'>
      <div className='detalle-subtitle'>
          <BsPersonFill color='green' className=''/>
          <h4>Datos del Operador</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
        <h6>Chofer</h6>
        <p> {pagOpe.pagopeopecod?.openom+" "+pagOpe.pagopeopecod?.opeape}</p>
        <h6>Telefono</h6>
        <p> {pagOpe.pagopeopecod?.opetel}</p>
        </div>
        <div className='detalle-subtitle'>
          <BsBuildingFill color='green' className=''/>
          <h4>Empresa</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
        <h6>Empresa</h6>
        <p> {pagOpe.pagopeemp ?? "-"} </p>
        </div>
        <div className='detalle-subtitle'>
          <BsCalendar2Date color='green' className=''/>
          <h4>Fechas</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
        <h6>Fecha Inicio del Periodo</h6>
        <p> {pagOpe.pagopeperini}</p>
        <h6>Fecha Fin del Periodo</h6>
        <p> {pagOpe.pagopeperfin}</p>
        <h6>Fecha del pago</h6>
        <p> {pagOpe.pagopefec}</p>
        <h6>Emision RH</h6>
        <p> {pagOpe.pagopeerh ?? "Ninguno"}</p>
        </div>
        <div className='detalle-subtitle'>
        <BsClipboard2CheckFill color='green' className=''/>
          <h4>Dias</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
        <h6>Cantidad de dias trabajados</h6>
        <p> {pagOpe.pagopecandia}</p>
        <h6>Costo por Dia</h6>
        <p> {"S/."+ (pagOpe.pagopecosdia ?? "00.00")}</p>
        </div>
        <div className='detalle-subtitle'>
        <BsConeStriped  color='green' className=''/>
          <h4>Adelantos</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
        <h6>Adelantos</h6>
        <p> {"S/."+ (pagOpe.pagopeade ?? "00.00")}</p>
        <h6>Saldo Viaticos</h6>
        <p> {"S/."+ (pagOpe.pagopesalvia ?? "00.00")}</p>
        <h6>Reemplazos</h6>
        <p> {"S/."+ (pagOpe.pagoperem ?? "00.00")}</p>
        <h6>Daños</h6>
        <p> {"S/."+ (pagOpe.pagopedan ?? "00.00")}</p>
        </div>
        <div className='detalle-subtitle'>
        <BsCreditCardFill color='green' className=''/>
          <h4>Descuentos</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
        <h6>Domingos</h6>
        <p> {"S/."+ (pagOpe.pagopedom ?? "00.00")}</p>
        <h6>R. Viaticos</h6>
        <p> {"S/."+ (pagOpe.pagopervi ?? "00.00")}</p>
        </div>
        <div className='detalle-subtitle'>
        <BsCurrencyDollar color='green' className=''/>
          <h4>Neto a Pagar</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
        <h6>Neto a pagar</h6>
        <p> {"S/."+ (pagOpe.pagopenetpag  ?? "00.00")}</p>
        <h6>Estado</h6>
        <p> {pagOpe.pagopeestpagope?.estpagopedes}</p>
        </div>
      </div>
      <div className='contenedor-btn-detalle'>
        <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
        <button className='btn-editar' type='button' onClick={ () => handleEditar()} >Editar</button>
      </div>      
    </div>
  );
}

export default PagoOperadorDetalle;

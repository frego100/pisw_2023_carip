import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getPagoOperador, deletePagoOperador } from '../../api/PagoOperador.api';
import { toast } from 'react-toastify';

function FormDeletePagOpe({ toggle, pagOpeId, loadPagOpes }) {

    /*Datos del proyecto seleccionado*/ 
    const [pagoOperador, setPagoOperador] = useState([]);

    /*Estado de los registros de mi formulario*/
    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    const onSubmit = handleSubmit(async (data) => {
        try {
            const res = await deletePagoOperador(pagoOperador.pagopecod);
            toggle();
            loadPagOpes();
            toast.success("Pago eliminado");
        } catch (error) {
            toast.error(`Error al eliminar el pago: ${error.message}`);
        }
    });

    useEffect(() => {
        async function loadPagoOperador() {
            try {
                const res = await getPagoOperador(pagOpeId);
                setPagoOperador(res.data);    
            } catch (error) {
                toast.error(`Error al cargar los pagos: ${error.message}`);
            }
        }
        loadPagoOperador();
    }, [pagOpeId]
    );

    return (
        <>
        <h3>¿Está seguro que quiere eliminar el pago al operador 
        <br />
            "{pagoOperador.pagopeopecod?.openom+" "+pagoOperador.pagopeopecod?.opeape}"?</h3>
        <form onSubmit={onSubmit}>
            <div className='contenedor-btn'>
                <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
                <button className='btn-eliminar' type='submit'>Eliminar</button>
            </div>
        </form>
        </>
    );
}

export { FormDeletePagOpe };
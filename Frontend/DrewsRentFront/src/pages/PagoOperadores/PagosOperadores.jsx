import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllPagosOperadores } from '../../api/PagoOperador.api';
import pagoOperadorColumns from '../../Utils/ComponentesUtils/pagoOperadorColumns';
import  Modal  from '../../components/Modal';
import { Table } from '../../components/Tabla/TableStyle3';
//import { FormEditResponsable } from './FormEditResponsable';
import { FormDeletePagOpe } from './FormDeletePagOpe';
import '../../styles/ContenedorComp.css';

import 'react-toastify/dist/ReactToastify.css';
import { toast, ToastContainer } from 'react-toastify';

function PagosOperadores() {

  const navigate = useNavigate();

  /*Datos de proyectos para la tabla*/    
  const [pagOpes, setpagOpes] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormDelete, setActiveFormDelete] = useState(false);
  
  /*Estado del idProyectoSeleccionado */
  const [selectedIdPagOpe, setSelectedIdPagOpe] = useState();
  
  /*Funciones */
  const handleCreate = () => {
    navigate('/pagosOperadores/create');
  }
  const handleEdit = (idPagOpe) => {
    navigate(`/pagosOperadores/${idPagOpe}/edit`);
  };
  const handleDelete = (idPagOpe) => {
    setSelectedIdPagOpe(idPagOpe);
    setActiveFormDelete(!activeFormDelete);
  };
  
  const loadPagosOperadores = async () => {
    try {
      const res = await getAllPagosOperadores();
      setpagOpes(res);
    } catch (error) {
      toast.error(`Error al cargar los pagos: ${error.message}`);
    }
  };

  useEffect(() => {
    loadPagosOperadores();
  }, []);


  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Pagos a Operadores</h2>
        <div className='btn-agregar' onClick={handleCreate}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de proyectos */}
      <Table
        columns={pagoOperadorColumns()}
        data={pagOpes}
        nombre={'pagosOperadores'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        onEditName={'pagopecod'}
        clickableRows={true}
      />         
    </div>

    <ToastContainer />
    
    <Modal toggle={handleEdit}>
    </Modal>

    <Modal active={activeFormDelete} toggle={handleDelete}>
      <FormDeletePagOpe toggle={handleDelete} pagOpeId={selectedIdPagOpe} loadPagOpes={loadPagosOperadores} /> 
    </Modal>
    </>
  )
}

export default PagosOperadores

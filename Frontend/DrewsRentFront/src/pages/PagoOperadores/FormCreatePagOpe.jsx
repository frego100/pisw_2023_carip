import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createPagoOperador } from '../../api/PagoOperador.api';
import { getAllOperadores } from '../../api/Operador.api';
import { getAllEstadoPagOpe } from '../../api/EstadoPagoOperador.api';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

function FormCreatePagOpe() {

    /*Datos de regiones*/ 
    const [operadores, setOperadores] = useState([]);
    const [estadosPagOpe, setEstadosPagOpe] = useState([]);

    /*Estado de los registros de mi formulario*/
    const {
        register,
        handleSubmit,  
        setValue,
        watch,
        formState: { errors },
    } = useForm();

    const navigate = useNavigate();

    const validateMaxSize = (value) => {
        return value.length <= 45;
    };

    const handleCancelar = () => {
        navigate('/pagosOperadores');
    };

    const diasTrabajados = parseInt(watch('pagopecandia')) || 0;
    const costoPorDia = parseFloat(watch('pagopecosdia')) || 0;
    const adelantos = parseFloat(watch('pagopeade', 0)) || 0;
    const saldoViaticos = parseFloat(watch('pagopesalvia', 0)) || 0;
    const reemplazos = parseFloat(watch('pagoperem', 0)) || 0;
    const danios = parseFloat(watch('pagopedan', 0)) || 0;
    const domingos = parseFloat(watch('pagopedom', 0)) || 0;
    const rViaticos = parseFloat(watch('pagopervi', 0)) || 0;

    const netoAPagar = (diasTrabajados * costoPorDia) - adelantos - saldoViaticos - reemplazos - danios + domingos + rViaticos;

    useEffect(() => {
      setValue('pagopenetpag', netoAPagar.toFixed(2));
    }, [netoAPagar, setValue]);
    
    // Función llamada al enviar el formulario
    const onSubmit = handleSubmit(async data => {
        try {
            data.pagopeade = data.pagopeade === '' ? null : data.pagopeade;
            data.pagopesalvia = data.pagopesalvia === '' ? null : data.pagopesalvia;
            data.pagoperem = data.pagoperem === '' ? null : data.pagoperem;
            data.pagopedan = data.pagopedan === '' ? null : data.pagopedan;
            data.pagopedom = data.pagopedom === '' ? null : data.pagopedom;
            data.pagopervi = data.pagopervi === '' ? null : data.pagopervi;
            const res = await createPagoOperador(data);
            navigate('/pagosOperadores');

            toast.success('Pago creado con éxito');
        } catch (error) {
            toast.error(`Error al crear el pago: ${error.message}`);
        }
    });

    const loadOperadores = async () => {
        try {
            const res = await getAllOperadores();
            setOperadores(res);
        } catch (error) {
            toast.error(`Error al cargar los operadores: ${error.message}`);
        }
    };
    const loadEstadosPagosOperadores = async () => {
        try {
            const res = await getAllEstadoPagOpe();
            setEstadosPagOpe(res);
        } catch (error) {
            toast.error(`Error al cargar los estados: ${error.message}`);
        }
    };

    // Cargar las regiones
    useEffect(() => {
        loadOperadores();
        loadEstadosPagosOperadores();
      }, []
    );

    const styles = {
        flexContainer: {
          display: 'flex',
          justifyContent: 'space-between', // Otra opción puede ser 'space-around' según tus preferencias
        },
        flexItem: {
          flex: '1', // Esto hace que cada elemento ocupe todo el espacio disponible
          margin: '0 5px',
        },
        separador: {
            color: '#000000',
            fontWeight: 'bold',
          },
      };

    // Renderizar
    return (
      <div className='contenedor-componente'>
        <h2>Registrar nuevo Pago al Operador</h2>
        <div className='contenedor-crear'>
          <form onSubmit={onSubmit}>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                        Operador
                        <select className='input-select'
                            name="operadores"
                            {...register('pagopeopecod', { required: true })}
                            >
                            <option value='' >Seleccionar Operador</option>
                            {operadores && operadores.map(operador =>(
                                <option value={operador.opecod} key={operador.opecod}>{operador.openom+" "+operador.opeape}</option>
                            ))}
                        </select>
                        {errors.pagopeopecod?.type === 'required' && <p className='text-error'>*El campo Operador es requerido</p>}  
                    </label>
                </div>
            </div>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                    Empresa
                    <input
                        className='input-text'
                        type="text"
                        placeholder='Empresa'
                        {...register('pagopeemp', { 
                            required: true,
                            validate: (value) => validateMaxSize(value),
                        })}
                    />
                    {errors.pagopeemp?.type === 'required' && <p className='text-error'>*El campo Empresa es requerido</p>}
                    {errors.pagopeemp?.type === 'validate' && <p className='text-error'>*El campo Empresa no debe superar los 45 caracteres</p>}
                    </label>
                </div>
            </div>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                    Cantidad de Dias trabajados
                    <input
                        className='input-text'
                        type="number"
                        step="1"
                        min="0"
                        placeholder='Dias Trabajados'
                        {...register('pagopecandia', { 
                            required: true})}
                    />
                    {errors.pagopecandia?.type === 'required' && <p className='text-error'>*El campo cantidad dias es requerido</p>}
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    Costo por Dia
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        placeholder='Costo por Dia'
                        {...register('pagopecosdia', { required: true })}
                    />
                    {errors.pagopecosdia?.type === 'required' && <p className='text-error'>*El campo costo por dia es requerido</p>}
                    </label>
                </div>
            </div>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                    Fecha Inicio del Periodo
                    <input
                        className='input-text'
                        type="date"
                        {...register('pagopeperini', { required: true })}
                    />
                    {errors.pagopeperini?.type === 'required' && <p className='text-error'>*El campo Fecha Inicio del Periodo es requerido</p>}
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    Fecha Fin del Periodo
                    <input
                        className='input-text'
                        type="date"
                        {...register('pagopeperfin', { required: true })}
                    />
                    {errors.pagopeperfin?.type === 'required' && <p className='text-error'>*El campo Fecha Fin del Periodo es requerido</p>}
                    </label>
                </div>
            </div>
            <h6 style={styles.separador}>Descuentos:</h6>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                    Adelantos
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        placeholder='Adelantos'
                        {...register('pagopeade')}
                    />
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    Saldo Viaticos
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        placeholder='Saldo Viaticos'
                        {...register('pagopesalvia')}
                    />
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    Reemplazos
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        placeholder='Reemplazos'
                        {...register('pagoperem')}
                    />
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    Daños
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        placeholder='Daños'
                        {...register('pagopedan')}
                    />
                    </label>
                </div>
            </div>
            <h6 style={styles.separador}>Adicionales:</h6>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                    Domingos
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        placeholder='Domingos'
                        {...register('pagopedom')}
                    />
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    R. Viaticos
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        placeholder='R. Viaticos'
                        {...register('pagopervi')}
                    />
                    </label>
                </div>
            </div>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                    Neto a Pagar
                    <input
                        className='input-text'
                        type="number"
                        step="0.01"
                        min="0"
                        disabled={true}
                        placeholder='Neto a pagar'
                        {...register('pagopenetpag', { required: true })}
                    />
                    {errors.pagopenetpag?.type === 'required' && <p className='text-error'>*El campo Neto a Pagar es requerido</p>}
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    Fecha del Pago
                    <input
                        className='input-text'
                        type="date"
                        {...register('pagopefec', { required: true })}
                    />
                    {errors.pagopefec?.type === 'required' && <p className='text-error'>*El campo Fecha del Pago es requerido</p>}
                    </label>
                </div>
            </div>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                    Emision RH
                    <input
                        className='input-text'
                        type="date"
                        placeholder='Emision RH'
                        {...register('pagopeerh', { required: true })}
                    />
                    {errors.pagopeerh?.type === 'required' && <p className='text-error'>*El campo RH es requerido</p>}
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                    Estado
                    <select className='input-select'
                        name="estadosPagOpe"
                        {...register('pagopeestpagope', { required: true })}
                        >
                        <option value='' >Seleccionar Estado</option>
                        {estadosPagOpe && estadosPagOpe.map(estado =>(
                            <option value={estado.estpagopecod} key={estado.estpagopecod}>{estado.estpagopedes}</option>
                        ))}
                    </select>
                    {errors.pagopeestpagope?.type === 'required' && <p className='text-error'>*El campo Estado es requerido</p>}  
                    </label> 
                </div>
            </div>
            <div className='contenedor-btn'>
                <button className='btn-cancelar' type='button' onClick={() => handleCancelar()}>Cancelar</button>
                <button className='btn-registrar' type='submit'>Registrar</button>
            </div>
          </form>
        </div>
      </div>
    )
}

export { FormCreatePagOpe };
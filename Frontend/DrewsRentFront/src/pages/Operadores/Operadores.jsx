import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllOperadoresActivos } from '../../api/Operador.api';
import  Modal  from '../../components/Modal';
import { Table } from '../../components/Tabla/Table';
import { FormCreateOperador } from './FormCreateOperador';
import { FormEditOperador } from './FormEditOperador';
import { FormDeleteOperador } from './FormDeleteOperador';
import '../../styles/ContenedorComp.css';
import operadorColumns from '../../Utils/ComponentesUtils/operadorColumns';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


function Operadores() {

  /*Datos de operador para la tabla*/    
  const [operadores, setOperadores] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);  
 
  /*Estado del idOperadorSeleccionado*/
  const [selectedIdOperador, setSelectedIdOperador] = useState();
  
  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  const handleEdit = (idOperador) => {
    setSelectedIdOperador(idOperador);
    setActiveFormEdit(!activeFormEdit);
  };
  const handleDelete = (idOperador) => {
    setSelectedIdOperador(idOperador);
    setActiveFormDelete(!activeFormDelete);
  };
  
  const loadOperadores = async () => {
    try {
      const res = await getAllOperadoresActivos();
      setOperadores(res);
    } catch (error) {
      toast.error(`Error al cargar las empresas`);
    }
  };

  useEffect(() => {
    loadOperadores();
  }, []);

  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Operadores</h2>
        <div className='btn-agregar' onClick={toggle}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de Operadores */}
      <Table
        nombreID={'opecod'}
        columns={operadorColumns()}
        data={operadores}
        nombre={'operadores'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={true}
      />         
    </div>
    <ToastContainer />

    <Modal active={activeFormCreate} toggle={toggle}>
      <FormCreateOperador toggle={toggle} loadOperadores={loadOperadores}/>      
    </Modal>

    <Modal active={activeFormEdit} toggle={handleEdit}>
      <FormEditOperador toggle={handleEdit} operadorId={selectedIdOperador} loadOperadores={loadOperadores} />      
    </Modal>

    <Modal active={activeFormDelete} toggle={handleDelete}>
    <FormDeleteOperador toggle={handleDelete} operadorId={selectedIdOperador} loadOperadores={loadOperadores} /> 
    </Modal>
    </>
  )
}
export default Operadores;

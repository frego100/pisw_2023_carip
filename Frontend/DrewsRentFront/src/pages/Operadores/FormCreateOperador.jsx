import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createOperador } from '../../api/Operador.api';
import { getAllEstadosOperador } from '../../api/EstadoOperador.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';


function FormCreateOperador({toggle, loadOperadores}) {

  /*Datos de regiones*/ 
  const [estadosOperador, setEstadosOperador] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,  
    setValue,
    formState: { errors },
  } = useForm();

  const validateMaxSize = (value) => {
    return value.length <= 45;
  };
  const validateTelefono = (value) => {
    return /^\d{9}$/.test(value);
  };

  // Función llamada al enviar el formulario
  const onSubmit = handleSubmit(async data => {
    try {
      const res = await createOperador(data);
      toggle();
      loadOperadores();
      toast.success('Operador creado con éxito');
    } catch (error) {
      toast.error(`Error al cargar los operador`);
    }
  });

  const loadEstReg = async () => {
    try {
      const resid = await getEstadoRegistro('Activo');
      if (resid === null) {
        toast.error(`El estado de registro Activo no existe en la Base de Datos`);
      } else {
        setValue('opeestreg', resid);
      }
    } catch (error) {
      toast.error(`Error al cargar los estados de registro`);
    }
  };    

  const loadEstadosOperador = async () => {
    try {
      const res = await getAllEstadosOperador();
      setEstadosOperador(res);
    } catch (error) {
      toast.error(`Error al cargar los estados de operador`);
    }
  };

  // Cargar los operadores
  useEffect(() => {
    loadEstReg();
    loadEstadosOperador();
    }, []
  );

  // Renderizar
  return (
    <>
    <h2>Registrar nuevo Operador</h2>
    <form onSubmit={onSubmit}>
      <label>
        Nombre
        <input
          className='input-text'
          type="text"
          placeholder='Nombre del Operador'
          {...register('openom', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.openom?.type === 'required' && <p className='text-error'>*El campo nombre es requerido</p>}  
        {errors.openom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}
      </label>
      <label>
        Apellido
        <input
          className='input-text'
          type="text"
          placeholder='Apellido del Operador'
          {...register('opeape', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.opeape?.type === 'required' && <p className='text-error'>*El campo apellido es requerido</p>}  
        {errors.opeape?.type === 'validate' && <p className='text-error'>*El campo apellido no debe superar los 45 caracteres</p>}
      </label>
      <label>
        Teléfono
        <input
          className='input-text'
          type="number"
          placeholder='Telefono del Operador'
          {...register('opetel', { required: true,
            validate: (value) => validateTelefono(value),
          })}
        />
        {errors.opetel?.type === 'required' && <p className='text-error'>*El campo teléfono es requerido</p>}
        {errors.opetel?.type === 'validate' && <p className='text-error'>*El campo teléfono debe ser numérico y de 9 caracteres</p>}
      </label>
      <label>
        Estado del operador
        <select className='input-select'
          name="estadosOperador"
          {...register('opeestopecod', { required: true })}
        >
          <option value='' >Seleccionar estado de operador</option>
          {estadosOperador && estadosOperador.map(estado =>(
            <option value={estado.estopecod} key={estado.estopecod}>
              {estado.estopedes}
            </option>
          ))}
        </select>
        {errors.opeestopecod?.type === 'required' && <p className='text-error'>*El campo estado de operador es requerido</p>}  
      </label>
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-registrar' type='submit'>Registrar</button>
      </div>
    </form>
    </>
  )
}

export { FormCreateOperador };

import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getOperador, updateOperador } from '../../api/Operador.api';
import { getAllEstadosOperador } from '../../api/EstadoOperador.api';
import { toast } from 'react-toastify';


function FormEditOperador({ toggle, operadorId, loadOperadores }) {
  
  /*Datos del operador seleccionado*/   
  const [operador, setOperador] = useState([]);

  /*Datos de estados de operador*/
  const [estadosOperador, setEstadosOperador] = useState([]);
    
  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,   
    setValue,
    formState: { errors },
  } = useForm();

  /*Validación de campos*/
  const validateTelefono = (value) => {
    return /^\d{9}$/.test(value);
  };
  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

  const onSubmit = handleSubmit(async data => {
    try {
      const res = await updateOperador(operador.opecod, data);
      toast.success('Operador editado con éxito');
      toggle();
      loadOperadores();  
    } catch (error) {
      toast.error(`Error al editar el Operador`);
    }
  });
  
  useEffect(() => {
    async function loadOperador() {        
      try{ 
        const res = await getOperador(operadorId);
        setOperador(res.data);
        setValue('openom', res.data.openom);        
        setValue('opeape', res.data.opeape);        
        setValue('opetel', res.data.opetel);        
        setValue('opeestopecod', res.data.opeestopecod.estopecod);
        setValue('opeestreg', res.data.opeestreg.estregcod);        
      }catch (error) {
        toast.error(`Error al cargar el operador`);
      }
    }
    async function loadEstadosOperador() {
      try {
        const res = await getAllEstadosOperador();
        setEstadosOperador(res);
      } catch (error) {
        toast.error(`Error al cargar los estados de operador`);
      }
    };
    loadOperador();
    loadEstadosOperador();
  }, [operadorId]
  );

  return (
    <>
    <h2>Editar operador "{operador?.openom}" </h2>
    <form onSubmit={onSubmit}>
      <label>
        Nombre
        <input
          className='input-text'
          type="text"
          placeholder='Nombre del Operador'
          {...register('openom', { required: true,
              validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.openom?.type === 'required' && <p className='text-error'>*El campo nombre es requerido</p>} 
        {errors.openom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}
      </label>
      <label>
        Apellido
        <input
          className='input-text'
          type="text"
          placeholder='Apellido del Operador'
          {...register('opeape', { required: true,
              validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.opeape?.type === 'required' && <p className='text-error'>*El campo apellido es requerido</p>} 
        {errors.opeape?.type === 'validate' && <p className='text-error'>*El campo apellido no debe superar los 45 caracteres</p>}
      </label>
      <label>
        Telefono
        <input
          className='input-text'
          type="text"
          placeholder='Telefono del Operador'
          {...register('opetel', { required: true,
              validate: (value) => validateTelefono(value),
          })}
        />
        {errors.opetel?.type === 'required' && <p className='text-error'>*El campo telefono es requerido</p>}
        {errors.opetel?.type === 'validate' && <p className='text-error'>*El telefono solo debe ser numérico y tener exactamente 9 caracteres</p>}  
      </label>
      <label>
        Estado del operador
        <select className='input-select'
          name="estadosOperador"
          {...register('opeestopecod', { required: true })}
        >
          {estadosOperador && estadosOperador.map(estado =>(
            <option value={estado.estopecod} key={estado.estopecod}>
              {estado.estopedes}
            </option>
          ))}
        </select>
        {errors.opeestopecod?.type === 'required' && <p className='text-error'>*El campo estado de operador es requerido</p>}  
      </label>
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-guardar' type='submit'>Guardar</button>
      </div>
    </form>
    </>
  );
}

export { FormEditOperador };

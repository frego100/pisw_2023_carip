import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getOperador, updateOperador } from '../../api/Operador.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';


function FormDeleteOperador({ toggle, operadorId, loadOperadores }) {

  /*Datos del operador seleccionado*/
  const [operador, setOperador] = useState([]);
  const [estReg, setEstReg] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = handleSubmit(async (data) => {
    try {
      const res = await updateOperador(operador.opecod, data);
      toggle();
      loadOperadores();
      toast.success("Operador eliminado con éxito");
    } catch (error) {
      toast.error('Error al eliminar operador');
    }
  });

  useEffect(() => {
    async function loadOperador() {
      try{
        const res = await getOperador(operadorId);
        setOperador(res.data);
        setValue('openom', res.data.openom);
        setValue('opeape', res.data.opeape);
        setValue('opetel', res.data.opetel);
        setValue('opeestopecod', res.data.opeestopecod.estopecod);
      }catch (error) {
        toast.error(`Error al cargar el operador`);
      }
    }
    async function loadEstReg() {
      try {
        const res = await getEstadoRegistro('Inactivo');
        if (res === null) {
          console.log('El estado de registro Inactivo no existe en la Base de datos');
        } else {
          setValue('opeestreg', res);
          setEstReg(res);
        }
      } catch (error) {
        toast.error(`Error al cargar los estados`);
      }
    }
    loadOperador();
    loadEstReg();
  }, [operadorId]
  );

  return (
    <>
    <h3>¿Está seguro que quiere eliminar el operador "{operador.openom}"?</h3>
    <form onSubmit={onSubmit}>
      {/* Campo de selección */}
      <label>
        Estado del Operador
        <select className='input-select' {...register('opeestreg', { required: true })}>
            <option value={estReg} >Inactivo</option>
        </select>
        {errors.opeestreg?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
      </label>
      {/* Botones de cancelar/eliminar */}
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-eliminar' type='submit'>Eliminar</button>
      </div>
    </form>
    </>
  );
}

export { FormDeleteOperador };

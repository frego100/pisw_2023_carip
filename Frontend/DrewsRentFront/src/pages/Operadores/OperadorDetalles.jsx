import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getOperadorById } from '../../api/Operador.api';
import { toast } from 'react-toastify';

function OperadorDetalle() {
  const params = useParams();

  const [operador, setOperador] = useState([]);

  const navigate = useNavigate();

  const handleRegresar = () => {
    navigate('/operadores');
  };
   

  useEffect(() => {
    const loadOperador = async () => {
      try {
        const response = await getOperadorById(params.id);
        setOperador(response.data);
      } catch (error) {
        toast.error(`Error al cargar los operadores: ${error.message}`);
      }
    };
    loadOperador();
  }, []);

  return (
    <div className='contenedor-componente'>
      <h2>Operador: {operador.openom} {operador.opeape} </h2>
      <div className='contenedor-detalle'>
        <div className='detalle-datos'>
          <h6>Nombre</h6>
          <p> {operador.openom}</p>
          <h6>Apellido</h6>
          <p> {operador.opeape}</p>
          <h6>Telefono</h6>
          <p> {operador.opetel}</p>
          <h6>Estado de operador</h6>
          <p> {operador.opeestopecod?.estopedes}</p>
        </div>
      </div>
      <div className='contenedor-btn-detalle'>
        <button className='btn-regresar' type='button' onClick={() => handleRegresar()} >Regresar</button>
      </div>
    </div>
  );
}

export default OperadorDetalle;
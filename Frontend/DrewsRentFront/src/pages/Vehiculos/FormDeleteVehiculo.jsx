import React, {useEffect, useState} from 'react';
import { useForm } from 'react-hook-form';
import { getVehiculo, updateVehiculo } from '../../api/Vehiculo.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';
import { getAllEstadoVeh } from '../../api/EstadoVehiculo.api'


function FormDeleteVehiculo({toggle, vehiculoId, loadVehiculos}){

  /*Datos del vehiculo seleccionado*/
  const [vehiculo, setVehiculo] = useState([]);
  const [estReg, setEstReg] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = handleSubmit(async (data) =>  {
    try{
      const res = await updateVehiculo(vehiculo.vehcod, data);
      toggle();
      loadVehiculos();
      toast.success('Vehiculo eliminado con éxito');
    }catch(error){
      toast.error(`Error al eliminar vehiculo`);
    }
  });

  useEffect(() => {
    async function loadVehiculo() {
      try {
        const res = await getVehiculo(vehiculoId);
        setVehiculo(res.data);
        setValue('vehprocod', res.data.vehprocod.procod);
        setValue('vehestvehcod', res.data.vehestvehcod.estvehcod);
        setValue('vehpla', res.data.vehpla);
        setValue('vehcat', res.data.vehcat);
        setValue('vehsoa', res.data.vehsoa);
        setValue('vehfecvensoa', res.data.vehfecvensoa);
        setValue('vehfecrevtec', res.data.vehfecrevtec);
        setValue('vehfecfab', res.data.vehfecfab);
        setValue('vehcol', res.data.vehcol);        
        setValue('vehtitpro', res.data.vehtitpro);        
        setValue('vehfecvenpol', res.data.vehfecvenpol);        
        setValue('vehcanpas', res.data.vehcanpas);        
        setValue('vehcomtip', res.data.vehcomtip);        
        setValue('vehton', res.data.vehton);        
        setValue('vehtip', res.data.vehtip);        
        setValue('vehmar', res.data.vehmar);        
        setValue('vehmod', res.data.vehmod);        
        setValue('vehkil', res.data.vehkil);        
        setValue('vehobs', res.data.vehobs);        
        setValue('vehnumser', res.data.vehnumser);        
        setValue('vehnummot', res.data.vehnummot);        
        setValue('vehcaruti', res.data.vehcaruti);
      } catch (error) {
        toast.error(`Error al cargar el vehiculo`);
      }
    };
    async function loadEstReg() {
      try {
        const res = await getEstadoRegistro('Inactivo');
        if (res === null) {
          toast.warning('El estado de registro Inactivo no existe en la Base de datos');
        } else {
          setValue('vehestreg', res);
          setEstReg(res);
        }
      } catch (error) {
        toast.error(`Error al cargar los estados`);
      }
    };
    loadVehiculo();
    loadEstReg();
  },[vehiculoId]);
    
  return (
    <>
    <h3>¿Está seguro que quiere eliminar el vehiculo "{vehiculo.vehpla}"?</h3>
    <form onSubmit={onSubmit}>
      <label>
        Estado de vehiculo
        <select className='input-select' {...register('vehestreg', { required: true })}>
          <option value={estReg} >Inactivo</option>
        </select>
        {errors.vehestreg?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
      </label>
      {/* Botones de cancelar/eliminar */}
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-eliminar' type='submit'>Eliminar</button>
      </div>
    </form>
    </>
  )
}

export {FormDeleteVehiculo};
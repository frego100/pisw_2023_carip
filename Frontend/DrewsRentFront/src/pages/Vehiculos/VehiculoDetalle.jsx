import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getVehiculoById } from '../../api/Vehiculo.api';
import { toast } from 'react-toastify';
import { BsCarFrontFill, BsMapFill } from "react-icons/bs";


function VehiculoDetalle() {

  const params = useParams();
  /*Datos de la empresa seleccionada*/ 
  const [vehiculo, setVehiculo] = useState([]);

  const navigate = useNavigate();

  const handleRegresar = () => {
    navigate('/vehiculos');
  };
  const handleEditar = () => {
    navigate(`/vehiculos/${params.id}/edit`);
  };

  useEffect(() => {
    const loadVehiculo = async () => {
      try {
        const response = await getVehiculoById(params.id);
        setVehiculo(response.data);
      } catch (error) {
        toast.error(`Error al cargar vehiculo`);
      }
  };
  loadVehiculo();
  }, []);

  return (
    <div className='contenedor-componente'>
      <h2>Vehículo: {vehiculo.vehpla} </h2>
      <div className='contenedor-detalle'>
        <div className='detalle-subtitle'>
          <BsCarFrontFill color='green' className=''/>
          <h5>Datos del responsable:</h5>
        </div>
        <div className='detalle-datos'>    
          <h6>Placa</h6>
          <p> {vehiculo.vehpla} </p>
          <h6> Estado del vehículo</h6>
          <p> {vehiculo.vehestvehcod?.estvehdes} </p>
          <h6>Categoría</h6>
          <p> {vehiculo.vehcat} </p>
          <h6>Tipo</h6>
          <p> {vehiculo.vehtip} </p>
          <h6>Modelo</h6>
          <p> {vehiculo.vehmod} </p>
          <h6>Marca</h6>
          <p> {vehiculo.vehmar} </p> 
          <h6>Soat</h6>
          <p> {vehiculo.vehsoa} </p> 
          <h6>Fecha de vencimiento de soat</h6>
          <p> {vehiculo.vehfecvensoa} </p>
          <h6>Fecha de revisión técnica</h6>
          <p> {vehiculo.vehfecrevtec} </p>
          <h6>Fecha de fabricación</h6>
          <p> {vehiculo.vehfecfab} </p>
          <h6>Numero de serie</h6>
          <p> {vehiculo.vehnumser} </p>
          <h6>Numero de motor</h6>
          <p> {vehiculo.vehnummot} </p>
          <h6>Observaciones</h6>
          <p> {vehiculo.vehobs} </p>
        </div>
        <div className='detalle-subtitle'>
          <BsMapFill color='green' className=''/>
          <h5>Datos del proveedor al que pertenece:</h5>
        </div>
        <div className='detalle-datos'>
          <h6>Proveedor</h6>
          <p> {vehiculo.vehprocod?.prorazsoc} </p>
          <h6>Region</h6>
          <p> {vehiculo.vehprocod?.proubi.disprocod.proregcod.regnom} </p>
          <h6>Provincia</h6>
          <p> {vehiculo.vehprocod?.proubi.disprocod.pronom} </p>   
          <h6>Distrito</h6>
          <p> {vehiculo.vehprocod?.proubi.disprocod.proregcod.regnom} </p>
        </div>          
      </div>
      <div className='contenedor-btn-detalle'>
        <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
      </div>      
    </div>
  );
}

export default VehiculoDetalle;

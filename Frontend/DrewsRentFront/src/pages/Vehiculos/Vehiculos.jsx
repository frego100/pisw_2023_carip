import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { BsFillPlusCircleFill } from "react-icons/bs";
import vehiculoColumns from "../../Utils/ComponentesUtils/vehiculoColumns";
import { getAllVehiculosActivos } from '../../api/Vehiculo.api';
import { Table } from '../../components/Tabla/Table';
import  Modal  from '../../components/Modal';
import { FormDeleteVehiculo } from './FormDeleteVehiculo';
import { toast, ToastContainer } from 'react-toastify';
import '../../styles/ContenedorComp.css'
import '../../styles/Vehiculos.css'
import 'react-toastify/dist/ReactToastify.css';


function Vehiculos() {
  const navigate = useNavigate();

  /*Datos de Vehiculos para la tabla*/  
  const [vehiculos, setVehiculos] = useState([]);
  
  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);

  /*Estado del idVehiculoSeleccionado */
  const [selectedIdVehiculo, setSelectedIdVehiculo] = useState(null);

  /*Funciones */
  const handleCreate = () => {
    navigate('/vehiculos/create');
  };
  const handleEdit = (idVehiculo) => {
    navigate(`/vehiculos/${idVehiculo}/edit`);
  };
  const handleDelete = (idVehiculo) => {
    setSelectedIdVehiculo(idVehiculo);
    setActiveFormDelete(!activeFormDelete);
  };

  const loadVehiculos = async () => {
    try {
      const res = await getAllVehiculosActivos();
      setVehiculos(res);
    } catch (error) {
        toast.error(`Error al cargar los vehiculos`);
    }
  };

  useEffect(() => {
    loadVehiculos();
  }, []);

  return (
    <>
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Vehiculos</h2>
        <div className='btn-agregar' onClick={handleCreate}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de vehiculos */}
      <Table
        nombreID={'vehcod'}
        columns={vehiculoColumns()}
        data={vehiculos}
        nombre={'vehiculos'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={true}
      />
    </div>

    <ToastContainer />

    {/* Modal para editar proyecto existente */}
    <Modal toggle={handleEdit}>
    </Modal>
    
    {/* Modal para eliminar proyecto */}
    <Modal active={activeFormDelete} toggle={handleDelete}>
      {/* <FormDeleteProyecto toggle={handleDelete} proyectoId={selectedIdProyecto} loadProyectos={loadProyectos} />  */}
      <FormDeleteVehiculo toggle={handleDelete} vehiculoId={selectedIdVehiculo} loadVehiculos={loadVehiculos} />
    </Modal>
    </>
  )
}

export default Vehiculos;
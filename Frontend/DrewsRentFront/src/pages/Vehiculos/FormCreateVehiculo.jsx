import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createVehiculo } from '../../api/Vehiculo.api';
import { getAllProveedores } from '../../api/Proveedores.api';
import { getAllEstadoVeh } from '../../api/EstadoVehiculo.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

function FormCreateVehiculo(){

  const [proveedores,setProveedores] = useState([]);
  const [estadoveh, setEstadoVeh] = useState([]);
    
  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const navigate = useNavigate();

  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

  const validateSOAT = (value) => {
    return /^\d{24}$/.test(value);
  };

  const validatePlaca = (value) => {
    // En este ejemplo, se asume el formato: ABC 123 o AB 1234 o ABC-123
    const placaPattern = /^([A-Z]{3}\s?\d{3}|[A-Z]{2}\s?\d{4}|[A-Z]{3}-\d{3})$/i;
    return placaPattern.test(value);
  };

  const handleCancelar = () => {
    navigate('/vehiculos');
  };

  // Función llamada al enviar el formulario
  const onSubmit = handleSubmit(async data => {
    try {
      const res = await createVehiculo(data);
      navigate('/vehiculos');
      toast.success('Vehiculo creado con éxito');
    } catch (error) {
      toast.error(`Error al crear el vehiculo: ${error.message}`);
    }
  });

  const loadProveedores = async () => {
    try {
      const res = await getAllProveedores();
      setProveedores(res);
    } catch (error) {
      toast.error(`Error al cargar los proveedores`);
    }
  };

  const loadEstadoVehiculos = async () => {
    try {
      const res = await getAllEstadoVeh();
      setEstadoVeh(res);
    } catch (error) {
      toast.error(`Error al cargar los vehiculos: ${error.message}`);
    }
  };

  const loadEstReg = async () => {
    try {
      const resid = await getEstadoRegistro('Activo');
      if (resid === null) {
        toast.error(`El estado de registro Activo no existe en la Base de Datos`);
      } else {
        setValue('vehestreg', resid);
      }
    } catch (error) {
      toast.error(`Error al cargar el estado de registro`);
    }
  };

  useEffect(() => {
    loadEstReg();
    loadProveedores();
    loadEstadoVehiculos();
  }, []
  );

  return (
    <div className='contenedor-componente'>
      <h2>Registrar nuevo Vehiculo</h2>
      <div className='contenedor-editar'>
        <form onSubmit={onSubmit}>
          <div className='form-fila'>
            <label className='flex1'>
              Placa del Vehiculo
              <input
                className='input-text'
                type="text"
                placeholder='Placa del vehiculo'
                {...register('vehpla', { 
                  required: true,
                  validate: (value) => validatePlaca(value),
                })}
              />
              {errors.vehpla?.type === 'required' && <p className='text-error'>*El campo Placa es requerido</p>}
              {errors.vehpla?.type === 'validate' && <p className='text-error'>*El formato debe ser: 'ABC 123' , 'AB 1234' , 'ABC-123' o 'AB-1234'</p>}
            </label>
            <label className='flex1'>
              Categoria del Vehiculo
              <input
                className='input-text'
                type="text"
                placeholder='Categoria del vehiculo'
                {...register('vehcat', {
                  validate: (value) => validateMaxSize(value),
                })}
              />
              {errors.vehcat?.type === 'validate' && <p className='text-error'>*El campo Categoria no debe superar los 45 caracteres</p>}
            </label>
          </div>
          <div className='form-fila'>
            <label className='flex1'>
              Estado del Vehiculo
              <select className='input-select'
                  name="vehestvehcod"
                  {...register('vehestvehcod', { required: true })}
                  >
                  <option value='' >Seleccionar Estado del Vehiculo</option>
                  {estadoveh && estadoveh.map(estado =>(
                      <option value={estado.estvehcod} key={estado.estvehcod}>{estado.estvehdes}</option>
                  ))}
              </select>
              {errors.vehestvehcod?.type === 'required' && <p className='text-error'>*El campo Estado del Vehiculo es requerido</p>}  
            </label>
            <label className='flex1'>
              Proveedor
              <select className='input-select'
                  name="vehprocod"
                  {...register('vehprocod', { required: true })}
                  >
                  <option value='' >Seleccionar Proveedor</option>
                  {proveedores && proveedores.map(proveedor =>(
                      <option value={proveedor.procod} key={proveedor.procod}>{proveedor.prorazsoc}</option>
                  ))}
              </select>
              {errors.vehprocod?.type === 'required' && <p className='text-error'>*El campo Proveedor es requerido</p>}  
            </label>
          </div>
          <div className='form-fila'>
            <label className='flex1'>
              SOAT
              <input
                className='input-text'
                type="text"
                placeholder='SOAT'
                {...register('vehsoa', {
                  validate: (value) => validateSOAT(value),
                })}
              />
              {errors.vehsoa?.type === 'validate' && <p className='text-error'>*El campo SOAT debe tener 24 caracteres</p>}
            </label>
            <label className='flex1'>
              Fecha de vencimiento del SOAT
              <input
                className='input-text'
                type="date"
                {...register('vehfecvensoa', {required: true})}
              />
              {errors.vehfecvensoa?.type === 'required' && <p className='text-error'>*El campo fecha de vencimiento del soat es requerido</p>}
            </label>
          </div>
          <div className='form-fila'>
            <label className='flex1'>
              Tipo de Vehiculo
              <input
                className='input-text'
                type="text"
                placeholder='Tipo de Vehiculo'
                {...register('vehtip', { 
                  required: true,
                  validate: (value) => validateMaxSize(value),
                })}
              />
              {errors.vehtip?.type === 'required' && <p className='text-error'>*El campo Tipo de Vehiculo es requerido</p>}
              {errors.vehtip?.type === 'validate' && <p className='text-error'>*El campo Tipo de Vehiculo no debe superar los 45 caracteres</p>}
            </label>
            <label className='flex1'>
              Marca del Vehiculo
              <input
                className='input-text'
                type="text"
                placeholder='Marca del Vehiculo'
                {...register('vehmar', { 
                  required: true,
                  validate: (value) => validateMaxSize(value),
                })}
              />
                {errors.vehmar?.type === 'required' && <p className='text-error'>*El campo Marca del Vehiculo es requerido</p>}
                {errors.vehmar?.type === 'validate' && <p className='text-error'>*El campo Marca del Vehiculo no debe superar los 45 caracteres</p>}
            </label>
            <label className='flex1'>
              Modelo del Vehiculo
              <input
                  className='input-text'
                  type="text"
                  placeholder='Modelo del Vehiculo'
                  {...register('vehmod', { 
                      required: true,
                      validate: (value) => validateMaxSize(value),
                  })}
              />
              {errors.vehmod?.type === 'required' && <p className='text-error'>*El campo Modelo del Vehiculo es requerido</p>}
              {errors.vehmod?.type === 'validate' && <p className='text-error'>*El campo Modelo del Vehiculo no debe superar los 45 caracteres</p>}
            </label>
          </div>       
          <div className='form-fila'>
            <label className='flex1'>
              Fecha de Revision Tecnica
              <input
                className='input-text'
                type="date"
                {...register('vehfecrevtec', {required: true})}
              />
              {errors.vehfecrevtec?.type === 'required' && <p className='text-error'>*El campo Fecha de Revisión técnica es requerido</p>}
            </label> 
            <label className='flex1'>
              Fecha de Fabricacion
              <input
                className='input-text'
                type="date"
                {...register('vehfecfab', {required: true})}
              />
              {errors.vehfecfab?.type === 'required' && <p className='text-error'>*El campo Fecha de fabricación es requerido</p>}
            </label>
            <label className='flex1'>
              Fecha de Venc. de Poliza
              <input
                className='input-text'
                type="date"
                {...register('vehfecvenpol', {required: true})}
              />
              {errors.vehfecvenpol?.type === 'required' && <p className='text-error'>*El campo Fecha de vencimiento de poliza es requerido</p>}
            </label>
          </div>
          <div className='form-fila'>
            <label className='flex1'>
              Numero de Serie
              <input
                className='input-text'
                type="number"
                placeholder='Numero de Serie'
                {...register('vehnumser', {required: true})}
              />
              {errors.vehnumser?.type === 'required' && <p className='text-error'>*El campo numero de serie es requerido</p>}
            </label>
            <label className='flex1'>
              Numero de Motor
              <input
                className='input-text'
                type="number"
                placeholder='Numero de Motor'
                {...register('vehnummot', {required: true})}
              />
              {errors.vehnummot?.type === 'required' && <p className='text-error'>*El campo numero de motor es requerido</p>}
            </label>
            <label className='flex1'>
              Nr.Pasajeros
              <input
                  className='input-text'
                  type="number"
                  step="1"
                  placeholder='Nr. de Pasajeros'
                  {...register('vehcanpas', {required: true})}
              />
              {errors.vehcanpas?.type === 'required' && <p className='text-error'>*El campo numero de pasajeros es requerido</p>}
            </label>
          </div>
          <div className='form-fila'>
            <label className='flex1'>
              Tonelaje
              <input
                className='input-text'
                type="number"
                placeholder='Tonelaje del vehiculo'
                {...register('vehton', {required: true})}
              />
              {errors.vehton?.type === 'required' && <p className='text-error'>*El campo tonalaje es requerido</p>}
            </label>
            <label className='flex1'>
              Kilometraje
              <input
                className='input-text'
                step="1"
                type="number"
                placeholder='Kilometraje del vehiculo'
                {...register('vehkil', {required: true})}
              />
              {errors.vehkil?.type === 'required' && <p className='text-error'>*El campo kilometraje es requerido</p>}
            </label>
            <label className='flex1'>
              Carga Util del Vehiculo
              <input
                className='input-text'
                type="number"
                placeholder='Carga Util'
                {...register('vehcaruti', {required: true})}
              />
              {errors.vehcaruti?.type === 'required' && <p className='text-error'>*El campo carga util es requerido</p>}
            </label>
          </div>
          <div className='form-fila'>
            <label className='flex1'>
              Titulo de Propiedad
              <input
                className='input-text'
                type="text"
                placeholder='Titulo de Propiedad'
                {...register('vehtitpro', {
                  validate: (value) => validateMaxSize(value),
                })}
              />
              {errors.vehtitpro?.type === 'validate' && <p className='text-error'>*El campo Titulo de Propiedad no debe superar los 45 caracteres</p>}
            </label>
            <label className='flex1'>
              Tipo de Combustible
              <input
                className='input-text'
                type="text"
                placeholder='Tipo de Combustible'
                {...register('vehcomtip', {
                  validate: (value) => validateMaxSize(value),
                })}
              />
              {errors.vehcomtip?.type === 'validate' && <p className='text-error'>*El campo Tipo de Combustible no debe superar los 45 caracteres</p>}
            </label>
            <label className='flex1'>
              Color del Vehiculo
              <input
                className='input-text'
                type="text"
                placeholder='Color del vehiculo'
                {...register('vehcol', {
                  validate: (value) => validateMaxSize(value),
                })}
              />
              {errors.vehcol?.type === 'validate' && <p className='text-error'>*El campo Color no debe superar los 45 caracteres</p>}
            </label>
          </div>
          <label>
            Observaciones
            <input
              className='input-text'
              type="text"
              placeholder='Observaciones'
              {...register('vehobs', { 
                validate: (value) => validateMaxSize(value),
              })}
            />
            {errors.vehobs?.type === 'validate' && <p className='text-error'>*El campo Observaciones del Vehiculo no debe superar los 45 caracteres</p>}
          </label>
          <div className='contenedor-btn'>
              <button className='btn-cancelar' type='button' onClick={() => handleCancelar()}>Cancelar</button>
              <button className='btn-registrar' type='submit'>Registrar</button>
          </div>
        </form>
      </div>
    </div>
  )
}

export {FormCreateVehiculo};
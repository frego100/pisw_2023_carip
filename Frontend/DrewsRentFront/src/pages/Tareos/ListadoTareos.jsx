import React, { useState, useEffect, useCallback } from 'react'
import { Button, Modal, Form, Input, Select, Table, Space, DatePicker} from 'antd'

import 'react-datepicker/dist/react-datepicker.css';
import { IoAddCircleOutline, IoChevronUp, IoChevronDown } from "react-icons/io5";
import { MdEdit } from "react-icons/md";
import ReactPaginate from 'react-paginate';

import '../../styles/Tareos.css'
import '../../styles/General.css'
import '../../styles/ContenedorComp.css'

import { getAllLeyendas } from '../../api/LeyendaTare.api';
import { getByIdListaTareo } from "../../api/TareoLista.api";
import { getAllConteoTareoAlquiler, getAllConteoTareoAlquilerPaginated } from "../../api/ConteoLeyendaAlquiler.api";
import { async } from 'rxjs';
import { toast } from 'react-toastify';

function ListadoTareo({ allAlquileres, selectedRegion, selectedProject, selectedEmpresa, selectedResponsable }) {

    //PAGINACION
    const itemsPerPage = 10;
    const [currentPage, setCurrentPage] = useState(0);

    const filteredAlquileres = allAlquileres.filter((alquiler) =>
    (selectedRegion === "Todos" || alquiler.alqprocod.proregcod.regnom === selectedRegion) &&
    (selectedProject === "Todos" || alquiler.alqprocod.pronom === selectedProject) &&
    (selectedEmpresa === "Todos" || alquiler.alqrescod.resempcod.empnom === selectedEmpresa) &&
    (selectedResponsable === "Todos" || `${alquiler.alqrescod.resnom} ${alquiler.alqrescod.resape}` === selectedResponsable)
    );

    const pageCount = Math.ceil(filteredAlquileres.length / itemsPerPage);
    const currentAlquileres = filteredAlquileres.slice(currentPage * itemsPerPage, (currentPage + 1) * itemsPerPage);

    const handlePageClick = (selectedPage) => {
        setCurrentPage(selectedPage.selected);
    };
    /*para el detalle de alquileres*/
    const [selectedAlquiler, setSelectedAlquiler] = useState(null);

    /*para la tabla*/
    const [tareosLista, setSelectedTareoLista] = useState([]);

    //EDICION
    const [editData, setEditData] = useState({});
    const [modalEditVisible, setModalEditVisible] = useState(false);
    const [allLeyendaTareo, setAllLeyendaTareo] = useState([]);

    //CONTADOR
    const [allCountAlquileres, setAllCountAlquileres] = useState({});

    const [edicionExitosa, setEdicionExitosa] = useState(false);
    const [form] = Form.useForm();

    const handleOpenEditarModal = (data) =>{
        setEditData(data);
        setModalEditVisible(true);
    }
    const handleCloseEditarModal = () =>{
        setModalEditVisible(false);
    }
    const handleEditarGuardar = async () => {
        try {
            setModalEditVisible(false);
            toast.success('Edicion pendiente');
        } catch (error) {
            toast.error('Error en la edicion:', error);
            console.error('Resultado de la edicion:', error);
        }
    };

    useEffect(() => {
        //PARA EL CONTEO
        async function loadAllCountAlquileres() {
            // const allCountAlquileres = await getAllConteoTareoAlquiler();
            // setAllCountAlquileres(allCountAlquileres);            
            const allCountAlquileres = await getAllConteoTareoAlquilerPaginated();
            setAllCountAlquileres(allCountAlquileres);

            const buffer = {}

            // while(allCountAlquileres.next){
            //     const nextdata = fetch(allCountAlquileres.next).then(data => data);
            //     buffer = {...buffer, }
            // }
            
            // console.log(fetch(allCountAlquileres.next).then(data => data).then(data => data.json() ));

        }

        async function loadLeyenda() {
            const allLeyendaTareo = await getAllLeyendas();
            setAllLeyendaTareo(allLeyendaTareo);
        }

        if (edicionExitosa) {
            setEdicionExitosa(false);
        }

        loadAllCountAlquileres();
        loadLeyenda();
    }, [edicionExitosa]);

    useEffect(() => {
        form.setFieldsValue({
            fecha: editData.fecha,
            descripcion: editData.LeyTarDes,
        });
    }, [modalEditVisible, editData, form]);

    function getConteoPorAlquiler(alquilerId) {
        return allCountAlquileres.find(conteo => conteo.alquiler_id === alquilerId);
    }

    const loadTareoListaById = async (alquiler) => {
        if (alquiler === null) {
            setSelectedTareoLista([]);
            return;
        }
        const tareosLista = await getByIdListaTareo(alquiler);
        //console.log(tareosLista);
        setSelectedTareoLista(tareosLista);
    }

    const handleClick = (alquiler) => {
        // Si el vehículo actualmente seleccionado es el mismo que se hizo clic, ciérralo
        if (selectedAlquiler === alquiler ) {
            setSelectedAlquiler(null);
        } else {
            // De lo contrario, selecciona el nuevo vehículo
            setSelectedAlquiler(alquiler);
            loadTareoListaById(alquiler);
        }
    };

    const handleChildClick = (e) => {
        e.stopPropagation(); // Detiene la propagación del evento de clic
    };

    const columns = [
        {
            title: 'Nº',
            dataIndex: 'tareo_id',
            key: 'tareo_id',
            render: (text, record, index) => index + 1,
        },
        {
            title: 'Fecha',
            dataIndex: 'fecha',
            key: 'fecha',
        },
        {
            title: 'Estado del Vehículo',
            dataIndex: 'leytardes',
            key: 'leytardes',
        },
        {
            title: 'Acciones',
            key: 'acciones',
            render: (text, data) => (
                <Space size="middle">
                    <MdEdit onClick={() => handleOpenEditarModal(data)} className='icono-editar-modal'/>
                </Space>
            ),
        },
    ];

    return (
        <div className="vehiculos-lista-container">
        {currentAlquileres.map((alquiler) => (    
            <div key={alquiler.alqcod} className="all-vehiculos-container">
                <div  onClick={() => handleClick(alquiler.alqcod)} className="vehiculo-container">
                    <div className="vehiculo-title-container">
                        <div className="vehiculo-title-texto-container">
                            <img src="/src/assets/GreenIcons/VehiculoIcon.png"/>
                            <h3 className='texto-detalles'>Placa:</h3>
                            <h3 className='texto-back'>{alquiler.alqvehcod.vehpla}</h3>
                        </div>
                        <div className="vehiculo-title-texto2-container">
                            <img src="/src/assets/GreenIcons/OperadorIcon.png"/>
                            <h3 className='texto-detalles'>Operador:</h3>
                            <h3 className='texto-back'>{alquiler.alqopecod.openom} {alquiler.alqopecod.opeape}</h3>
                        </div>

                        <div className="vehiculo-title-dias-container">
                            <div className='divDT'><p>DT = {getConteoPorAlquiler(alquiler.alqcod)?.dt || 0} </p></div>
                            <div className='divDNT'><p>DNT = {getConteoPorAlquiler(alquiler.alqcod)?.dnt || 0} </p></div>
                            <div className='divMP'><p>MP = {getConteoPorAlquiler(alquiler.alqcod)?.mp || 0} </p></div>
                            <div className='divMC'><p>MC = {getConteoPorAlquiler(alquiler.alqcod)?.mc || 0} </p></div>
                            <div className='divTSI'><p>TSI = {getConteoPorAlquiler(alquiler.alqcod)?.tsi || 0} </p></div>
                        </div>
                        
                        <div className="vehiculo-title-icon-container">
                            {selectedAlquiler === alquiler.alqcod ? (
                                    <IoChevronUp className='icono-updown-detalles'/>
                                ) : (
                                    <IoChevronDown className='icono-updown-detalles'/>
                                )
                            }
                        </div>
                        
                    </div>

                    {(selectedAlquiler === alquiler.alqcod && (
                        <div className="vehiculo-detalles-container">
                            <div className="vehiculo-detalles-texto-container">
                                <div className="row">
                                    <div className="row-detalle">
                                        <img src="/src/assets/GreenIcons/CalendarioIcon.png"/>
                                        <p className='texto-detalles'>Fecha inicio de alquiler: </p>
                                        <p className='texto-back'>{alquiler.alqfecini}</p>
                                    </div>
                                    <div className="row-detalle">
                                        <img src="/src/assets/GreenIcons/CalendarioIcon.png"/>
                                        <p className='texto-detalles'>Fecha fin de alquiler: </p>
                                        <p className='texto-back'>{alquiler.alqfecfin}</p>
                                    </div>
                                    <div className="row-detalle">
                                        <img src="/src/assets/GreenIcons/UbicacionIcon.png"/>
                                        <p className='texto-detalles'>Region: </p>
                                        <p className='texto-back'>{alquiler.alqprocod.proregcod.regnom}</p>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="row-detalle">
                                        <img src="/src/assets/GreenIcons/VehiculoIcon.png"/>
                                        <p className='texto-detalles'>Modelo: </p>
                                        <p className='texto-back'>{alquiler.alqvehcod.vehmod}</p>
                                    </div>
                                    <div className="row-detalle">
                                        <img src="/src/assets/GreenIcons/EmpresaIcon.png"/>
                                        <p className='texto-detalles'>Empresa: </p>
                                        <p className='texto-back'>{alquiler.alqrescod.resempcod.empnom}</p>
                                    </div><div className="row-detalle">
                                        <img src="/src/assets/GreenIcons/UsuarioIcon.png"/>
                                        <p className='texto-detalles'>Responsable: </p>
                                        <p className='texto-back'>{alquiler.alqrescod.resnom} {alquiler.alqrescod.resape}</p>
                                    </div>
                                </div>

                                <div onClick={handleChildClick} className="vehiculo-detalles-calendar-container">
                                    <div className='vehiculo-detalles-table-container'>
                                        <Table className="tabla-tareos" dataSource={tareosLista} columns={columns} />
                                            <div >
                                                <Modal
                                                    forceRender
                                                    title={`Edicion de Tareo de la unidad ${alquiler.alqvehcod.vehpla}`}
                                                    open={modalEditVisible}
                                                    onCancel={handleCloseEditarModal}
                                                    onOk={handleEditarGuardar}
                                                    footer={[
                                                    <div className='button-modal-container'>
                                                        <Button className='button-modal-agregar' onClick={handleEditarGuardar} >Guardar</Button>
                                                        <Button onClick={handleCloseEditarModal}>Cancelar</Button>
                                                    </div>
                                                    ]}
                                                    centered
                                                    width={420} 
                                                    >
                                                    <div style={{ display: 'flex', justifyContent: 'center' }}>
                                                    <Form
                                                        form={form}
                                                        labelCol={{ span: 18 }}
                                                        wrapperCol={{ span: 14 }}
                                                        layout="vertical"
                                                        >
                                                        <Form.Item style={{ marginLeft:'10px', marginBottom: '5px' }} label='Fecha' labelCol={{ style: { marginBottom: '-30px' } }}>
                                                            <DatePicker
                                                                style={{ width: '250px' }}
                                                                getCalendarContainer={(trigger) => trigger.parentNode}
                                                                placement="topLeft"
                                                                onChange={(date) => {
                                                                    if (date) {
                                                                        const formattedDate = date.format('YYYY-MM-DD');
                                                                        setEditData(prevState => ({ ...prevState, fecha: formattedDate }));
                                                                    }
                                                                }}
                                                                name='fecha'
                                                            />
                                                        </Form.Item >
                                                        <Form.Item label="Estado del vehiculo" name="descripcion" style={{ marginLeft:'10px', marginBottom: '5px' }}  labelCol={{ style: { marginBottom: '-30px' } }} >
                                                            <Select 
                                                                style={{ width: '250px' }}
                                                                onChange={(descripcion) => setEditData({ ...editData, LeyTarDes: descripcion })}
                                                            >
                                                                {allLeyendaTareo.map((leyenda) => (
                                                                    <Select.Option key={leyenda.leytardes} value={leyenda.leytardes}>
                                                                    {leyenda.leytardes}
                                                                    </Select.Option>
                                                                ))}
                                                            </Select>
                                                        </Form.Item>
                                                    </Form>
                                                    </div>
                                                </Modal>
                                            </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                        )
                    )}

                </div>
            </div>
        ))}

        <ReactPaginate
            previousLabel={<span className="arrow">&lt;</span>}
            nextLabel={<span className="arrow">&gt;</span>}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={pageCount}
            pageLinkClassName={'page-number'}
            marginPagesDisplayed={1}
            pageRangeDisplayed={3}
            onPageChange={handlePageClick}
            containerClassName={'pagination'}
            activeClassName={'active'}
            nextClassName={'next'}
        />

    </div>
    );
}

export default ListadoTareo
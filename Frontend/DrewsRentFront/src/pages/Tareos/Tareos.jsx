import React, { useState, useEffect, useCallback } from 'react'

import 'react-datepicker/dist/react-datepicker.css';
import { IoAddCircleOutline, IoChevronUp, IoChevronDown } from "react-icons/io5";
import { MdEdit } from "react-icons/md";
import { useNavigate } from 'react-router-dom';

import '../../styles/Tareos.css'
import '../../styles/General.css'
import '../../styles/ContenedorComp.css'

import { getAllProjectNames } from '../../api/Proyecto.api';
import { getAllNamesEmpresas } from '../../api/Empresa.api';
import { getAllNamesResponsable } from '../../api/Responsables.api';
import { getAllLeyendas } from '../../api/LeyendaTare.api';
import { getAllRegionNames } from "../../api/Region.api";
import { getAllAlquileres } from "../../api/Alquiler.api";
import { getAllConteoTareoAlquiler } from "../../api/ConteoLeyendaAlquiler.api";
import { async } from 'rxjs';
import { toast } from 'react-toastify';
import ListadoTareos from "./ListadoTareos";

function Tareo() {
    
    const navigate = useNavigate();

    /*para los filtros*/
    const [selectedProject, setSelectedProject] = useState("Todos");
    const [selectedEmpresa, setSelectedEmpresa] = useState("Todos");
    const [selectedResponsable, setSelectedResponsable] = useState("Todos");
    const [selectedRegion, setSelectedRegion] = useState("Todos");

    /*para los filtros*/
    const [allRegion, setRegion] = useState([]);
    const [projectNames, setProjectNames] = useState([]);
    const [allresponsables, setResponsables] = useState([]);
    const [allEmpresas, setEmpresas] = useState([]);

    /*para el listado*/
    const [allAlquileres, setAllAlquileres] = useState([]);

    useEffect(() => {
        async function loadRegion() {
            const allRegion = await getAllRegionNames();
            setRegion(allRegion);
        }

        async function loadProjectNames() {
            const projectNames = await getAllProjectNames();
            setProjectNames(projectNames);
        }

        async function loadResponsables() {
            const allresponsables = await getAllNamesResponsable();
            setResponsables(allresponsables);
        }

        async function loadEmpresas() {
            const allEmpresas = await getAllNamesEmpresas();
            setEmpresas(allEmpresas);
        }
        
        async function loadAllAlquileres() {
            const allAlquileres = await getAllAlquileres();
            setAllAlquileres(allAlquileres);
        }

        loadProjectNames();
        loadResponsables();
        loadEmpresas();
        loadRegion();
        loadAllAlquileres();
    }, []);

    function getConteoPorAlquiler(alquilerId) {
        return allCountAlquileres.find(conteo => conteo.alquiler_id === alquilerId);
    }

    const loadTareoListaById = async (alquiler) => {
        if (alquiler === null) {
            setSelectedTareoLista([]);
            return;
        }
        const tareosLista = await getByIdListaTareo(alquiler);
        console.log(tareosLista);
        setSelectedTareoLista(tareosLista);
    }

    const loadConteoTareoListaById = async (alquiler) => {
        const conteotareosLista = await getByIdConteoTareoAlquiler(alquiler);
        console.log(conteotareosLista);
    }

    const handleClick = (alquiler) => {
        // Si el vehículo actualmente seleccionado es el mismo que se hizo clic, ciérralo
        if (selectedAlquiler === alquiler ) {
            setSelectedAlquiler(null);
        } else {
            // De lo contrario, selecciona el nuevo vehículo
            setSelectedAlquiler(alquiler);
            loadTareoListaById(alquiler);
        }
    };

    const handleChildClick = (e) => {
        e.stopPropagation(); // Detiene la propagación del evento de clic
    };

    const handleExportar = () => {
        navigate('/tareos/exportar');
    };

    const columns = [
        {
            title: 'Nº',
            dataIndex: 'id',
            key: 'id',
            render: (text, record, index) => index + 1,
        },
        {
            title: 'Fecha',
            dataIndex: 'fecha',
            key: 'fecha',
        },
        {
            title: 'Estado del Vehículo',
            dataIndex: 'LeyTarDes',
            key: 'LeyTarDes',
        },
        {
            title: 'Acciones',
            key: 'acciones',
            render: (text, dataEdit) => (
                <Space size="middle">
                    <MdEdit onClick={() => handleOpenEditarModal(dataEdit)} className='icono-editar-modal'/>
                </Space>
            ),
        },
    ];


    return (
    <div className='contenedor-componente'>
        <div className='contenedor-titulo'>
            <h2>Tareos de Unidades</h2>
            <button className='btn-filtro' onClick={handleExportar}>Exportar Tareos</button>
        </div>
        <div className='contenedor-filtro-tareos'>
            <div className='container_text'>
                <h4>Region:</h4>
            </div>
            <div className='container_filter'>
                <select value={selectedRegion} onChange={(e) => setSelectedRegion(e.target.value)} className='list-city_controller'>
                    <option value="Todos">Todos</option>
                    {allRegion && allRegion.map((region) => (
                        <option key={region} value={region}>
                            {region}
                        </option>
                    ))}
                </select>
            </div>
            <div className='container_text'>
                <h4>Proyecto:</h4>
            </div>
            <div className='container_filter'>
                <select value={selectedProject} onChange={(e) => setSelectedProject(e.target.value)} className='list-city_controller'>
                <option value="Todos">Todos</option>
                    {projectNames && projectNames.map((projectName) => (
                        <option key={projectName} value={projectName}>
                            {projectName}
                        </option>
                    ))}
                </select>
            </div>
            
            <div className='container_text'>
                <h4>Empresas:</h4>
            </div>
            <div className='container_filter'>
                <select value={selectedEmpresa} onChange={(e) => setSelectedEmpresa(e.target.value)} className='list-city_controller'>
                    <option value="Todos">Todos</option>
                    {allEmpresas && allEmpresas.map((nombreEmpresas) => (
                        <option key={nombreEmpresas} value={nombreEmpresas}>
                            {nombreEmpresas}
                        </option>
                    ))}
                </select>
            </div>
            
            <div className='container_text'>
                <h4>Responsable:</h4>
            </div>
            <div className='container_filter'>
                <select value={selectedResponsable} onChange={(e) => setSelectedResponsable(e.target.value)} className='list-city_controller'>
                <option value="Todos">Todos</option>
                    {allresponsables && allresponsables.map((responsableName) => (
                        <option key={responsableName} value={responsableName}>
                            {responsableName}
                        </option>
                    ))}
                </select>
            </div>
        </div>

        <div className="table-container-tareos">
            <h3>Vehiculos</h3>
            <ListadoTareos allAlquileres={allAlquileres} selectedRegion={selectedRegion} 
            selectedProject={selectedProject} selectedEmpresa={selectedEmpresa} selectedResponsable={selectedResponsable}/>
        
        </div>
    </div>
    );
}

export default Tareo
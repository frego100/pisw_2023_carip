import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getAllProyectos } from '../../api/Proyecto.api';
import { exportarTareos } from '../../api/ExportarTareo.api';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

function ExportTareos() {
    const [proyectos, setProyectos] = useState([]);
    const [proyectosSeleccionados, setProyectosSeleccionados] = useState([]);
    const [selectedOptionFecha, setSelectedOptionFecha] = useState('todasFechas');
    const [selectedOptionProyecto, setSelectedOptionProyecto] = useState('todosProyectos');

    const {
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        getValues,
    } = useForm();

    const navigate = useNavigate();

    const handleCancelar = () => {
        navigate('/tareos');
    };

    const onSubmit = handleSubmit(async data => {
        try {
            const dataToSend = {
                selectedOptionFecha,
                fechaInicio: selectedOptionFecha === 'fechasEspecificas' ? getValues('fechaInicio') : '',
                fechaFin: selectedOptionFecha === 'fechasEspecificas' ? getValues('fechaFin') : '',
                selectedOptionProyecto,
                proyectosSeleccionados: selectedOptionProyecto === 'proyectosEspecificos' ? proyectosSeleccionados : []
            };
            const blobData = await exportarTareos(dataToSend);

            // Utilizar Axios para manejar la descarga del archivo
            const url = window.URL.createObjectURL(new Blob([blobData]));
            const a = document.createElement('a');
            a.href = url;
            a.download = 'archivo_excel.xlsx';
            a.click();

            // Limpiar recursos después de la descarga
            window.URL.revokeObjectURL(url);

            toast.success('Tareos exportado a Excel con éxito');
        } catch (error) {
            toast.error(`Error al exportar los tareos: ${error.message}`);
        }
    });

    const loadProyectos = async () => {
        try {
            const res = await getAllProyectos();
            setProyectos(res);
        } catch (error) {
            toast.error(`Error al cargar los proyectos: ${error.message}`);
        }
    };

    useEffect(() => {
        loadProyectos();
    }, []);

    const agregarProyecto = proyecto => {
        if (proyecto){
            setProyectosSeleccionados([...proyectosSeleccionados, proyecto]);
            setProyectos(proyectos.filter(p => p.procod !== proyecto.procod));
            // Actualizar el valor del select
            setValue('exportarProyecto', ''); // Limpiar el valor actual
        }
    };

    const quitarProyecto = proyecto => {
        setProyectosSeleccionados(proyectosSeleccionados.filter(p => p.procod !== proyecto.procod));
        setProyectos([...proyectos, proyecto]);
    };

    const validateFechaFin = value => {
        return value >= getValues('fechaInicio');
    };

    const selectedProyectoNotNull = value => {
        return proyectosSeleccionados.length !== 0;
    };
    

    const styles = {
        flexContainer: {
          display: 'flex',
          justifyContent: 'space-between', // Otra opción puede ser 'space-around' según tus preferencias
        },
        flexItem: {
          flex: '1', // Esto hace que cada elemento ocupe todo el espacio disponible
          margin: '0 5px',
        },
        separador: {
            color: '#000000',
            fontWeight: 'bold',
        },
      };

  return (
    <div className='contenedor-componente'>
      <h2>Exportar los Tareos a Excel</h2>
      <div className='contenedor-detalle'>
        <form onSubmit={onSubmit}>            
            <h3>Exportar por Fecha:</h3>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                        <h4>
                            <input
                                    type="radio"
                                    value="todasFechas"
                                    checked={selectedOptionFecha === 'todasFechas'}
                                    onChange={() => setSelectedOptionFecha('todasFechas')}
                                />Todas las fechas
                        </h4>
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                        <h4>
                            <input
                                type="radio"
                                value="fechasEspecificas"
                                checked={selectedOptionFecha === 'fechasEspecificas'}
                                onChange={() => setSelectedOptionFecha('fechasEspecificas')}
                            />
                            Seleccionar Fechas
                        </h4>
                    </label>
                </div>
            </div>


            {selectedOptionFecha === 'fechasEspecificas' && (
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                        Fecha de Inicio
                        <input
                            className='input-text'
                            type="date"
                            {...register('fechaInicio',{
                                required: true,
                            })}
                        />
                        {errors.fechaInicio?.type === 'required' && <p className='text-error'>*El campo Fecha Inicio es requerido</p>}
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                        Fecha de Fin
                        <input
                            className='input-text'
                            type="date"
                            {...register('fechaFin',
                            {
                                required: true,
                                validate: (value) => validateFechaFin(value),
                            })}
                        />
                        {errors.fechaFin?.type === 'required' && <p className='text-error'>*El campo Fecha Fin es requerido</p>}
                        {errors.fechaFin?.type === 'validate' && <p className='text-error'>*La fecha de Fin debe ser despues dela Fecha de Inicio</p>}
                    </label>
                </div>
            </div>
            )}


            <h3>Exportar por Proyecto:</h3>
            <div style={styles.flexContainer}>
                <div style={styles.flexItem}>
                    <label>
                        <h4>
                            <input
                                type="radio"
                                value="todosProyectos"
                                checked={selectedOptionProyecto === 'todosProyectos'}
                                onChange={() => setSelectedOptionProyecto('todosProyectos')}
                            />
                            Todos los proyectos
                        </h4>
                    </label>
                </div>
                <div style={styles.flexItem}>
                    <label>
                        <h4>
                            <input
                                type="radio"
                                value="proyectosEspecificos"
                                checked={selectedOptionProyecto === 'proyectosEspecificos'}
                                onChange={() => setSelectedOptionProyecto('proyectosEspecificos')}
                            />
                            Seleccionar Proyectos
                        </h4>
                    </label>
                </div>
            </div>
            

            {selectedOptionProyecto === 'proyectosEspecificos' && (
            <div>
                <div>
                <label>
                    Proyectos de los Tareos a exportar:
                    <select
                        className='input-select'
                        name="proyectos"
                        {...register('exportarProyecto',
                        {
                            validate: selectedProyectoNotNull,
                        })}
                        
                    >
                        <option value='' >Seleccionar Proyecto</option>
                        {proyectos.map(proyecto => (
                            <option value={proyecto.procod} key={proyecto.procod}>
                                {proyecto.pronom}
                            </option>
                        ))}
                    </select>
                    {errors.exportarProyecto?.type === 'validate' && <p className='text-error'>*Debe haber por lo menos un proyecto Seleccionado</p>}
                    <button
                        type="button"
                        onClick={() => agregarProyecto(proyectos.find(p => p.procod === parseInt(getValues('exportarProyecto'))))}
                    >
                        Agregar
                    </button>
                </label>
                </div>
                <div>
                    Proyectos seleccionados:
                    {proyectosSeleccionados.map(proyecto => (
                        <div key={proyecto.procod}>
                            {proyecto.pronom}
                            <button type="button" onClick={() => quitarProyecto(proyecto)}>
                                X
                            </button>
                        </div>
                    ))}
                </div>
                </div>
            )}

                <div className='contenedor-btn'>
                    <button className='btn-cancelar' type='button' onClick={() => handleCancelar()}>Cancelar</button>
                    <button className='btn-registrar' type='submit'>Exportar</button>
                </div>
        </form>
      </div>
    </div>
  );
}

export { ExportTareos };
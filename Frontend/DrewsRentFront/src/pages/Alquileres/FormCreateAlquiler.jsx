import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { getAllProyectos } from '../../api/Proyecto.api';
import { getAllResponsables, getResponsableById } from '../../api/Responsables.api';
import { getAllVehiculos } from '../../api/Vehiculo.api';
import { getAllOperadores } from '../../api/Operador.api';
import { getAllEstadosAlquiler } from '../../api/EstadoAlquiler.api';
import { createAlquiler } from '../../api/Alquiler.api';
import { toast } from 'react-toastify';


function FormCreateAlquiler({toggle, loadAlquileres}) {

  /* Datos de proyectos*/
  const [proyectos, setProyectos] = useState([]);
  /* Datos de responsables*/
  const [responsables, setResponsables] = useState([]);
  /* Datos de operadores*/
  const [operadores, setOperadores] = useState([]);
  /* Datos de vehiculos*/
  const [vehiculos, setVehiculos] = useState([]);
  /* Datos de los estados del alquiler*/
  const [estadosAlq, setEstadosAlq] = useState([]);

  const [responsableId, setResponsableId] = useState(0);
  //const [responsable, setResponsable] = useState([]);
  const [area, setArea] = useState('');
  const [empresa, setEmpresa] = useState('');

  
  /*Estado de los registros del formulario*/
  const {
    register,
    handleSubmit, 
    setValue,
    getValues,
    formState: { errors },
  } = useForm({
    defaultValues: {
      alqrescod: 0,
      alqfecini: '',
      alqfecfin: '',
    },
  });

  const navigate = useNavigate();

  // funcion llamada al enviar el formualrio
  const onSubmit = handleSubmit(async data => {
    if (data.alqfecini) {
      setValue('alqfecini', formatDate(data.alqfecini));
    }
    if (data.alqfecfin) {
      setValue('alqfecfin', formatDate(data.alqfecfin));
    }
    try {
      const response = await createAlquiler(data);
      toggle();
      toast.success('Alquiler creado con éxito');
      loadAlquileres();       
    } catch (error) {
      toast.error(`Error al crear el alquiler: ${error.message}`);
    }
  });

  const handleChangeResponsable = async (event) => {
    const resId = event.target.value;
    setValue('alqrescod', resId);
    setResponsableId(resId);
    const responsable = await getResponsableById(resId);
    if (responsable.data) {
      setArea(responsable.data.resarecod.arenom || '');
      setEmpresa(responsable.data.resempcod.emprazsoc || '');
    }
  };

  // Función para formatear la fecha al formato "YYYY-MM-DD"
  const formatDate = (dateString) => {
    const date = new Date(dateString);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
  };

  const loadProyectos = async () => {
    try {
      const response = await getAllProyectos();
      setProyectos(response);      
    } catch (error) {
      toast.error('Error al cargar proyectos');
    }
  };
  const loadResponsables  = async () => {
    try {
      const response = await getAllResponsables();
      setResponsables(response);      
    } catch (error) {
      toast.error('Error al cargar responsables');
    }
  };
  const loadVehiculos  = async () => {
    try {
      const response = await getAllVehiculos();
      setVehiculos(response);      
    } catch (error) {
      toast.error('Error al cargar vehiculos');
    }
  };
  const loadOperadores  = async () => {
    try {
      const response = await getAllOperadores();
      setOperadores(response);
    } catch (error) {
      toast.error('Error al cargar operadores');
    }
  };
  const loadEstadosAlquiler  = async () => {
    try {
      const response = await getAllEstadosAlquiler();
      setEstadosAlq(response);       
    } catch (error) {
      toast.error('Error al cargar estado de alquiler');
    }
  };

  // Cargar datos de la api
  useEffect(() => {
    loadProyectos();
    loadResponsables();
    loadOperadores();
    loadVehiculos();
    loadEstadosAlquiler();  
    }, []
  );

  // Renderizar
  return (
    <>
    <h2>Registrar nuevo alquiler</h2>
    <form onSubmit={onSubmit}>
      <label>
        Proyecto
        <select
          className='input-select'
          {...register('alqprocod', { required: true })}
        >
          <option value=''> Seleccionar proyecto</option>
          {proyectos && proyectos.map(proyecto=>(
            <option value={proyecto.procod} key={proyecto.procod}>
              {proyecto.pronom}
            </option>
          ))}
        </select>
        {errors.alqprocod?.type === 'required' && <p className='text-error'>*El campo proyecto es requerido</p>}
      </label>
      <div className='form-fila'>
        <label className='flex1'>
          Responsable
          <select
            className='input-select'
            name='responsables'
            onChange={handleChangeResponsable}
            //{...register('alqrescod', { required: true })}
          >
            <option value=''> Seleccionar responsable</option>
            {responsables && responsables.map(responsable=>(
              <option value={responsable.rescod} key={responsable.rescod}>
                {responsable.resnom}
              </option>
            ))}
          </select>
          {errors.alqrescod?.type === 'required' && <p className='text-error'>*El campo responsable es requerido</p>}
        </label>
        <label className='flex1'>
          Area
          <input
            className='input-text'
            type="text"
            value={area} //getResponsableById(getValues('alqrescod')).data.resarecod.arenom || ''
            readOnly
          />
        </label>
        <label className='flex1'>
          Empresa
          <input
            className='input-text'
            type="text"
            value={empresa}
            readOnly
          />
        </label>
      </div>
      <label>
        Vehículo
        <select
          className='input-select'
          {...register('alqvehcod', { required: true })}
        >
          <option value=''> Seleccionar vehículo</option>
          {vehiculos && vehiculos.map(vehiculo=>(
            <option value={vehiculo.vehcod} key={vehiculo.vehcod}>
              {vehiculo.vehpla}
            </option>
          ))}
        </select>
        {errors.alqvehcod?.type === 'required' && <p className='text-error'>*El campo vehículo es requerido</p>}
      </label>

      <label>
        Operador
        <select
          className='input-select'
          {...register('alqopecod', { required: true })}
        >
          <option value=''> Seleccionar operador</option>
          {operadores && operadores.map(operador=>(
            <option value={operador.opecod} key={operador.opecod}>
              {operador.openom}
            </option>
          ))}
        </select>
        {errors.alqopecod?.type === 'required' && <p className='text-error'>*El campo operador es requerido</p>}
      </label> 

      <label>
        Estado de alquiler
        <select
          className='input-select'
          {...register('alqestalqcod', { required: true })}
        >
          <option value=''> Seleccionar estado de alquiler</option>
          {estadosAlq && estadosAlq.map(estadoAlq=>(
            <option value={estadoAlq.estalqcod} key={estadoAlq.estalqcod}>
              {estadoAlq.estalqdes}
            </option>
          ))}
        </select>
        {errors.alqestalqcod?.type === 'required' && <p className='text-error'>*El campo estado de alquiler es requerido</p>}
      </label>

      <label>
        Fecha Inicio
        <input className='input-text' type="date" {...register('alqfecini', { required: true })} />
        {errors.alqfecini?.type === 'required' && <p className='text-error'>*El campo fecha inicio es requerido</p>}
      </label>

      <label>
        Fecha Fin
        <input className='input-text' type="date" {...register('alqfecfin', { required: true })} />
        {errors.alqfecfin?.type === 'required' && <p className='text-error'>*El campo fecha fin es requerido</p>}
      </label>

      <label>
        Observaciones
        <input
          className='input-text'
          type="text"
          placeholder='Observaciones'
          {...register('alqobs')}
        />
      </label>

      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-registrar' type='submit'>Registrar</button>
      </div>

    </form>
    </>
  )
}

export { FormCreateAlquiler };
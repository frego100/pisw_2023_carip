import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllAlquileres } from '../../api/Alquiler.api';
import alquilerColumns from '../../Utils/AlquilerUtils/alquilerColumns';
import Modal from '../../components/Modal';
import { TableStyle2 } from '../../components/Tabla/TableStyle2';
import { FormCreateAlquiler } from './FormCreateAlquiler';
import { DateRangeFilter } from './DateRangeFilter';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../../styles/ContenedorComp.css';


function Alquileres() {

  /*Datos de proyectos para la tabla*/    
  const [alquileres, setAlquileres] = useState([]);

  /* Nuevos estados para el filtro por rango de fecha */
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  /* Estado del formulario para crear y eliminar*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);

  /*Estado del idAlquiler Seleccionado */
  const {selectedIdAlquiler, setSelectedIdAlquiler} = useState();

  /* Manejar el estado de los formularios*/
  const handleCreate = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  
  const handleDelete = (idAlquiler) => {
    setSelectedIdAlquiler(idAlquiler);
    setActiveFormDelete(!activeFormDelete);
  };
  // Función para actualizar las fechas del rango
  const handleDateFilter = (start, end) => {
    setStartDate(start);
    setEndDate(end);
  };

  const loadAlquileres = async () => {
    try {
      const res = await getAllAlquileres();
      // Filtrar por fecha si hay fechas seleccionadas
      if (startDate && endDate) {
        const filtered = res.filter(
          (alquiler) =>
            new Date(alquiler.alqfecini) >= startDate &&
            new Date(alquiler.alqfecfin) <= endDate
        );
        setAlquileres(filtered);
      } else {
        setAlquileres(res);
      }
    } catch (error) {
      toast.error(`Error al cargar los alquileres: ${error.message}`);
    }
  };

  useEffect(() => {
    loadAlquileres();
  }, [startDate, endDate]);

  return (
    <>
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Alquileres</h2>
        <div className='btn-agregar' onClick={handleCreate}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
        <h3>Agregar</h3>
      </div>
    </div>

    {/* Tabla de alquileres */}
    <TableStyle2
      nombreID={'alqcod'}    
      columns={alquilerColumns()}
      data={alquileres}
      nombre={'alquileres'}
      onEdit={handleDelete}
      onDelete={handleDelete}
    >
      <DateRangeFilter onFilterChange={handleDateFilter} />
    </TableStyle2>

    <ToastContainer />

    {/* Modal para crear un nuevo alquler*/}
    <Modal active={activeFormCreate} toggle={handleCreate}>
      <FormCreateAlquiler toggle={handleCreate} loadAlquileres={loadAlquileres}/>
    </Modal>
    
    </div>
    </>
  )
}

export default Alquileres;
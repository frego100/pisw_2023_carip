import React, { useState } from 'react';
import DatePicker from 'react-datepicker';

// Componente DateRangeFilter: Filtro para seleccionar un rango de fechas
// Props:
//   - onFilterChange: Función para manejar el cambio en el filtro de fechas
function DateRangeFilter({ onFilterChange }) {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const handleApplyFilter = () => {
    onFilterChange(startDate, endDate);
  };

  return (
    <div className="date-range-filter">
      <label className='text-fil'>Rango de fecha:</label>
      <DatePicker
        className='input-fecha'
        selected={startDate}
        onChange={(date) => setStartDate(date)}
        selectsStart
        startDate={startDate}
        endDate={endDate}
        dateFormat="yyy/MM/dd"
        placeholderText="Fecha de inicio"
      />
      <DatePicker
        className='input-fecha'
        selected={endDate}
        onChange={(date) => setEndDate(date)}
        selectsEnd
        startDate={startDate}
        endDate={endDate}
        dateFormat="yyy/MM/dd"
        placeholderText="Fecha de fin"
      />
      <button className='btn-filtro' onClick={handleApplyFilter}>Aplicar</button>
    </div>
  );
}

export { DateRangeFilter };

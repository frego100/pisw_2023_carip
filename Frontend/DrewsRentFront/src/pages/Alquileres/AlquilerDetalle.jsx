import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getAlquilerById } from '../../api/Alquiler.api';
import { toast } from 'react-toastify';
import { BsPersonRolodex, BsBuildingFill, BsPersonFill, BsCarFrontFill, BsPersonBadgeFill } from "react-icons/bs";


function AlquilerDetalle() {

  const params = useParams();
  /*Datos del alquiler*/ 
  const [alquiler, setAlquiler] = useState([]);

  const navigate = useNavigate();

  const handleRegresar = () => {
    navigate('/alquileres');
  };
  const handleEditar = () => {
    navigate(`/alquileres/${params.id}/edit`);
  };

  useEffect(() => {
    const loadAlquiler = async () => {
      try {
        const response = await getAlquilerById(params.id);
        setAlquiler(response.data);
      } catch (error) {
        toast.error(`Error al cargar alquiler`);
      }
  };
    loadAlquiler();
  }, []);

  return (
    <div className='contenedor-componente'>
      <h2>Detalle del alquiler </h2>
      <div className='contenedor-detalle'>
        <div className='detalle-datos'>
          <h6>Estado de alquiler</h6>
          <p> {alquiler.alqestalqcod?.estalqdes} </p>
          <h6>Fecha inicio</h6>
          <p> {alquiler.alqfecini} </p>
          <h6>Fecha fin</h6>
          <p> {alquiler.alqfecfin} </p>
          <h6>Observaciones</h6>
          <p> {alquiler.alqobs} </p>
        </div>
        <div className='detalle-subtitle'>
          <BsPersonRolodex color='green' className=''/>
          <h4>Proyecto</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
          <h6>Nombre</h6>     
          <p> {alquiler.alqprocod?.pronom} </p>
          <h6>Región</h6>
          <p> {alquiler.alqprocod?.proregcod.regnom} </p>
        </div>          
        <div className='detalle-subtitle'>
          <BsBuildingFill color='green' className=''/>
          <h4>Datos de la empresa</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
          <h6>Razón social</h6>
          <p> {alquiler.alqrescod?.resempcod.emprazsoc} </p>
          <h6>RUC</h6>
          <p> {alquiler.alqrescod?.resempcod.empruc} </p>
          <h6>Area</h6>
          <p> {alquiler.alqrescod?.resarecod.arenom} </p>
        </div>
        <div className='detalle-subtitle'>
          <BsPersonFill color='green' className=''/>
          <h4>Datos del responsable</h4>
          <div className="linea"></div>
        </div>  
        <div className='detalle-datos'>         
          <h6>Nombre y apellidos</h6>
          <p> {alquiler.alqrescod?.resnom} {alquiler.alqrescod?.resape} </p>
          <h6>Teléfono</h6>
          <p> {alquiler.alqrescod?.restel} </p>
          <h6>Email</h6>
          <p> {alquiler.alqrescod?.resema} </p>
        </div>
        <div className='detalle-subtitle'>
          <BsCarFrontFill color='green' className=''/>
          <h4>Datos del vehículo</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
          <h6>Placa</h6>
          <p> {alquiler.alqvehcod?.vehpla} </p>
          <h6>Marca</h6>
          <p> {alquiler.alqvehcod?.vehmar} </p>
          <h6>Soat</h6>
          <p> {alquiler.alqvehcod?.vehsoa} </p>
        </div>
        <div className='detalle-subtitle'>
          <BsPersonBadgeFill color='green' className=''/>
          <h4>Datos del operador</h4>
          <div className="linea"></div>
        </div>
        <div className='detalle-datos'>
          <h6>Nombre y apellidos</h6>
          <p> {alquiler.alqopecod?.openom} {alquiler.alqopecod?.opeape}</p>
          <h6>Teléfono</h6>
          <p> {alquiler.alqopecod?.opetel} </p>
          <h6>Estado del operador</h6>
          <p> {alquiler.alqopecod?.opeestopecod?.estopedes} </p>
        </div>      
      </div>
      <div className='contenedor-btn-detalle'>
        <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
        <button className='btn-editar' type='button' onClick={ () => handleEditar()} >Editar</button>
      </div>      
    </div>
  );
}

export default AlquilerDetalle;
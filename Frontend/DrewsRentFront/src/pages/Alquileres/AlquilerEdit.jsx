import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';
import { getAlquilerById, updateAlquiler } from '../../api/Alquiler.api';
import { getAllProyectos } from '../../api/Proyecto.api';
import { getAllResponsables } from '../../api/Responsables.api';
import { getAllVehiculos } from '../../api/Vehiculo.api';
import { getAllOperadores } from '../../api/Operador.api';
import { getAllEstadosAlquiler } from '../../api/EstadoAlquiler.api';
import { toast } from 'react-toastify';


function AlquilerEdit() {

  /* Datos de proyectos*/
  const [proyectos, setProyectos] = useState([]);
  /* Datos de responsables*/
  const [responsables, setResponsables] = useState([]);
  /* Datos de operadores*/
  const [operadores, setOperadores] = useState([]);
  /* Datos de vehiculos*/
  const [vehiculos, setVehiculos] = useState([]);
  /* Datos de los estados del alquiler*/
  const [estadosAlq, setEstadosAlq] = useState([]);

  const params = useParams();
  
  /*Datos del alquiler*/ 
  const [alquiler, setAlquiler] = useState([]);

  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const navigate = useNavigate();

  const onSubmit = handleSubmit(async data => {
    const res = await updateAlquiler(alquiler.alqcod, data);
    toast.success('Alquiler editado con éxito');
    navigate('/alquileres');
  }); 

  const handleCancelar = () => {
    navigate('/alquileres');
  };

  const loadProyectos = async () => {
    try {
      const response = await getAllProyectos();
      setProyectos(response);      
    } catch (error) {
      toast.error('Error al cargar proyectos');
    }
  };
  const loadResponsables  = async () => {
    try {
      const response = await getAllResponsables();
      setResponsables(response);      
    } catch (error) {
      toast.error('Error al cargar responsables');
    }
  };
  const loadVehiculos  = async () => {
    try {
      const response = await getAllVehiculos();
      setVehiculos(response);      
    } catch (error) {
      toast.error('Error al cargar vehiculos');      
    }
  };
  const loadOperadores  = async () => {
    try {
      const response = await getAllOperadores();
      setOperadores(response);
    } catch (error) {
      toast.error('Error al cargar operadores');      
    }
  };
  const loadEstadosAlquiler  = async () => {
    try {
      const response = await getAllEstadosAlquiler();
      setEstadosAlq(response);       
    } catch (error) {
      toast.error('Error al cargar estados de alquiler');      
    }
  };

  useEffect(() => {
    async function loadAlquiler() {
      try {
        const res = await getAlquilerById(params.id);
        setAlquiler(res.data);
        setValue('alqprocod', res.data.alqprocod.procod);
        setValue('alqrescod', res.data.alqrescod.rescod);
        setValue('alqvehcod', res.data.alqvehcod.vehcod);
        setValue('alqopecod', res.data.alqopecod.opecod);
        setValue('alqestalqcod', res.data.alqestalqcod.estalqcod);
        setValue('alqfecini', res.data.alqfecini);
        setValue('alqfecfin', res.data.alqfecfin);
        setValue('alqobs', res.data.alqobs);        
      } catch (error) {
        toast.error('Error al cargar la lista de alquileres')
      }
    };
    loadAlquiler();
    loadProyectos();
    loadResponsables();
    loadOperadores();
    loadVehiculos();
    loadEstadosAlquiler();  
  }, [params.id]);

  return(
    <div className='contenedor-componente'>
      <h2>Editar alquiler código: {alquiler?.alqcod} </h2>
      <div className='contenedor-editar'>
        <form onSubmit={onSubmit}>
          {/* Campo de selección para el proyecto del alquiler*/}
          <label>
            Proyecto
            <select
              className='input-select'
              name='proyectos'
              {...register('alqprocod', { required: true })}
            >
              {proyectos && proyectos.map(proyecto=>(
                <option value={proyecto?.procod} key={proyecto?.procod}>
                  {proyecto?.pronom}
                </option>
              ))}
            </select>
            {errors.alqprocod?.type === 'required' && <p className='text-error'>*El campo proyecto es requerido</p>}
          </label>

          {/* Campo de selección para el responsable del alquiler*/}
          <label>
            Responsable
            <select
              className='input-select'
              {...register('alqrescod', { required: true })}
            >
              {responsables && responsables.map(responsable=>(
                <option value={responsable.rescod} key={responsable.rescod}>
                  {responsable.resnom}
                </option>
              ))}
            </select>
            {errors.alqrescod?.type === 'required' && <p className='text-error'>*El campo responsable es requerido</p>}
          </label>

          {/* Campo de selección para el vehiculo del alquiler*/}
          <label>
            Vehículo
            <select
              className='input-select'
              {...register('alqvehcod', { required: true })}
            >
              {vehiculos && vehiculos.map(vehiculo=>(
                <option value={vehiculo.vehcod} key={vehiculo.vehcod}>
                  {vehiculo.vehpla}
                </option>
              ))}
            </select>
            {errors.alqvehcod?.type === 'required' && <p className='text-error'>*El campo vehículo es requerido</p>}
          </label>

          {/* Campo de selección para el operador del alquiler*/}
          <label>
            Operador
            <select
              className='input-select'
              {...register('alqopecod', { required: true })}
            >
              {operadores && operadores.map(operador=>(
                <option value={operador.opecod} key={operador.opecod}>
                  {operador.openom}
                </option>
              ))}
            </select>
            {errors.alqopecod?.type === 'required' && <p className='text-error'>*El campo operador es requerido</p>}
          </label>

          {/* Campo de selección para el estado del alquiler*/}
          <label>
            Estado de alquiler
            <select
              className='input-select'
              {...register('alqestalqcod', { required: true })}
            >
              {estadosAlq && estadosAlq.map(estadoAlq=>(
                <option value={estadoAlq.estalqcod} key={estadoAlq.estalqcod}>
                  {estadoAlq.estalqdes}
                </option>
              ))}
            </select>
            {errors.alqestalqcod?.type === 'required' && <p className='text-error'>*El campo estado de alquiler es requerido</p>}
          </label>

          {/* Campo de selección para la fecha inicio del alquiler*/}
          <label>
            Fecha Inicio
            <input className='input-text' type="date" {...register('alqfecini', { required: true })} />
            {errors.alqfecini?.type === 'required' && <p className='text-error'>*El campo fecha inicio es requerido</p>}
          </label>

          {/* Campo de selección para la fecha fin del alquiler*/}
          <label>
            Fecha Fin
            <input className='input-text' type="date" {...register('alqfecfin', { required: true })} />
            {errors.alqfecfin?.type === 'required' && <p className='text-error'>*El campo fecha fin es requerido</p>}
          </label>

          {/* Campo de selección para las observaciones del alquiler*/}
          <label>
            Observaciones
            <input
              className='input-text'
              type="text"
              placeholder='Observaciones'
              {...register('alqobs')}
            />
          </label>

          {/* Botones de cancelar/guardar */}
          <div className='contenedor-btn-editar'>
            <button className='btn-cancelar' type='button' onClick={ () => handleCancelar()}>Cancelar</button>
            <button className='btn-guardar' type='submit'>Guardar</button>
          </div>

        </form>
      </div>
    </div>
  );
}
export default AlquilerEdit;
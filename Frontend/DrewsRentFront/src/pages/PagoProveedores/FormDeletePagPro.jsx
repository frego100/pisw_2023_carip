import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getProyecto, updateProyecto } from '../../api/Proyecto.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';

import { updatePago, getByIdPago } from "../../api/PagoProveedores.api";
import { getEstadoPagoProveedor } from "../../api/EstadoPagoProveedor.api"

function DeletePago({ toggle, pagoId, loadAllPagos }) {

    const [pago, setPago] = useState({});
    const [estReg, setEstReg] = useState(null);

    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    const onSubmit = handleSubmit(async (data) => {
        //const res = await updatePago(pagoId, data);
        try {
            const updatedPago = { ...pago, pagproestpagpro: data.pagproestpagpro, pagproprocod: data.pagproprocod };
            const res = await updatePago(pagoId, updatedPago);
            loadAllPagos();
            toggle();
            toast.success('Pago creado con éxito');
        } catch (error) {
            toast.error(`Error al eliminar el pago: ${error.message}`);
        }

    });

    useEffect(() => {
        async function loadEstReg() {
            try {
                const res = await getEstadoPagoProveedor('Pagado');
                setValue('pagproestpagpro', res);
                setEstReg(res);
            } catch (error) {
                toast.error(`Error al cargar los estados: ${error.message}`);
            }
        }

        async function loadPago() {
            try {
                const res = await getByIdPago(pagoId);
                setPago(res);
                setValue('pagproprocod', res.pagproprocod.procod);
                console.log('Eso eliminas',res);
                console.log('Eso eliminas ID',pagoId);
            } catch (error) {
                toast.error(`Error al cargar el pago: ${error.message}`);
            }
        }

        loadEstReg();
        loadPago();
    }, []
    );

    return (
        <>
        <div className='contenedor-modal-eliminar'>
        <h3>¿Está seguro que quiere eliminar el pago?</h3>
            <form className='contenedor-modal-formulario' onSubmit={onSubmit}>
                <label className='modal-formulario-label'>
                    Estado de pago
                    <select className='formulario-extra-eliminar'
                        {...register(`pagproestpagpro`, { required: true })} >
                        <option value={estReg} >Pagado</option>
                    </select>
                    {errors.pagproprocod?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
                </label>

                <div className='contenedor-botones-modal'>
                    <button className='boton-cancelar' type='button' onClick={toggle}>Cancelar</button>
                    <button className='boton-eliminar' type='submit'>Eliminar</button>
                </div>
            </form>
        </div>
        </>
    );
}

export { DeletePago };
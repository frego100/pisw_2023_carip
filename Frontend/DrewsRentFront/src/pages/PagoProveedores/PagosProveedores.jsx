import React, { useState, useEffect } from 'react'

import '../../styles/ContenedorComp.css'
import '../../styles/Pagos.css'

import { BsFillPlusCircleFill } from "react-icons/bs";
import { Link, useNavigate } from 'react-router-dom';

import { getAllPlacaVehiculo } from "../../api/Vehiculo.api";
import pagosColumns from '../../Utils/ComponentesUtils/pagosColumns';
import  Modal  from '../../components/Modal';
import { TableStyle2 } from '../../components/Tabla/TableStyle2';
import { getAllPagos } from "../../api/PagoProveedores.api";
import FormCreatePagPro from './FormCreatePagPro';
import FormEditPagPro from './FormEditPagPro';
import { DeletePago } from './FormDeletePagPro';

import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function PagoProveedores() {
  const navigate = useNavigate();

  //Pagos
  const [allPagos, setAllPagos] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);
  
  const [selectedPagoId, setSelectedPagoId] = useState();

  //MODALLLLL
  const [modalOpen, setModalOpen] = useState(false);

  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };

  const handleCreate = () => {
    navigate('/pagosProveedores/create');
  }

  const handleEdit = (idPago) => {
    setSelectedPagoId(idPago);
    navigate(`/pagosProveedores/${idPago}/update`);
    console.log('Editar', idPago);
    setActiveFormEdit(true);
    console.log('Editar2', selectedPagoId);
  };

  const handleDelete = (idPago) => {
    setSelectedPagoId(idPago);
    setActiveFormDelete(!activeFormDelete);
    console.log('Eliminar', idPago);
  };

  const loadAllPagos = async () => {
    try {
      const res = await getAllPagos();
      //setAllPagos(res.filter((pago) => pago.pagproestpagpro.estpagprodes === 'Pendiente'));
      setAllPagos(res);
    } catch (error) {
      toast.error(`Error al cargar los pagos: ${error.message}`);
    }
  };

  useEffect(() => {    
    loadAllPagos();
  }, []);

  return (
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Pagos a Proveedores</h2>
        <div className='btn-agregar' onClick={handleCreate}>
        <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>

      <TableStyle2
        nombreID={'pagprocod'} 
        columns={pagosColumns()} 
        data={allPagos} 
        nombre={'pagosProveedores'} 
        onDelete={handleDelete}
        onEdit={handleDelete}
      />

      <ToastContainer />

      {activeFormCreate && <FormCreatePagPro />}

      <Modal active={activeFormDelete} toggle={handleDelete}>
        <DeletePago toggle={handleDelete} pagoId={selectedPagoId} loadAllPagos={loadAllPagos} /> 
      </Modal>
      
    </div>
  )
}

export default PagoProveedores

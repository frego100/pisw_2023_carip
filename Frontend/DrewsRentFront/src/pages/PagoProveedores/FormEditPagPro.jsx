import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate, useParams } from 'react-router-dom';

import { updatePago, getByIdPago } from '../../api/PagoProveedores.api';
import { getAllProveedores } from "../../api/Proveedores.api";
import { getAllEstadoPagPro } from '../../api/EstadoPagoProveedor.api';
import { toast } from 'react-toastify';

function FormEditPagPro() {
    const navigate = useNavigate();

    const {id} = useParams();

    //Para el formulario
    const [proveedores, setProveedores] = useState([]);
    const [estadosPago, setEstadosPago] = useState([]);

    //Objeto para modificar
    const [pagoPro, setPagoPro] = useState({});
    //Estado de los registros de mi formulario
    const {
        register,
        handleSubmit,  
        formState: { errors },
        setValue,
    } = useForm();

    const handleRegresarClick = () => {
        navigate(-1);
    };

    const onSubmit = async (data) => {
        try {
            const resPago = await updatePago(id, data);
            console.log('Data Pago Actualizado:', resPago);
            navigate('/pagosProveedores');
            toast.success('Pago editado con éxito');
        } catch (error) {
            console.log(data);
            toast.error(`Error al editar el pago: ${error.message}`);
        }
    };

    const loadProveedores = async () => {
        try {
            const res = await getAllProveedores();
            setProveedores(res);
        } catch (error) {
            toast.error(`Error al cargar los proveedores: ${error.message}`);
        }
    };

    const loadEstadoPagoPro = async () => {
        try {
            const res = await getAllEstadoPagPro();
            setEstadosPago(res);
        } catch (error) {
            toast.error(`Error al cargar los estados: ${error.message}`);
        }
    };

    const loadPagoData = async () => {
        try {
            const pago = await getByIdPago(id);
            setPagoPro(pago);
            setValue('pagprofec', pago?.pagprofec || '');
            setValue('pagproobs', pago?.pagproobs || '');
            setValue('pagprototal', pago?.pagprototal || '');
            setValue('pagproestpagpro', pago?.pagproestpagpro?.estpagprocod || '');
            setValue('pagproprocod', pago?.pagproprocod?.procod || '');
        } catch (error) {
            toast.error(`Error al cargar el pago: ${error.message}`);
        }
    };
    
    useEffect(() => {
    loadProveedores();
    loadEstadoPagoPro();
    loadPagoData();
    }, []);

  return (
    <div className='contenedor-componente'>
      <div className='contenedor-formulario'>
        <h2>Editar pago</h2>
        <form onSubmit={handleSubmit(onSubmit)}>
            <div className='agregar-row'>
                <label className='form-row1'>
                    Proveedor
                    <select className='formulario-pago' name="proveedor" 
                        value={pagoPro?.pagproprocod?.procod || ''}
                        {...register('pagproprocod', { required: true })} 
                        >
                        {proveedores && proveedores.map(proveedor =>(
                            <option value={proveedor.procod} key={proveedor.procod}>{proveedor.prorazsoc}</option>
                        ))}
                    </select>
                    {errors.pagproprocod?.type === 'required' && <p className='text-error'>*El campo es requerido</p>}  
                </label>

                <label className='form-row1'>
                    Fecha
                    <input className='formulario-pago' type='date'
                        {...register('pagprofec', { required: true })}
                    />
                    {errors.pagprofec?.type === 'required' && <p className='text-error'>*El campo es requerido</p>}
                </label>

                <label className='form-row1'>
                    Estado del pago
                    <select className='formulario-pago' name="estadoPago" 
                        defaultValue={pagoPro?.pagproestpagpro?.estpagprocod || ''}
                        {...register('pagproestpagpro', { required: true })} 
                        >
                        {estadosPago && estadosPago.map(estadoPa =>(
                            <option value={estadoPa.estpagprocod} key={estadoPa.estpagprocod}>{estadoPa.estpagprodes}</option>
                        ))}
                    </select>
                    {errors.pagproestpagpro?.type === 'required' && <p className='text-error'>*El campo es requerido</p>}  
                </label>
            </div>

            <div className='agregar-row'>
                <label>
                    Observacion
                    <input className='formulario-pago-detalle'  type="text" placeholder='Ninguna'
                        {...register('pagproobs', { required: true })}
                        defaultValue={pagoPro?.pagproobs || ''}
                    />
                    {errors.pagproobs?.type === 'required' && (
                        <p className='text-error'>*El campo es requerido</p>
                    )}
                </label>

                <label>
                    Monto Total
                    <input className='formulario-pago2' type='number'
                        {...register('pagprototal', { required: true })}
                        defaultValue={pagoPro?.pagprototal || ''}
                    />
                    {errors.pagprototal?.type === 'required' && (
                        <p className='text-error'>*El campo es requerido</p>
                    )}
                </label>

            </div>

            <div className='contenedor-botones'>
                <button className='btn-cancelar' type='button' onClick={handleRegresarClick}>Cancelar</button>
                <button className='btn-registrar' type='submit'>Editar</button>
            </div>

        </form>    
      </div>
    </div>
    )
}

export default FormEditPagPro;
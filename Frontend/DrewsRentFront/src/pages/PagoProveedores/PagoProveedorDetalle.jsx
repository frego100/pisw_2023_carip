import React, { useEffect, useState } from 'react';
import { BsFillPersonFill, BsCalendar3 , BsClipboardCheck, 
    BsBank, BsGeoAlt, BsGeo, BsPhone, BsEnvelopeAt, BsBuilding, 
    BsCreditCard2Front, BsCoin } from "react-icons/bs";

import { useParams, useNavigate } from 'react-router-dom'
import { getByIdPago } from "../../api/PagoProveedores.api";
import '../../styles/ContenedorComp.css'
import '../../styles/Pagos.css'
import { Form } from "antd";
import { MdOutlineAddToPhotos } from "react-icons/md";
import { toast } from 'react-toastify';

import { PagoExtra } from './Extra-Vehiculos/PagoExtra';
import { PagoVehiculo } from './Extra-Vehiculos/PagoVehiculos';

function PagoProveedorDetalle() {
    const navigate = useNavigate();
    const {id} = useParams();
    const [pago, setPago] = useState({});

    const handleRegresar = () => {
        navigate('/pagosProveedores');
    };

    const handleEditar = () => {
        navigate(`/pagosProveedores/${id}/update`);
    };

    useEffect(() => {
        const loadPago = async () => {
            try {
                const res = await getByIdPago(id);
                setPago(res);
            } catch (error) {
                toast.error(`Error al cargar el pago: ${error.message}`);
            }
        };

        loadPago();
    }, [id]);

    return (
    <div className='contenedor-componente'>

        <div className='contenedor-lista-detalle-pago'>

            <h2> Pago </h2>
            
            <div className='agregar-row' >
                <h4> Datos proveedor </h4>
                <div className="line"></div>
            </div>
            
            <div className='agregar-row'>
                <div className='detalle'>
                    <BsFillPersonFill color='green' className='' />
                    <h4>Razon social:</h4>
                    <p>{pago.pagproprocod?.prorazsoc}</p>
                </div>
                <div className='detalle'>
                    <BsGeo color='green' className='' />
                    <h4>Direccion:</h4>
                    <p>{pago.pagproprocod?.prodir}</p>
                </div>
                <div className='detalle'>
                    <BsPhone color='green' className='' />
                    <h4>Telefono:</h4>
                    <p>{pago.pagproprocod?.protel}</p>
                </div>
            </div>

            <div className='agregar-row'>
                <div className='detalle'>
                    <BsEnvelopeAt color='green' className='' />
                    <h4>Email:</h4>
                    <p>{pago.pagproprocod?.proema}</p>
                </div>
                <div className='detalle'>
                    <BsBuilding  color='green' className='' />
                    <h4>RUC:</h4>
                    <p>{pago.pagproprocod?.proruc}</p>
                </div>
                <div className='detalle'>
                    <BsGeoAlt color='green' className='' />
                    <h4>Ubicacion:</h4>
                    <p>{pago.pagproprocod?.proubi?.disnom+" - "+pago.pagproprocod?.proubi?.disprocod?.proregcod?.regnom}</p>
                </div>
            </div>

            <div className='agregar-row' >
                <h4> Datos de pago </h4>
                <div className="line"></div>
            </div>

            <div className='agregar-row'>
                <div className='detalle'>
                    <BsBank color='green' className='' />
                    <h4>Datos de banco:</h4>
                    <p>{pago.pagproprocod?.probandep}</p>
                </div>
                <div className='detalle'>
                    <BsBuilding  color='green' className='' />
                    <h4>Tipo de cuenta:</h4>
                    <p>{pago.pagproprocod?.protipcuedep}</p>
                </div>
                <div className='detalle'>
                    <BsCreditCard2Front color='green' className='' />
                    <h4>Numero de cuenta:</h4>
                    <p>{pago.pagproprocod?.pronumcuedep}</p>
                </div>
            </div>

            <div className='agregar-row'>
                <div className='detalle'>
                    <BsBank color='green' className='' />
                    <h4>Datos de banco:</h4>
                    <p>{pago.pagproprocod?.probandet}</p>
                </div>
                <div className='detalle'>
                    <BsBuilding  color='green' className='' />
                    <h4>Tipo de cuenta:</h4>
                    <p>{pago.pagproprocod?.protipcuedet}</p>
                </div>
                <div className='detalle'>
                    <BsCreditCard2Front color='green' className='' />
                    <h4>Numero de cuenta:</h4>
                    <p>{pago.pagproprocod?.pronumcuedet}</p>
                </div>
            </div>

            <div className='agregar-row' >
                <h4> Detalles </h4>
                <div className="line"></div>
            </div>

            <div className='agregar-row'>
                <div className='detalle'>
                    <BsCalendar3 color='green' className='' />
                    <h4>Fecha:</h4>
                    <p>{pago.pagprofec}</p>
                </div>
                <div className='detalle'>
                    <BsCoin color='green' className='' />
                    <h4>Pago total:</h4>
                    <p>{pago.pagprototal}</p>
                </div>
                <div className='detalle'>
                    <BsClipboardCheck color='green' className='' />
                    <h4>Estado del pago:</h4>
                    <p>{pago.pagproestpagpro?.estpagprodes}</p>
                </div>
            </div>

            <PagoExtra pago={pago} id={id}/>

            <PagoVehiculo pago={pago} id={id}/>
            
        </div>

        <div className='contenedor-btn-editar'>
            <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
            <button className='btn-editar' type='button' onClick={ () => handleEditar()} >Editar</button>
        </div>   
    </div>
    );
}

export default PagoProveedorDetalle;
import React, { useEffect, useState } from 'react';
import { Button, Modal, Form, Select, Table, Input, InputNumber, DatePicker  } from "antd";
import { MdDelete, MdEdit, MdOutlineAddToPhotos } from "react-icons/md";
import { getAllPagosV, updatePagoV, deletePagoV, createPagoV } from "../../../api/PagoProveedorVehiculo.api";
import { getAllPlacaVehiculo } from "../../../api/Vehiculo.api";

import '../../../styles/ContenedorComp.css'
import '../../../styles/Pagos.css'

import { toast } from 'react-toastify';
import { Space } from 'antd';

function PagoVehiculo({ pago, id }) {

    const [placas, setPlacas] = useState([]);
    const [pagoVehiculo, setPagoVehiculo] = useState([]);
    const pagosProveedoresVehiculos = pagoVehiculo.filter(pagoVehiculoItem  => pagoVehiculoItem .pagvehpagcod.pagprocod === pago.pagprocod);
    const [visibleVehiculo, setVisibleVehiculo] = useState(false);
    const [visibleVehiculoD, setVisibleVehiculoD] = useState(false);
    const [visibleVehiculoC, setVisibleVehiculoC] = useState(false);
    const [pagoVehiculoNew, setpagoVehiculoNew] = useState({});
    const [pagoVehiculoEdit, setpagoVehiculoEdit] = useState({});
    const [pagoVehiculoDelete, setpagoVehiculoDelete] = useState({});
    const [form] = Form.useForm();
    console.log(pagosProveedoresVehiculos);

    const [confirmacionDelete, setconfirmacionDelete] = useState(null);

    const columnsVehiculos = [
        {
            title: 'Nº',
            dataIndex: 'pagvehcod',
            key: 'pagvehcod',
            render: (text, record, index) => index + 1,
        },
        {
            title: 'Placa',
            dataIndex: 'pagvehpla',
            key: 'pagvehpla',
        },
        {
            title: 'Fecha de Inicio',
            dataIndex: 'pagvehperini',
            key: 'pagvehperini',
        },
        {
            title: 'Fecha de Fin',
            dataIndex: 'pagvehperfin',
            key: 'pagvehperfin',
        },
        {
            title: 'Cantidad',
            dataIndex: 'pagvehcan',
            key: 'pagvehcan',
        },
        {
            title: 'Costo',
            dataIndex: 'pagvehcos',
            key: 'pagvehcos',
        },
        {
            title: 'Monto',
            dataIndex: 'pagvehmon',
            key: 'pagvehmon',
        },
        {
            title: 'Acciones',
            key: 'acciones',
            render: (text, data) => (
                <Space size="middle">
                    <MdEdit onClick={() => handleEditarVehiculo(data)} className='icono-editar-modal'/>
                    <MdDelete onClick={() => handleDeleteVehiculo(data)} className='icono-editar-modal'/>
                </Space>
            ),
        }
    ];

    const handleCreateVehiculo = () =>{
        setVisibleVehiculoC(true);
    }

    const handleDeleteVehiculo = (vehiculo) =>{
        setpagoVehiculoDelete(vehiculo);
        setVisibleVehiculoD(true);
    }

    const handleEditarVehiculo = (vehiculo) => {
        setpagoVehiculoEdit(vehiculo);
        setVisibleVehiculo(true);
    };

    const handleModalCancel = () => {
        form.resetFields();
        setVisibleVehiculo(false);
        setVisibleVehiculoD(false);
        setVisibleVehiculoC(false);
    };

    const [edicionExitosa, setEdicionExitosa] = useState(false);

    const handleModalCreateVehiculo = async () => {
        try {
            form.validateFields().then(async (values) => {
                console.log('Datos del formulario:', values);
                pagoVehiculoNew.pagvehpagcod = id;
                console.log('Datos del formulario:', pagoVehiculoNew);
                const resultado = await createPagoV(pagoVehiculoNew);
                console.log('Resultado de la actualización:', resultado);
                setEdicionExitosa(true);
                handleModalCancel();
                toast.success('Creacion de pago exitosa');
            })
            .catch((errorInfo) => {
            console.log('Error al validar el formulario:', errorInfo);
            toast.error('Datos incompletos');
            });
        } catch (error) {
            toast.error('Error en la creacion del pago adicional:', error);
            console.error('Resultado de la actualización:', error);
        }
    };

    const handleModalDeleteVehiculo = async () => {
        try {
            if (confirmacionDelete === "si") {
                const id = pagoVehiculoDelete.pagvehcod;
                console.log('DELETEVehiculo:', pagoVehiculoDelete);
                const resultado = await deletePagoV(id);
                console.log('Resultado de la eliminacion:', resultado);
                setEdicionExitosa(true);
                setVisibleVehiculoD(false);
                toast.success('Eliminacion exitosa');
            } else{
                setVisibleVehiculoD(false);
                console.log('sin confirmacion');
            }
        } catch (error) {
            toast.error('Error en la eliminacion del pago adicional:', error);
            console.error('Resultado de la eliminacion:', error);
        }
    };

    const handleModalOkVehiculo = async () => {
        try {
            const id = pagoVehiculoEdit.pagvehpagcod.pagprocod;
            pagoVehiculoEdit.pagvehpagcod = id;
            console.log('PAGOVehiculo:', pagoVehiculoEdit);

            const resultado = await updatePagoV(pagoVehiculoEdit.pagvehcod, pagoVehiculoEdit);

            console.log('Resultado de la actualización:', resultado);
            setEdicionExitosa(true);
            setVisibleVehiculo(false);
            toast.success('Edicion exitosa');
        } catch (error) {
            toast.error('Error en la actualización del pago adicional:', error);
            console.error('Resultado de la actualización:', error);
        }
    };

    useEffect(() => {
        const loadPagoVehiculo = async () => {
            try {
                const res = await getAllPagosV();
                setPagoVehiculo(res);
                console.log('vehiculo',res);
            } catch (error) {
                toast.error(`Error al cargar los pagos Vehiculos: ${error.message}`);
            }
        };

        const loadPlacas = async () => {
            try {
                const res = await getAllPlacaVehiculo();
                setPlacas(res);
            } catch (error) {
                toast.error(`Error al cargar las placas: ${error.message}`);
            }
        };

        if (edicionExitosa) {
            setEdicionExitosa(false);
        }

        loadPlacas();
        loadPagoVehiculo();
    }, [edicionExitosa]);

    useEffect(() => {
        form.setFieldsValue({
            fechai: pagoVehiculoEdit.pagvehperini,
            fechaf: pagoVehiculoEdit.pagvehperfin,
            placa: pagoVehiculoEdit.pagvehpla,
            dias: pagoVehiculoEdit.pagvehcan,
            costo: pagoVehiculoEdit.pagvehcos,
            monto: pagoVehiculoEdit.pagvehmon,
        });
    }, [visibleVehiculo, pagoVehiculoEdit, form]);

    return (
        <>
        <div className='agregar-row' >
            <h4> Pagos Vehiculos </h4>
            <MdOutlineAddToPhotos color='green' className="icon-add-detalles"  onClick={handleCreateVehiculo} />
            <div className="line"></div>
        </div>
        <div className='vehiculo-detalles-table-container'>
            <Table className="tabla-tareos" dataSource={pagosProveedoresVehiculos} key={pagoVehiculo.pagvehcod} columns={columnsVehiculos} />
            <div>
            <Modal
                forceRender
                title={`Edicion del pago vehiculo`}
                open={visibleVehiculo}
                onCancel={handleModalCancel}
                onOk={handleModalOkVehiculo}
                footer={[
                <div className='button-modal-container'>
                    <Button className='button-modal-agregar' onClick={handleModalOkVehiculo} >Guardar</Button>
                    <Button onClick={handleModalCancel}>Cancelar</Button>
                </div>
                ]}
                centered
                width={650} 
                >
                <div style={{ maxHeight: '800px',overflowY: 'auto'}}>
                    <Form
                        form={form}
                        labelCol={{ span: 6 }}
                        wrapperCol={{ span: 16 }}
                        layout="vertical"
                        >
                        <div className='agregar-row' >
                            <Form.Item style={{ marginLeft:'10px', marginBottom: '5px' }}  
                            label='Fecha de Inicio' labelCol={{ style: { marginBottom: '-30px' } }} >
                                <DatePicker
                                    style={{ width: '200px' }}
                                    getCalendarContainer={(trigger) => trigger.parentNode}
                                    placement="topLeft"
                                    onChange={(date) => {
                                        if (date) {
                                            console.log('Fecha seleccionada:', date);
                                            const formattedDate = date.format('YYYY-MM-DD');
                                            setpagoVehiculoEdit(prevState => ({ ...prevState, pagvehperini: formattedDate }));
                                        }
                                    }}
                                />
                            </Form.Item >

                            <Form.Item style={{ marginLeft:'20px', marginBottom: '5px' }}  
                            label='Fecha final' labelCol={{ style: { marginBottom: '-30px' } }} >
                                <DatePicker
                                    style={{ width: '200px' }}
                                    getCalendarContainer={(trigger) => trigger.parentNode}
                                    placement="topLeft"
                                    onChange={(date) => {
                                        if (date) {
                                            const formattedDate = date.format('YYYY-MM-DD');
                                            setpagoVehiculoEdit(prevState => ({ ...prevState, pagvehperfin: formattedDate }));
                                        }
                                    }}
                                />
                            </Form.Item >
                        </div>

                        <div className='agregar-row' >
                        <Form.Item name="placa" style={{ marginLeft:'10px', marginBottom: '5px' }}  label='Placa' labelCol={{ style: { marginBottom: '-30px' } }} >
                            <Select style={{ width: '200px' }}
                                onChange={(placa) => setpagoVehiculoEdit({ ...pagoVehiculoEdit, pagvehpla: placa })}
                            >
                                {placas.map((placa) => (
                                    <Select.Option key={placa} value={placa}>
                                    {placa}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>

                        <Form.Item name="dias" style={{ marginLeft:'50px', marginBottom: '5px'  }}  label='Cantidad de dias' labelCol={{ style: { marginBottom: '-30px' } }} >
                            <InputNumber style={{ width: '220px' }} 
                            onChange={(dias) => setpagoVehiculoEdit({ ...pagoVehiculoEdit, pagvehcan: dias })}
                            placeholder="Escribe un número" />
                        </Form.Item >
                        </div>
                        
                        <div className='agregar-row' >
                        <Form.Item name="costo" style={{ marginLeft:'10px', marginBottom: '5px'  }}  label='Costo' labelCol={{ style: { marginBottom: '-30px' } }} >
                            <InputNumber style={{ width: '220px' }} 
                            onChange={(costo) => setpagoVehiculoEdit({ ...pagoVehiculoEdit, pagvehcos: costo })}
                            placeholder="Escribe un número" />
                        </Form.Item >

                        <Form.Item name="monto" style={{ marginLeft:'20px', marginBottom: '5px' }}  label='Monto' labelCol={{ style: { marginBottom: '-30px' } }} >
                            <InputNumber style={{ width: '220px' }} 
                            onChange={(monto) => setpagoVehiculoEdit({ ...pagoVehiculoEdit, pagvehmon: monto })}
                            placeholder="Escribe un número" />
                        </Form.Item >
                        </div>

                    </Form>
                </div>
                
            </Modal>
            </div>

            <div>
            <Modal
                forceRender
                title={`Eliminacion pago vehiculo`}
                open={visibleVehiculoD}
                onCancel={handleModalCancel}
                onOk={handleModalDeleteVehiculo}
                footer={[
                <div className='button-modal-container'>
                    <Button className='button-modal-agregar' onClick={handleModalDeleteVehiculo} >Guardar</Button>
                    <Button onClick={handleModalCancel}>Cancelar</Button>
                </div>
                ]}
                centered
                width={450} 
                >
                <div style={{ textAlign: 'center' }}>
                    <p>¿Estás seguro que deseas eliminar este pago de vehículo?</p>
                    <Select
                        style={{ width: '50%' }}
                        placeholder="Seleccione una opción"
                        onChange={(value) => setconfirmacionDelete(value)}
                    >
                        <Select.Option value="si">Sí</Select.Option>
                        <Select.Option value="no">No</Select.Option>
                    </Select>
                </div>
            </Modal>
            </div>

            <div>
            <Modal
                forceRender
                title={`Agregar pago vehiculo`}
                open={visibleVehiculoC}
                onCancel={handleModalCancel}
                footer={[
                <div className='button-modal-container'>
                    <Button className='button-modal-agregar' onClick={handleModalCreateVehiculo} >Guardar</Button>
                    <Button onClick={handleModalCancel}>Cancelar</Button>
                </div>
                ]}
                centered
                width={650} 
                >
                <div style={{ maxHeight: '800px',overflowY: 'auto'}}>
                    <Form
                        form={form}
                        labelCol={{ span: 6 }}
                        wrapperCol={{ span: 16 }}
                        layout="vertical"
                        onFinish={handleModalCreateVehiculo}
                        >
                        <div className='agregar-row' >
                            <Form.Item style={{ marginLeft:'10px', marginBottom: '5px' }}  
                            label='Fecha de Inicio' labelCol={{ style: { marginBottom: '-30px' } }} 
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor, ingrese una fecha',
                                },
                            ]}
                            >
                                <DatePicker
                                    style={{ width: '200px' }}
                                    getCalendarContainer={(trigger) => trigger.parentNode}
                                    placement="topLeft"
                                    onChange={(date) => {
                                        if (date) {
                                            const formattedDate = date.format('YYYY-MM-DD');
                                            setpagoVehiculoNew(prevState => ({ ...prevState, pagvehperini: formattedDate }));
                                        }
                                    }}
                                />
                            </Form.Item >

                            <Form.Item style={{ marginLeft:'20px', marginBottom: '5px' }}  
                            label='Fecha final' labelCol={{ style: { marginBottom: '-30px' } }} 
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor, ingrese una fecha',
                                },
                            ]}
                            >
                                <DatePicker
                                    style={{ width: '200px' }}
                                    getCalendarContainer={(trigger) => trigger.parentNode}
                                    placement="topLeft"
                                    onChange={(date) => {
                                        if (date) {
                                            const formattedDate = date.format('YYYY-MM-DD');
                                            setpagoVehiculoNew(prevState => ({ ...prevState, pagvehperfin: formattedDate }));
                                        }
                                    }}
                                />
                            </Form.Item >
                        </div>

                        <div className='agregar-row' >
                        <Form.Item style={{ marginLeft:'10px', marginBottom: '5px' }}  
                        label='Placa' labelCol={{ style: { marginBottom: '-30px' } }} 
                        rules={[
                            {
                                required: true,
                                message: 'Por favor, ingrese una placa',
                            },
                        ]}>
                            <Select style={{ width: '200px' }}
                                onChange={(placa) => setpagoVehiculoNew({ ...pagoVehiculoNew, pagvehpla: placa })}
                            >
                                {placas.map((placa) => (
                                    <Select.Option key={placa} value={placa}>
                                    {placa}
                                    </Select.Option>
                                ))}
                            </Select>
                        </Form.Item>

                        <Form.Item style={{ marginLeft:'50px', marginBottom: '5px'  }}  
                        label='Cantidad de dias' labelCol={{ style: { marginBottom: '-30px' } }} 
                        rules={[
                            {
                                required: true,
                                message: 'Por favor, ingrese una numero',
                            },
                        ]}>
                            <InputNumber style={{ width: '220px' }} 
                            onChange={(dias) => setpagoVehiculoNew({ ...pagoVehiculoNew, pagvehcan: dias })}
                            placeholder="Escribe un número" />
                        </Form.Item >
                        </div>
                        
                        <div className='agregar-row' >
                        <Form.Item style={{ marginLeft:'10px', marginBottom: '5px'  }}  
                        label='Costo' labelCol={{ style: { marginBottom: '-30px' } }} 
                        rules={[
                            {
                                required: true,
                                message: 'Por favor, ingrese una numero',
                            },
                        ]}>
                            <InputNumber style={{ width: '220px' }} 
                            onChange={(costo) => setpagoVehiculoNew({ ...pagoVehiculoNew, pagvehcos: costo })}
                            placeholder="Escribe un número" />
                        </Form.Item >

                        <Form.Item style={{ marginLeft:'20px', marginBottom: '5px' }}  
                        label='Monto' labelCol={{ style: { marginBottom: '-30px' } }} 
                        rules={[
                            {
                                required: true,
                                message: 'Por favor, ingrese una numero',
                            },
                        ]}>
                            <InputNumber style={{ width: '220px' }} 
                            onChange={(monto) => setpagoVehiculoNew({ ...pagoVehiculoNew, pagvehmon: monto })}
                            placeholder="Escribe un número" />
                        </Form.Item >
                        </div>

                    </Form>
                </div>
                
            </Modal>
            </div>

        </div>
        </>
    );
}

export { PagoVehiculo };
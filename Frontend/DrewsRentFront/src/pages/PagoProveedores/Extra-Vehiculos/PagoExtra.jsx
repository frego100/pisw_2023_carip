import React, { useEffect, useState } from 'react';
import { Button, Modal, Form, Select, Table, Input, InputNumber, DatePicker  } from "antd";
import { MdDelete, MdEdit, MdOutlineAddToPhotos } from "react-icons/md";
import { getAllPagosE, updatePagoE, deletePagoE, createPagoE } from "../../../api/PagoProveedoresExtra.api";
import { getAllPlacaVehiculo } from "../../../api/Vehiculo.api";

import moment from 'moment';

import '../../../styles/ContenedorComp.css'
import '../../../styles/Pagos.css'

import { toast } from 'react-toastify';
import { Space } from 'antd';

function PagoExtra({ pago, id }) {

    const [placas, setPlacas] = useState([]);
    const [pagoExtra, setPagoExtra] = useState([]);
    const pagosProveedoresExtras = pagoExtra.filter(pagoExtraItem  => pagoExtraItem .pagextpagcod.pagprocod === pago.pagprocod);
    const [visibleExtra, setVisibleExtra] = useState(false);
    const [visibleExtraD, setVisibleExtraD] = useState(false);
    const [visibleExtraC, setVisibleExtraC] = useState(false);
    const [pagoExtraNew, setpagoExtraNew] = useState({});
    const [pagoExtraDelete, setpagoExtraDelete] = useState({});
    const [pagoExtraEdit, setpagoExtraEdit] = useState({});
    const [form] = Form.useForm();
    console.log(pagosProveedoresExtras);

    const [confirmacionDelete, setconfirmacionDelete] = useState(null);

    const columnsExtra = [
        {
            title: 'Nº',
            dataIndex: 'pagextcod',
            key: 'pagextcod',
            render: (text, record, index) => index + 1,
        },
        {
            title: 'Placa',
            dataIndex: 'pagextvehpla',
            key: 'pagextvehpla',
        },
        {
            title: 'Descripcion',
            dataIndex: 'pagextdes',
            key: 'pagextdes',
        },
        {
            title: 'Fecha',
            dataIndex: 'pagextfec',
            key: 'pagextfec',
        },
        {
            title: 'Costo',
            dataIndex: 'pagextcos',
            key: 'pagextcos',
        },
        {
            title: 'Monto',
            dataIndex: 'pagextmon',
            key: 'pagextmon',
        },
        {
            title: 'Acciones',
            key: 'acciones',
            render: (text, data) => (
                <Space size="middle" align='center'>
                    <MdEdit onClick={() => handleEditarExtra(data)} className='icono-editar-modal'/>
                    <MdDelete onClick={() => handleDeleteExtra(data)} className='icono-editar-modal'/>
                </Space>
            ),
        }
    ];

    const handleCreateExtra = () =>{
        setVisibleExtraC(true);
    }

    const handleDeleteExtra = (vehiculo) =>{
        setpagoExtraDelete(vehiculo);
        setVisibleExtraD(true);
    }

    const handleEditarExtra = (vehiculo) => {
        try {
            setpagoExtraEdit(vehiculo);
            setVisibleExtra(true);
        } catch (error) {
            console.log('ERROR',error)
        }

    };

    const handleModalCancel = () => {
        console.log('Formulario antes de reset:', form);
        form.setFieldsValue({});
        setVisibleExtra(false);
        setVisibleExtraD(false);
        setVisibleExtraC(false);
    };

    const [edicionExitosa, setEdicionExitosa] = useState(false);

    const handleModalCreateExtra = async () => {
        try {
            form.validateFields().then(async (values) => {
                console.log('Datos del formulario:', values);
                pagoExtraNew.pagextpagcod = id;
                console.log('Datos del formulario:', pagoExtraNew);
                const resultado = await createPagoE(pagoExtraNew);
                console.log('Resultado de la actualización:', resultado);
                setEdicionExitosa(true);
                handleModalCancel();
                toast.success('Creacion de pago exitosa');
            })
            .catch((errorInfo) => {
            console.log('Error al validar el formulario:', errorInfo);
            toast.error('Datos incompletos');
            });
        } catch (error) {
            toast.error('Error en la creacion del pago adicional:', error);
            console.error('Resultado de la actualización:', error);
        }
    };

    const handleModalDeleteExtra = async () => {
        try {
            if (confirmacionDelete === "si") {
                const id = pagoExtraDelete.pagextcod;
                console.log('DELETEVehiculo:', pagoExtraDelete);
                const resultado = await deletePagoE(id);
                console.log('Resultado de la eliminacion:', resultado);
                setEdicionExitosa(true);
                setVisibleExtraD(false);
                toast.success('Eliminacion exitosa');
            } else{
                setVisibleExtraD(false);
                console.log('sin confirmacion');
            }
        } catch (error) {
            toast.error('Error en la eliminacion del pago adicional:', error);
            console.error('Resultado de la eliminacion:', error);
        }
    };

    const handleModalOkExtra = async () => {
        try {
            const id = pagoExtraEdit.pagextpagcod.pagprocod;
            pagoExtraEdit.pagextpagcod = id;
            console.log('PAGOEXTRA:', pagoExtraEdit);
            const resultado = await updatePagoE(pagoExtraEdit.pagextcod, pagoExtraEdit);
            console.log('Resultado de la actualización:', resultado);
            setEdicionExitosa(true);
            setVisibleExtra(false);
            toast.success('Edicion exitosa');
        } catch (error) {
            toast.error('Error en la actualización del pago adicional:', error);
            console.error('Resultado de la actualización:', error);
        }
    };

    useEffect(() => {
        const loadPagoExtra = async () => {
            try {
                const res = await getAllPagosE();
                setPagoExtra(res);
                console.log('EXTRA',res);
            } catch (error) {
                toast.error(`Error al cargar los pagos Extras: ${error.message}`);
            }
        };

        const loadPlacas = async () => {
            try {
                const res = await getAllPlacaVehiculo();
                setPlacas(res);
            } catch (error) {
                toast.error(`Error al cargar las placas: ${error.message}`);
            }
        };

        if (edicionExitosa) {
            setEdicionExitosa(false);
        }

        loadPlacas();
        loadPagoExtra();
    }, [edicionExitosa]);

    useEffect(() => {
        form.setFieldsValue({
            fecha: pagoExtraEdit.pagextfec,
            placa: pagoExtraEdit.pagextvehpla,
            descripcion: pagoExtraEdit.pagextdes,
            costo: pagoExtraEdit.pagextcos,
            monto: pagoExtraEdit.pagextmon,
        });
    }, [visibleExtra, pagoExtraEdit, form]);

    return (
        <>

        <div className='agregar-row' >
            <h4> Pagos Extra </h4>
            <MdOutlineAddToPhotos color='green' className="icon-add-detalles" onClick={handleCreateExtra} />
            <div className="line"></div>
        </div>

        <div className='vehiculo-detalles-table-container'>
                <Table className="tabla-tareos" dataSource={pagosProveedoresExtras} key={pagoExtra.pagextcod} columns={columnsExtra} />
                <div>
                <Modal
                    forceRender
                    title={`Edicion del pago extra`}
                    open={visibleExtra}
                    onCancel={handleModalCancel}
                    onOk={handleModalOkExtra}
                    footer={[
                    <div className='button-modal-container'>
                        <Button className='button-modal-agregar' onClick={handleModalOkExtra} >Guardar</Button>
                        <Button onClick={handleModalCancel}>Cancelar</Button>
                    </div>
                    ]}
                    centered
                    width={650} 
                    >
                    <div style={{ maxHeight: '800px',overflowY: 'auto' }}>
                        <Form
                            form={form}
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 16 }}
                            layout="vertical"
                            >
                            <div className='agregar-row' >
                            <Form.Item style={{ marginLeft:'10px', marginBottom: '5px' }}  
                            label='Fecha' labelCol={{ style: { marginBottom: '-30px' } }} >
                                <DatePicker
                                    style={{ width: '200px' }}
                                    getCalendarContainer={(trigger) => trigger.parentNode}
                                    placement="topLeft"
                                    onChange={(date) => {
                                        if (date) {
                                            console.log('Fecha seleccionada:', date);
                                            const formattedDate = date.format('YYYY-MM-DD');
                                            setpagoExtraEdit(prevState => ({ ...prevState, pagextfec: formattedDate }));
                                        }
                                    }}
                                />
                            </Form.Item >

                            <Form.Item  name="placa" style={{ marginLeft:'20px', marginBottom: '5px' }}  
                            label='Placa' labelCol={{ style: { marginBottom: '-30px' } }} >
                                <Select style={{ width: '200px' }}
                                onChange={(placa) => setpagoExtraEdit({ ...pagoExtraEdit, pagextvehpla: placa })}
                                >
                                    {placas.map((placa) => (
                                        <Select.Option key={placa} value={placa}>
                                        {placa}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                            </div>

                            <div>
                            <Form.Item name="descripcion" style={{ marginBottom: '5px', marginLeft:'60px' }} labelCol={{ style: { marginBottom: '-30px' } }} label='Descripción'>
                                <Input style={{ width: '490px'}} 
                                onChange={(e) => setpagoExtraEdit(prevState => ({ ...prevState, pagextdes: e.target.value }))}
                                placeholder="Escribe aquí"/>
                            </Form.Item>
                            </div>

                            <div className='agregar-row' >
                                <Form.Item name="costo" style={{ marginLeft:'20px', marginBottom: '5px'  }}  label='Costo' labelCol={{ style: { marginBottom: '-30px' } }} >
                                    <InputNumber style={{ width: '220px' }} placeholder="Escribe un número"
                                    onChange={(costo) => setpagoExtraEdit({ ...pagoExtraEdit, pagextcos: costo })} />
                                </Form.Item >

                                <Form.Item name="monto" style={{ marginLeft:'20px', marginBottom: '5px' }}  label='Monto' labelCol={{ style: { marginBottom: '-30px' } }} >
                                    <InputNumber style={{ width: '220px' }} placeholder="Escribe un número"
                                    onChange={(monto) => setpagoExtraEdit({ ...pagoExtraEdit, pagextmon: monto })} />
                                </Form.Item >
                            </div>
                            
                        </Form>
                    </div>
                    
                </Modal>
                </div>

                <div>
                <Modal
                forceRender
                    title={`Eliminacion pago extra`}
                    open={visibleExtraD}
                    onCancel={handleModalCancel}
                    onOk={handleModalDeleteExtra}
                    footer={[
                    <div className='button-modal-container'>
                        <Button className='button-modal-agregar' onClick={handleModalDeleteExtra} >Guardar</Button>
                        <Button onClick={handleModalCancel}>Cancelar</Button>
                    </div>
                    ]}
                    centered
                    width={450} 
                    >
                    <div style={{ textAlign: 'center' }}>
                        <p>¿Estás seguro que deseas eliminar este pago de vehículo?</p>
                        <Select
                            style={{ width: '50%' }}
                            placeholder="Seleccione una opción"
                            onChange={(value) => setconfirmacionDelete(value)}
                        >
                            <Select.Option value="si">Sí</Select.Option>
                            <Select.Option value="no">No</Select.Option>
                        </Select>
                    </div>
                </Modal>
                </div>

                <div>
                <Modal
                    forceRender
                    title={`Agregar pago extra`}
                    open={visibleExtraC}
                    onCancel={handleModalCancel}
                    //onOk={handleModalOkExtra}
                    footer={[
                    <div className='button-modal-container'>
                        <Button className='button-modal-agregar' onClick={handleModalCreateExtra} >Guardar</Button>
                        <Button onClick={handleModalCancel}>Cancelar</Button>
                    </div>
                    ]}
                    centered
                    width={650} 
                    >
                    <div style={{ maxHeight: '800px',overflowY: 'auto' }}>
                        <Form
                            form={form}
                            labelCol={{ span: 6 }}
                            wrapperCol={{ span: 16 }}
                            layout="vertical"
                            onFinish={handleModalCreateExtra}
                            >
                            <div className='agregar-row' >
                            <Form.Item style={{ marginLeft:'10px', marginBottom: '5px' }}  label='Fecha' 
                            labelCol={{ style: { marginBottom: '-30px' } }}
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor, ingrese una fecha',
                                },
                            ]}>
                                <DatePicker
                                    style={{ width: '200px' }}
                                    getCalendarContainer={(trigger) => trigger.parentNode}
                                    placement="topLeft"
                                    onChange={(date) => {
                                        if (date) {
                                            const formattedDate = date.format('YYYY-MM-DD');
                                            setpagoExtraNew(prevState => ({ ...prevState, pagextfec: formattedDate }));
                                        }
                                    }}
                                />
                            </Form.Item >

                            <Form.Item style={{ marginLeft:'20px', marginBottom: '5px' }}  
                            label='Placa' labelCol={{ style: { marginBottom: '-30px' } }} 
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor, ingrese una placa',
                                },
                            ]}>
                                <Select style={{ width: '200px' }}
                                onChange={(placa) => setpagoExtraNew({ ...pagoExtraNew, pagextvehpla: placa })}
                                >
                                    {placas.map((placa) => (
                                        <Select.Option key={placa} value={placa}>
                                        {placa}
                                        </Select.Option>
                                    ))}
                                </Select>
                            </Form.Item>
                            </div>

                            <div>
                            <Form.Item style={{ marginBottom: '5px', marginLeft:'60px' }} 
                            labelCol={{ style: { marginBottom: '-30px' } }} label='Descripción'
                            rules={[
                                {
                                    required: true,
                                    message: 'Por favor, ingrese una descripcion',
                                },
                            ]}>
                                <Input style={{ width: '490px'}} 
                                onChange={(e) => setpagoExtraNew(prevState => ({ ...prevState, pagextdes: e.target.value }))}
                                placeholder="Escribe aquí"/>
                            </Form.Item>
                            </div>

                            <div className='agregar-row' >
                                <Form.Item style={{ marginLeft:'20px', marginBottom: '5px'  }}  
                                label='Costo' labelCol={{ style: { marginBottom: '-30px' } }} 
                                rules={[
                                    {
                                        required: true,
                                        message: 'Por favor, ingrese un costo',
                                    },
                                ]}>
                                    <InputNumber style={{ width: '220px' }} placeholder="Escribe un número"
                                    onChange={(costo) => setpagoExtraNew({ ...pagoExtraNew, pagextcos: costo })} />
                                </Form.Item >

                                <Form.Item style={{ marginLeft:'20px', marginBottom: '5px' }}  
                                label='Monto' labelCol={{ style: { marginBottom: '-30px' } }} 
                                rules={[
                                    {
                                        required: true,
                                        message: 'Por favor, ingrese un monto',
                                    },
                                ]}>
                                    <InputNumber style={{ width: '220px' }} placeholder="Escribe un número"
                                    onChange={(monto) => setpagoExtraNew({ ...pagoExtraNew, pagextmon: monto })} />
                                </Form.Item >
                            </div>
                            
                        </Form>
                    </div>
                </Modal>
                </div>

            </div>
        </>
    );
}

export { PagoExtra };
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';

import { createPago } from '../../api/PagoProveedores.api';
import { getAllProveedores } from "../../api/Proveedores.api";
import { getAllEstadoPagPro } from '../../api/EstadoPagoProveedor.api';
import { toast } from 'react-toastify';

function FormCreatePagPro() {
    const navigate = useNavigate();

    //Para el formulario
    const [proveedores, setProveedores] = useState([]);
    const [estadosPago, setEstadosPago] = useState([]);

    //Estado de los registros de mi formulario
    const {
        register,
        handleSubmit,  
        formState: { errors },
    } = useForm();

    const handleRegresarClick = () => {
        navigate(`/pagosProveedores/`);
    };

    const onSubmit = async (data) => {
        try {
            const resPago = await createPago(data);
            toast.success('Pago creado con éxito');
            navigate(-1);
        } catch (error) {
            console.log(data);
            toast.error(`Error al crear el pago: ${error.message}`);
        }
    };

    const loadProveedores = async () => {
        try {
            const res = await getAllProveedores();
            setProveedores(res);
        } catch (error) {
            toast.error(`Error al cargar los proveedores: ${error.message}`);
        }
    };

    const loadEstadoPagoPro = async () => {
        try {
            const res = await getAllEstadoPagPro();
            setEstadosPago(res);
        } catch (error) {
            toast.error(`Error al cargar los estados: ${error.message}`);
        }
    };

    useEffect(() => {
        loadProveedores();
        loadEstadoPagoPro();
    }, []);

  return (
    <div className='contenedor-componente'>
    <div className='contenedor-formulario'>
    <h2>Registrar nuevo pago</h2>
    <form onSubmit={handleSubmit(onSubmit)}>
        <div className='agregar-row'>
            <label className='form-row1'>
                Proveedor
                <select className='formulario-pago' name="proveedor" defaultValue={''}
                    {...register('pagproprocod', { required: true })} >
                    <option value='' >Seleccionar proveedor</option>
                    {proveedores && proveedores.map(proveedor =>(
                        <option value={proveedor.procod} key={proveedor.procod}>{proveedor.prorazsoc}</option>
                    ))}
                </select>
                {errors.pagproprocod?.type === 'required' && <p className='text-error'>*El campo es requerido</p>}  
            </label>

            <label className='form-row1'>
                Fecha
                <input className='formulario-pago' type='date'
                    {...register('pagprofec', { required: true })}
                />
                {errors.pagprofec?.type === 'required' && <p className='text-error'>*El campo es requerido</p>}
            </label>

            <label className='form-row1'>
                Estado del pago
                <select className='formulario-pago' name="estadoPago" defaultValue={''}
                    {...register('pagproestpagpro', { required: true })} >
                    <option value='' >Seleccionar proveedor</option>
                    {estadosPago && estadosPago.map(estadoPa =>(
                        <option value={estadoPa.estpagprocod} key={estadoPa.estpagprocod}>{estadoPa.estpagprodes}</option>
                    ))}
                </select>
                {errors.pagproestpagpro?.type === 'required' && <p className='text-error'>*El campo es requerido</p>}  
            </label>
        </div>

        <div className='agregar-row'>
            <label>
                Observacion
                <input className='formulario-pago-detalle'  type="text" placeholder='Ninguna'
                    {...register('pagproobs', { required: true })}
                    defaultValue={''}
                />
                {errors.pagproobs?.type === 'required' && (
                    <p className='text-error'>*El campo es requerido</p>
                )}
            </label>

            <label>
                Monto Total
                <input className='formulario-pago2' type='number'
                    {...register('pagprototal', { required: true })}
                    defaultValue={0}
                />
                {errors.pagprototal?.type === 'required' && (
                    <p className='text-error'>*El campo es requerido</p>
                )}
            </label>

        </div>

        <div className='contenedor-botones'>
            <button className='btn-cancelar' type='button' onClick={handleRegresarClick}>Cancelar</button>
            <button className='btn-registrar' type='submit'>Registrar</button>
        </div>

    </form>
    
    </div>

    </div>
  )
}

export default FormCreatePagPro;
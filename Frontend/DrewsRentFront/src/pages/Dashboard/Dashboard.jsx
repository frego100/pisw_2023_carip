import React, { useState, useEffect, useRef } from "react";

import Chart from "chart.js/auto";

import "../../styles/Dashboard.css";
import "../../styles/General.css";

import { getAllTotalRegistro } from "../../api/TotalRegistros.api";
import { getProyectoPorRegion } from "../../api/ProyectoPorRegion.api";
import { getVehiculoPorEstado } from "../../api/VehiculosPorEstado.api";
import { getOperadoresPorEstado } from "../../api/OperadorPorEstado.api";
import {
  getAllConteoTareoFecha,
  getAllConteoTareoFechaPaginated,
} from "../../api/ConteoLeyendaFecha.api";

function Dashboard() {
  const [totalRegistros, setTotalRegistros] = useState({});
  const [proyectoRegion, setProyectoRegion] = useState([]);
  const [vehiculoEstado, setVehiculoEstado] = useState([]);
  const [operadorEstado, setOperadoresEstado] = useState([]);
  const [conteoTareoFecha, setConteoTareoFecha] = useState([]);

  const chartRef = useRef(null);

  useEffect(() => {
    async function loadTotalRegistros() {
      const totalRegistrosArray = await getAllTotalRegistro();

      const totalRegistros = totalRegistrosArray[0];
      setTotalRegistros(totalRegistros);
    }

    async function loadProyectoRegion() {
      const proyectoRegion = await getProyectoPorRegion();
      setProyectoRegion(proyectoRegion);
    }

    async function loadVehiculoEstadoData() {
      const vehiculoEstado = await getVehiculoPorEstado();
      //console.log(vehiculoEstado);
      setVehiculoEstado(vehiculoEstado);
    }

    async function loadOperadorEstadoData() {
      const operadorEstado = await getOperadoresPorEstado();
      //console.log(operadorEstado);
      setOperadoresEstado(operadorEstado);
    }

    async function loadConteoTareoFechaData() {
      const conteoTareoFecha = await getAllConteoTareoFechaPaginated();
      setConteoTareoFecha(conteoTareoFecha);
      // const conteoTareoFecha = await getAllConteoTareoFecha();
      // //console.log(conteoTareoFecha);
      // setConteoTareoFecha(conteoTareoFecha);
    }

    loadTotalRegistros();
    loadProyectoRegion();
    loadVehiculoEstadoData();
    loadOperadorEstadoData();
    loadConteoTareoFechaData();
  }, []);

  useEffect(() => {
    if (proyectoRegion.length > 0) {
      if (chartRef.current) {
        chartRef.current.destroy(); // Destruye el gráfico anterior si existe
      }
      const ctx = document.getElementById("barChart").getContext("2d");
      const newChart = new Chart(ctx, {
        type: "bar",
        data: {
          labels: proyectoRegion.map((item) => item.region_nombre),
          datasets: [
            {
              label: "Cantidad de Proyectos",
              data: proyectoRegion.map((item) => item.cantidad_proyectos),
              backgroundColor: "rgba(75, 192, 192, 0.2)",
              borderColor: "rgba(75, 192, 192, 1)",
              borderWidth: 1,
            },
          ],
        },
        options: {
          scales: {
            y: {
              beginAtZero: true,
              ticks: {
                stepSize: 1, // Establece el intervalo de los valores en el eje Y
                precision: 0, // Establece la precisión a 0 para mostrar números enteros
              },
            },
          },
          responsive: true,
          maintainAspectRatio: false,
        },
      });
      chartRef.current = newChart; // Almacena el nuevo gráfico en la referencia
    }
  }, [proyectoRegion]);

  useEffect(() => {
    async function loadVehiculoEstado() {
      const data = await getVehiculoPorEstado();
      //const ctx = document.getElementById('pieChart').getContext('2d');

      const canvas = document.getElementById("pieChart");

      const existingChart = Chart.getChart(canvas);
      if (existingChart) {
        existingChart.destroy();
      }

      const ctx = canvas.getContext("2d");

      const pieData = {
        labels: data.map((item) => item.estado_vehiculo),
        datasets: [
          {
            label: "Cantidad de Vehículos",
            data: data.map((item) => item.cantidad_vehiculos),
            backgroundColor: "rgba(75, 192, 192, 0.2)",
            borderColor: "rgba(75, 192, 192, 1)",
            borderWidth: 1,
          },
        ],
      };

      new Chart(ctx, {
        type: "pie",
        data: pieData,
        options: {
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
            title: {
              display: true,
              text: "Estado de Vehiculos",
              font: {
                size: 13,
              },
            },
          },
        },
      });
    }

    loadVehiculoEstado();
  }, []);

  useEffect(() => {
    async function loadOperadorEstado() {
      const data = await getOperadoresPorEstado();
      //const ctx = document.getElementById('pieChart2').getContext('2d');
      const canvas = document.getElementById("pieChart2");

      const existingChart = Chart.getChart(canvas);
      if (existingChart) {
        existingChart.destroy();
      }

      const ctx = canvas.getContext("2d");

      const pieData = {
        labels: data.map((item) => item.estado_operador),
        datasets: [
          {
            label: "Cantidad de Operadores",
            data: data.map((item) => item.cantidad_operador),
            backgroundColor: "rgba(75, 192, 192, 0.2)",
            borderColor: "rgba(75, 192, 192, 1)",
            borderWidth: 1,
          },
        ],
      };

      new Chart(ctx, {
        type: "pie",
        data: pieData,
        options: {
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
            title: {
              display: true,
              text: "Estado de Operadores",
              font: {
                size: 13,
              },
            },
          },
        },
      });
    }

    loadOperadorEstado();
  }, []);

  const LineChart = () => {
    const chartRef = useRef(null);

    useEffect(() => {
      async function loadConteoTareoFechaData() {
        try {
          // const data = await getAllConteoTareoFecha();
          const data = await getAllConteoTareoFechaPaginated();
          setTimeout(() => {
            renderChart(data);
            //console.error('Si hay datos');
          }, 100);
        } catch (error) {
          console.error("Error al obtener datos:", error);
        }
      }

      loadConteoTareoFechaData();
    }, []);

    function renderChart(data) {
      const canvas = chartRef.current;
      if (!canvas) {
        //console.error('El elemento canvas es nulo.');
        return;
      }
      const existingChart = Chart.getChart(canvas);
      if (existingChart) {
        existingChart.destroy();
      }

      const ctx = canvas.getContext("2d");
      //console.log('Contexto del canvas:', ctx);
      const lineData = {
        labels: data.map((item) => item.tarfec),
        datasets: [
          {
            label: "DT",
            data: data.map((item) => item.dt),
            borderColor: "#6B97E4E5",
            fill: false,
          },
          {
            label: "DNT",
            data: data.map((item) => item.dnt),
            borderColor: "#A2A2A2",
            fill: false,
          },
          {
            label: "MP",
            data: data.map((item) => item.mp),
            borderColor: "#FFC107",
            fill: false,
          },
          {
            label: "MC",
            data: data.map((item) => item.mc),
            borderColor: "#FF8A00",
            fill: false,
          },
          {
            label: "TSI",
            data: data.map((item) => item.tsi),
            borderColor: "#DC3545",
            fill: false,
          },
        ],
      };

      new Chart(ctx, {
        type: "line",
        data: lineData,
        options: {
          responsive: true,
          maintainAspectRatio: false,
          plugins: {
            title: {
              display: true,
              text: "Estado de Tareos",
              font: {
                size: 13,
              },
            },
          },
          scales: {
            y: {
              beginAtZero: true,
              precision: 0,
              ticks: {
                stepSize: 5, // Configura el paso para los ticks del eje y a 5
              },
            },
          },
        },
      });
      console.log(data);
    }

    return <canvas ref={chartRef} className="graficos" />;
  };

  return (
    <>
      <div className="contenedor-general">
        <div className="dashboard-cotenedor">
          <div className="dashboard-detalles-count">
            <div className="detalles-alquiler">
              <div className="detalles-icono">
                <img src="/src/assets/WhiteIcons/AlquilerIcon.png" />
              </div>
              <div className="detalles-texto-column">
                <h2>{totalRegistros.total_alquileres}</h2>
                <p>Alquileres</p>
              </div>
            </div>
            <div className="detalles-proyectos">
              <div className="detalles-icono">
                <img src="/src/assets/WhiteIcons/ProjectIcon.png" />
              </div>
              <div className="detalles-texto-column">
                <h2>{totalRegistros.total_proyectos}</h2>
                <p>Proyectos</p>
              </div>
            </div>
            <div className="detalles-vehiculos">
              <div className="detalles-icono">
                <img src="/src/assets/WhiteIcons/CarsIcon.png" />
              </div>
              <div className="detalles-texto-column">
                <h2>{totalRegistros.total_vehiculos}</h2>
                <p>Vehiculos</p>
              </div>
            </div>
            <div className="detalles-operadores">
              <div className="detalles-icono">
                <img src="/src/assets/WhiteIcons/OperatorIcon.png" />
              </div>
              <div className="detalles-texto-column">
                <h2>{totalRegistros.total_operadores}</h2>
                <p>Operadores</p>
              </div>
            </div>
            <div className="detalles-proveedores">
              <div className="detalles-icono">
                <img src="/src/assets/WhiteIcons/ManagersIcon.png" />
              </div>
              <div className="detalles-texto-column">
                <h2>{totalRegistros.total_proveedores}</h2>
                <p>Proveedores</p>
              </div>
            </div>
          </div>

          <div className="dashboard-detalles-graficos">
            <div className="dashboard-detalles-graficos1">
              <canvas className="graficos" id="barChart"></canvas>
            </div>
          </div>

          <div className="dashboard-detalles-graficos">
            <div className="dashboard-detalles-graficos2">
              <canvas className="graficos" id="pieChart"></canvas>
            </div>
            <div className="dashboard-detalles-graficos3">
              <canvas className="graficos" id="pieChart2"></canvas>
            </div>
          </div>

          <div className="dashboard-detalles-graficos">
            <div className="dashboard-detalles-graficos4">
              <LineChart></LineChart>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default Dashboard;

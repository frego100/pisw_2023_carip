import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getEmpresa, updateEmpresa } from '../../api/Empresa.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';

function FormDeleteEmpresa ({ toggle, empresaId, loadEmpresas }) {

    /*Datos del proyecto seleccionado*/ 
    const [empresa, setEmpresa] = useState([]);
    
    /*Datos de proyectos para la tabla*/   
    const [estReg, setEstReg] = useState([]);

    /*Estado de los registros de mi formulario*/
    const {
        register,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm();

    const onSubmit = handleSubmit(async (data) => {
        try {
            const res = await updateEmpresa(empresa.empcod, data);
            toggle();
            loadEmpresas();
            toast.success("Empresa eliminada con éxito");
        } catch (error) {
            toast.error('Error al eliminar empresa');
        }
    });

    useEffect(() => {
        async function loadEmpresa() {
            try {
                const res = await getEmpresa(empresaId);
                setEmpresa(res.data);
                setValue('empnom', res.data.empnom);
                setValue('emprazsoc', res.data.emprazsoc);
                setValue('empruc', res.data.empruc);
                setValue('empdir', res.data.empdir);
                setValue('empubicod', res.data.empubicod.discod);
            } catch (error) {
                toast.error(`Error al cargar la empresa: ${error.message}`);
            }
        }
        async function loadEstReg() {
            try {
                const res = await getEstadoRegistro('Inactivo');
                if (res === null) {
                    console.log('El estado de registro no existe. AGREGAR ESTADO DE REGISTRO A LA BASE DE DATOS');
                } else {
                    setValue('empestreg', res);
                    setEstReg(res);
                }
            } catch (error) {
                toast.error(`Error al cargar los estados: ${error.message}`);
            }
        }
        loadEmpresa();
        loadEstReg();
        }, [empresaId]
    );

    return (
        <>
        <h3>¿Está seguro que quiere eliminar la empresa  "{empresa.empnom}"?</h3>
        <form onSubmit={onSubmit}>
            {/* Campo de selección para la región del proyecto */}
            <label>
                Estado de empresa
                <select className='input-select' {...register('empestreg', { required: true })}>
                    <option value={estReg} >Inactivo</option>
                </select>
                {errors.empestreg?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
            </label>
            {/* Botones de cancelar/eliminar */}
            <div className='contenedor-btn'>
                <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
                <button className='btn-eliminar' type='submit'>Eliminar</button>
            </div>
        </form>
        </>
    );
}

export { FormDeleteEmpresa };
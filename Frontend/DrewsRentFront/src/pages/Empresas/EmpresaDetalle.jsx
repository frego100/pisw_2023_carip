import React, { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { getEmpresaById } from '../../api/Empresa.api';
import { toast } from 'react-toastify';

function EmpresaDetalle() {

  const params = useParams();
  /*Datos de la empresa*/ 
  const [empresa, setEmpresa] = useState([]);

  const navigate = useNavigate();

  const handleRegresar = () => {
    navigate('/empresas');
  };

  useEffect(() => {
    const loadEmpresa = async () => {
      try {
        const response = await getEmpresaById(params.id);
        setEmpresa(response.data);
      } catch (error) {
        toast.error(`Error al cargar la empresa`);
      }
  };
  loadEmpresa();
  }, []);

  return (
    <div className='contenedor-componente'>
      <h2>Empresa: {empresa.empnom} </h2>
      <div className='contenedor-detalle'>
        <div className='detalle-datos'>
          <h6>RUC</h6>
          <p> {empresa.empruc} </p>
          <h6>Razon Social</h6>
          <p> {empresa.emprazsoc}</p>
          <h6>Nombre</h6>
          <p> {empresa.empnom}</p>
          <h6>Region</h6>
          <p> {empresa.empubicod?.disprocod.proregcod.regnom} -  {empresa.empubicod?.disprocod.proregcod.regnomacro}</p>
          <h6>Provincia</h6>
          <p> {empresa.empubicod?.disprocod.pronom} </p>
          <h6>Distrito</h6>
          <p> {empresa.empubicod?.disnom} </p>
          <h6>Direccion</h6>
          <p> {empresa.empdir}</p>
        </div>
      </div>
      <div className='contenedor-btn-detalle'>
        <button className='btn-regresar' type='button' onClick={ () => handleRegresar() } >Regresar</button>
      </div>      
    </div>
  );
}

export default EmpresaDetalle;

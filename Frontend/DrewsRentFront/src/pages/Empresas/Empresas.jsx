import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllEmpresasActivos } from '../../api/Empresa.api';
import empresaColumns from '../../Utils/ComponentesUtils/empresaColumns';
import  Modal  from '../../components/Modal';
import { Table } from '../../components/Tabla/Table';
import { FormCreateEmpresa } from './FormCreateEmpresa';
import { FormEditEmpresa } from './FormEditEmpresa';
import { FormDeleteEmpresa } from './FormDeleteEmpresa';
import '../../styles/ContenedorComp.css';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Empresas() {

  /*Datos de empresas para la tabla*/    
  const [empresas, setEmpresas] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);
  
  /*Estado del idempresaeleccionado */
  const [selectedIdEmpresa, setSelectedIdEmpresa] = useState();

  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  const handleEdit = (idEmpresa) => {
    setSelectedIdEmpresa(idEmpresa);
    setActiveFormEdit(!activeFormEdit);
  };
  const handleDelete = (idEmpresa) => {
    setSelectedIdEmpresa(idEmpresa);
    setActiveFormDelete(!activeFormDelete);
  };
  
  const loadEmpresas = async () => {
    try {
      const res = await getAllEmpresasActivos();
      setEmpresas(res);
    } catch (error) {
      toast.error(`Error al cargar las empresas`);
    }
  };

  useEffect(() => {
    loadEmpresas();
  }, []);

  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Empresas</h2>
        <div className='btn-agregar' onClick={toggle}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de empresas */}
      <Table
        nombreID={'empcod'}
        columns={empresaColumns()}
        data={empresas}
        nombre={'empresas'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={true}
      />         
    </div>

    <ToastContainer />

    {/* Modal para crear nuevo empresa */}
    <Modal active={activeFormCreate} toggle={toggle}>
      <FormCreateEmpresa toggle={toggle} loadEmpresas={loadEmpresas}/>      
    </Modal>
    
    {/* Modal para editar empresa existente */}
    <Modal active={activeFormEdit} toggle={handleEdit}>
      <FormEditEmpresa toggle={handleEdit} empresaId={selectedIdEmpresa} loadEmpresas={loadEmpresas} />      
    </Modal>

    {/* Modal para eliminar empresa */}
    <Modal active={activeFormDelete} toggle={handleDelete}>
      <FormDeleteEmpresa toggle={handleDelete} empresaId={selectedIdEmpresa} loadEmpresas={loadEmpresas} /> 
    </Modal>
    </>
  )
}

export default Empresas;
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getEmpresa, updateEmpresa } from '../../api/Empresa.api';
import { getAllDistritos } from '../../api/Distrito.api';
import { getAllProvincias } from '../../api/Provincia.api';
import { getAllRegiones } from '../../api/Region.api';
import { toast } from 'react-toastify';

function FormEditEmpresa({ toggle, empresaId, loadEmpresas }) {

  /*Datos del proyecto seleccionado*/ 
  const [empresa, setEmpresa] = useState([]);
    
  const [regiones, setRegiones] = useState([]);
  const [provincias, setProvincias] = useState([]);
  const [distritos, setDistritos] = useState([]);

  const [selectedRegion, setSelectedRegion] = useState('');
  const [selectedProvincia, setSelectedProvincia] = useState('');
  const [selectedDistrito, setSelectedDistrito] = useState('');

    /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,   
    setValue,
    formState: { errors },
  } = useForm();

  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

    // Función de validación para RUC (solo números y tamaño 11)
  const validateRUC = (value) => {
    return /^\d{11}$/.test(value);
  };

  const onSubmit = handleSubmit(async data => {
    try {
      const res = await updateEmpresa(empresa.empcod, data);
      toggle();
      loadEmpresas();
      toast.success('Empresa editada con éxito');
    } catch (error) {
      toast.error(`Error al editar la empresa: ${error.message}`);
    }
  });

  useEffect(() => {
    async function loadEmpresa() {
      try {
        const res = await getEmpresa(empresaId);
        setEmpresa(res.data);
        setValue('empnom', res.data.empnom);
        setValue('emprazsoc', res.data.emprazsoc);
        setValue('empruc', res.data.empruc);
        setValue('empdir', res.data.empdir);
        setValue('empubicod', res.data.empubicod.discod);
        setValue('empestreg', res.data.empestreg.estregcod);
        setSelectedRegion(res.data.empubicod.disprocod.proregcod.regcod);
        setSelectedProvincia(res.data.empubicod.disprocod.procod);
        setSelectedDistrito(res.data.empubicod.discod);  
      } catch (error) {
        toast.error(`Error al cargar las empresas: ${error.message}`);
      }
    }
    async function loadDistritos () {
      try {
        const res = await getAllDistritos();
        setDistritos(res);
      } catch (error) {
        toast.error(`Error al cargar los distritos: ${error.message}`);
      }
    };
    const loadProvincias = async () => {
      try {
        const res = await getAllProvincias();
        setProvincias(res);
      } catch (error) {
        toast.error(`Error al cargar las provincias: ${error.message}`);
      }
    };

    const loadRegiones = async () => {
      try {
        const res = await getAllRegiones();
        setRegiones(res);
      } catch (error) {
        toast.error(`Error al cargar las regiones: ${error.message}`);
      }
    };
    loadEmpresa();
    loadDistritos();
    loadProvincias();
    loadRegiones();
  }, [empresaId]
  );


  return (
    <>
    <h2>Editar empresa "{empresa?.empnom}" </h2>
    <form onSubmit={onSubmit}>
      <label>
        RUC
        <input
          className='input-text'
          type="number"
          placeholder='RUC'
          {...register('empruc', { required: true,
            validate: (value) => validateRUC(value),
          })}
        />
        {errors.empruc?.type === 'required' && <p className='text-error'>*El campo RUC es requerido</p>}
        {errors.empruc?.type === 'validate' && <p className='text-error'>*El RUC solo debe ser numérico y tener exactamente 11 caracteres</p>}
      </label>
      <label>
        Razon social
        <input
          className='input-text'
          type="text"
          placeholder='Razon social'
          {...register('emprazsoc', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.emprazsoc?.type === 'required' && <p className='text-error'>*El campo razon social es requerido</p>}
        {errors.emprazsoc?.type === 'validate' && <p className='text-error'>*El campo razon social no debe superar los 45 caracteres</p>}
      </label>
      {/* Campo de entrada para el nombre de la Empresa */}
      <label>
        Nombre
        <input
          className='input-text'
          type="text"
          placeholder='Nombre de la empresa'
          {...register('empnom', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.empnom?.type === 'required' && (
          <p className='text-error'>*El campo nombre es requerido</p>
        )}
        {errors.empnom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}  

      </label>
      <div className='form-fila'>
        <label className='flex1'>
          Region
          <select className='input-select'
            name="regiones"
            value={selectedRegion}
            onChange={(e) => {
              setSelectedRegion(e.target.value);
              setSelectedProvincia('');
              setSelectedDistrito('');
            }}
            >
            <option value='' >Seleccionar Region</option>
            {regiones && regiones.map( (region) =>(
              <option value={region.regcod} key={region.regcod}>{region.regnom}</option>
            ))}
          </select>
        </label>
        <label className='flex1'>
          Provincia
          <select
            style={{
              backgroundColor: !selectedRegion ? '#f2f2f2' : 'inherit',
              color: !selectedRegion ? '#999' : 'inherit',
            }}
            className='input-select'
            name="provincias"
            value={selectedProvincia}
            disabled={!selectedRegion}
            onChange={(e) => {
              setSelectedProvincia(e.target.value);
              setSelectedDistrito('');
            }}
          >
            <option value='' >Seleccionar Provincia</option>
            {provincias && provincias
              .filter((provincia) => provincia.proregcod.regcod === parseInt(selectedRegion))
              .map((provincia) =>(
                <option value={provincia.procod} key={provincia.procod}>{provincia.pronom}</option>
            ))}
          </select>
        </label>
        <label className='flex1'>
          Distrito
          <select 
            style={{
              backgroundColor: !selectedProvincia ? '#f2f2f2' : 'inherit',
              color: !selectedProvincia ? '#999' : 'inherit',
            }}
            className='input-select'
            name="distritos"
            disabled={!selectedProvincia}
            {...register('empubicod', { required: true })}
            value={selectedDistrito}
            onChange={(e) => 
              setSelectedDistrito(e.target.value)}
          >
            <option value='' >Seleccionar Distrito</option>
            {distritos && distritos
              .filter((distrito) => distrito.disprocod.procod === parseInt(selectedProvincia))
              .map((distrito) => (
                <option value={distrito.discod} key={distrito.discod}>{distrito.disnom}</option>
            ))}
          </select>
          {errors.empubicod?.type === 'required' && <p className='text-error'>*El campo distrito es requerido</p>}  
        </label>
      </div>
      <label className='flex1'>
        Direccion
        <input
          className='input-text'
          type="text"
          placeholder='Direccion'
          {...register('empdir', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.empdir?.type === 'required' && <p className='text-error'>*El campo direccion es requerido</p>}
        {errors.empdir?.type === 'validate' && <p className='text-error'>*El campo direccion no debe superar los 45 caracteres</p>}    
      </label>                
    {/* Botones de cancelar/guardar */}
    <div className='contenedor-btn'>
      <button className='btn-cancelar' type='button' onClick={ toggle }>Cancelar</button>
      <button className='btn-guardar' type='submit'>Guardar</button>
    </div>
    </form>
    </>
  );
}

export { FormEditEmpresa };
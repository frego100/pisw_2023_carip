import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getArea, updateArea } from '../../api/Area.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { toast } from 'react-toastify';


function FormDeleteArea({ toggle, areaId, loadAreas }) {

  /*Datos del proyecto seleccionado*/ 
  const [area, setArea] = useState([]);
  const [estReg, setEstReg] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm();

  const onSubmit = handleSubmit(async (data) => {
    try {
      const res = await updateArea(area.arecod, data);
      toggle();
      loadAreas();
      toast.success("Area eliminada con éxito");
    } catch (error) {
      toast.error(`Error al eliminar el area`);
    }
  });

  useEffect(() => {
    async function loadArea() {
      const res = await getArea(areaId);
      setArea(res.data);
      setValue('arenom', res.data.arenom);
    }
    async function loadEstReg() {
      try {
        const res = await getEstadoRegistro('Inactivo');
        if (res === null) {
          toast.warning('El estado de registro Inactivo no existe');
        } else {
          setValue('areestreg', res);
          setEstReg(res);
        }
      } catch (error) {
        toast.error(`Error al cargar los estados`);
      }
    }
    loadArea();
    loadEstReg();
    }, [areaId]
  );

  return (
    <>
    <h3>¿Está seguro que quiere eliminar el area "{area.arenom}"?</h3>
    <form onSubmit={onSubmit}>
      <label>
        Estado de area
        <select className='input-select' {...register('areestreg', { required: true })}>
          <option value={estReg} >Inactivo</option>
        </select>
        {errors.areestreg?.type === 'required' && <p className='text-error'>*El campo estado es requerido</p>}  
      </label>
      {/* Botones de cancelar/eliminar */}
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-eliminar' type='submit'>Eliminar</button>
      </div>
    </form>
    </>
  );
}

export { FormDeleteArea };
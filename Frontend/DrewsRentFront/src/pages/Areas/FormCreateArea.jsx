import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { createArea } from '../../api/Area.api';
import { getEstadoRegistro } from '../../api/EstadoRegistro.api';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';

function FormCreateArea({toggle, loadAreas}) {

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,  
    setValue,
    formState: { errors },
  } = useForm();

  const navigate = useNavigate();

  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

  // Función llamada al enviar el formulario
  const onSubmit = handleSubmit(async data => {
    try {
      const res = await createArea(data);
      toggle();
      loadAreas();
      toast.success('Area creada con éxito');
    } catch (error) {
      toast.error(`Error al crear el area`);
    }
  });

  const loadEstReg = async () => {
    try {
      const resid = await getEstadoRegistro('Activo');
      if (resid === null) {
        toast.error(`El estado de registro Activo no existe`);
      } else {
        setValue('areestreg', resid);
      }
    } catch (error) {
      toast.error(`Error al cargar el estado de registro: ${error.message}`);
    }
  };

  useEffect(() => {
    loadEstReg();
    }, []
  );

    // Renderizar
  return (
    <>
    <h2>Registrar nueva Area</h2>
    <form onSubmit={onSubmit}>
      <label>
        Nombre
        <input
          className='input-text'
          type="text"
          placeholder='Nombre del area'
          {...register('arenom', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.arenom?.type === 'required' && <p className='text-error'>*El campo nombre es requerido</p>}
        {errors.arenom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}    
      </label>
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={toggle}>Cancelar</button>
        <button className='btn-registrar' type='submit'>Registrar</button>
      </div>
    </form>
    </>
  )
}

export { FormCreateArea };
import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { getArea, updateArea } from '../../api/Area.api';
import { toast } from 'react-toastify';

function FormEditArea({ toggle, areaId, loadAreas }) {

  /*Datos del area seleccionada*/ 
  const [area, setArea] = useState([]);

  /*Estado de los registros de mi formulario*/
  const {
    register,
    handleSubmit,   
    setValue,
    formState: { errors },
  } = useForm();

  const validateMaxSize = (value) => {
    return value.length <= 45;
  };

  const onSubmit = handleSubmit(async data => {
    try {
      const res = await updateArea(area.arecod, data);
      toggle();
      loadAreas();    
      toast.success(`Area editada con éxito`);
    } catch (error) {
      toast.error(`Error al editar el area`);
    }
  });

  useEffect(() => {
    async function loadArea() {
      try {
        const res = await getArea(areaId);
        setArea(res.data);
        setValue('arenom', res.data.arenom);
        setValue('areestreg', res.data.areestreg.estregcod);
      } catch (error) {
        toast.error(`Error al cargar las areas: ${error.message}`);
      }
    }
    loadArea();
    }, [areaId]
  );

  return (
    <>
    <h2>Editar area "{area?.arenom}" </h2>
    <form onSubmit={onSubmit}>
      {/* Campo de entrada para el nombre del proyecto */}
      <label>
        Nombre
        <input
          className='input-text'
          type="text"
          placeholder='Nombre del area'
          {...register('arenom', { required: true,
            validate: (value) => validateMaxSize(value),
          })}
        />
        {errors.arenom?.type === 'required' && <p className='text-error'>*El campo nombre es requerido</p>}
        {errors.arenom?.type === 'validate' && <p className='text-error'>*El campo nombre no debe superar los 45 caracteres</p>}    
      </label>         
      {/* Botones de cancelar/guardar */}
      <div className='contenedor-btn'>
        <button className='btn-cancelar' type='button' onClick={ toggle }>Cancelar</button>
        <button className='btn-guardar' type='submit'>Guardar</button>
      </div>
    </form>
    </>
  );
}

export { FormEditArea };
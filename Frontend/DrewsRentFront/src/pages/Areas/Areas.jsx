import React, { useEffect, useState } from 'react';
import { BsFillPlusCircleFill } from "react-icons/bs";
import { getAllAreasActivos } from '../../api/Area.api';
import areaColumns from '../../Utils/ComponentesUtils/areaColumns';
import  Modal  from '../../components/Modal';
import { Table } from '../../components/Tabla/Table';
import { FormCreateArea } from './FormCreateArea';
import { FormEditArea } from './FormEditArea';
import { FormDeleteArea } from './FormDeleteArea';
import '../../styles/ContenedorComp.css';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function Areas() {

  /*Datos de proyectos para la tabla*/    
  const [areas, setAreas] = useState([]);   

  /*Estado del formulario para create, edit y delete*/
  const [activeFormCreate, setActiveFormCreate] = useState(false);
  const [activeFormEdit, setActiveFormEdit] = useState(false);
  const [activeFormDelete, setActiveFormDelete] = useState(false);
  
  /*Estado del idProyectoSeleccionado */
  const [selectedIdArea, setSelectedIdArea] = useState();
  
  /*Funciones */
  const toggle = () => {
    setActiveFormCreate(!activeFormCreate);
  };
  const handleEdit = (idArea) => {
    setSelectedIdArea(idArea);
    setActiveFormEdit(!activeFormEdit);
  };
  const handleDelete = (idArea) => {
    setSelectedIdArea(idArea);
    setActiveFormDelete(!activeFormDelete);
  };
  
  const loadAreas = async () => {
    try {
      const res = await getAllAreasActivos();
      setAreas(res);
    } catch (error) {
        toast.error(`Error al cargar las Areas`);
    }
  };

  useEffect(() => {
    loadAreas();
  }, []);

  return (
    <>   
    <div className='contenedor-componente'>
      <div className='contenedor-titulo'>
        <h2>Areas</h2>
        <div className='btn-agregar' onClick={toggle}>
          <BsFillPlusCircleFill
            color='green'
            className="icon-add"
          />
          <h3>Agregar</h3>
        </div>
      </div>
      {/* Tabla de areas */}
      <Table
        nombreID={'arecod'}
        columns={areaColumns()}
        data={areas}
        nombre={'areas'}
        onEdit={handleEdit}
        onDelete={handleDelete}
        clickableRows={false}
      />         
    </div>

    <ToastContainer />

    {/* Modal para crear nuevo proyecto */}
    <Modal active={activeFormCreate} toggle={toggle}>
      <FormCreateArea toggle={toggle} loadAreas={loadAreas}/>      
    </Modal>
    
    {/* Modal para editar proyecto existente */}
    <Modal active={activeFormEdit} toggle={handleEdit}>
      <FormEditArea toggle={handleEdit} areaId={selectedIdArea} loadAreas={loadAreas} />      
    </Modal>

    {/* Modal para eliminar proyecto */}
    <Modal active={activeFormDelete} toggle={handleDelete}>
      <FormDeleteArea toggle={handleDelete} areaId={selectedIdArea} loadAreas={loadAreas} /> 
    </Modal>
    </>
  )
}

export default Areas;
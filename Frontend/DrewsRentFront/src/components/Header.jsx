import React from 'react'
import '../styles/Header.css'

import drewsLogo from '/src/assets/Logo/logo.png'

function Header (){
  return (
    <div className='header-bar'>
      <img src={drewsLogo} alt="logo DREWS" id='img-logo'/>
      <h1>Gestión Administrativa Drews Rent</h1>
    </div>
  )
}

export default Header

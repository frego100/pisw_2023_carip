import { getEstRegCodByDes } from "./EstadoRegistro.api";

import useAxios from "../Utils/useAxios";

const VehApi = useAxios("vehiculo/");

export const getVehiculo = (vehiculoId) => {
  return VehApi.get(`${vehiculoId}`);
};

export const getVehiculoById = (id) => {
  const res = VehApi.get(`/${id}`);
  return res;
};

export const getAllVehiculosActivos = async () => {
  const estregId = await getEstRegCodByDes("Activo");
  const response = await VehApi.get(`/est_reg/${estregId}`);
  return response.data;
};

export const getAllVehiculos = async () => {
  const res = await VehApi.get("/");
  return res.data;
};

export const getAllVehiculo = async () => {
  const res = await VehApi.get("/");
  return res.data;
};

export const getPlacaVehiculo = async (id) => {
  const response = await getVehiculo(id);
  const placa = response.data.vehpla;
  return placa;
};

export const getAllPlacaVehiculo = async () => {
  const response = await VehApi.get();
  const placas = response.data;
  const placaVehiculo = placas.map((placa) => placa.vehpla);
  return placaVehiculo;
};

export const getCountVehiculo = async () => {
  const res = await VehApi.get("/");
  const vehioculos = res.data;
  const cantidad = vehioculos.length;
  return cantidad;
};

/****************************************/
export const createVehiculo = (vehiculo) => {
  return VehApi.post("/create/", vehiculo);
};

export const updateVehiculo = (id, vehiculo) => {
  return VehApi.put(`/${id}/update/`, vehiculo);
};

export const deleteVehiculo = (id) => {
  return VehApi.delete(`/${id}/delete/`);
};

import useAxios from "../Utils/useAxios";

const VehPorEstApi = useAxios("vehiculos-por-estado/");


export const getVehiculoPorEstado = async () => {
    const res = await VehPorEstApi.get('/');
    return res.data;
}

export const getAllVehiculoPorEstado = async () => {

    const res = await VehPorEstApi.get('/');
    return res.data;
};
export const getAllVehiculosPorEstado2 = async  () => {
    try {
        const response = await VehPorEstApi.get('/');
        return response.data;
    } catch (error) {
        console.error('Error:', error);
        throw error; // Re-throw the error to propagate it to the next catch block if needed
    }
}



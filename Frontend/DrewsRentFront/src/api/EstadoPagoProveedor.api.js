import useAxios from "../Utils/useAxios";

const EstPagProApi = useAxios('estado-pago-proveedor/');

export const getAllEstadoPagPro = async () => {
    const response = await EstPagProApi.get('/');
    return response.data;
}

export const getEstadoPagoProveedor = async (val) => {
    try {
        const response = await EstPagProApi.get('/');
        const data = response.data;

        const estadoEncontrado = data.find(estado => estado.estpagprodes === val);

        if (estadoEncontrado) {
            return estadoEncontrado.estpagprocod;
        } else {
            return null; 
        }
    } catch (error) {
        console.error('Error al obtener el estado del registro del servidor:', error);
        throw error; 
    }
};
import useAxios from "../Utils/useAxios";

const LeyAlqApi = useAxios('alquiler-leyenda-conteo/');


export const getAllConteoTareoAlquiler = async () => {
    const res = await LeyAlqApi.get('/')
    return res.data;
}

export const getByIdConteoTareoAlquiler = async (alquilerId) => {
    const res = await LeyAlqApi.get(`${alquilerId}`)
    return res.data;
}

export const getAllConteoTareoAlquilerPaginated = async () => {
    try{
        let buffer = []

        const res = await LeyAlqApi.get('/')    
        let index = res.data;

        while(index.next){
            const data = index.results;
            buffer = [...buffer, ...data];
            const tmp = await LeyAlqApi.get(index.next, LeyAlqApi);
            index = tmp.data;
        }
        if(index.results){
            buffer = [...buffer, index.results];
        }
        // console.log("tOTAL LEYENDA TAREOS",buffer.length);
        return buffer;
    }catch(err){
        console.log(err);
    }
}





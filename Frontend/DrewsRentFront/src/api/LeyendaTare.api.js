import useAxios from "../Utils/useAxios";

const LeyTar = useAxios('leyeda-tareo/');


export const getAllLeyendas = async () => {
    const response = await LeyTar.get('/');
    return response.data;
}

import useAxios from "../Utils/useAxios";

const ProvinciaApi = useAxios("provincia/");


export const getAllProvincias = async () => {
    const res = await ProvinciaApi.get('/');
    return res.data;
}

export const getAllNamesProvincias = async (id) => {
    const response = await ProvinciaApi.get();
    const nombres = response.data;
    const nombreProvincia = nombres.map((nombre) => nombre.pronom);
    return nombreProvincia;
}
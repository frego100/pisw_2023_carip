import useAxios from "../Utils/useAxios";


const ValApi = useAxios('valorizacion/');

export const getValorizacion = (id) => {
    return ValApi.get(`${id}`)
}

export const getAllValorizaciones = async () => {
    const response = await ValApi.get('/');
    return response.data;
}

export const getCountValorizacion = async () => {
    const res = await ValApi.get('/');
    const valorizaciones = res.data;
    const cantidad = valorizaciones.length;
    return cantidad;
}

export const createValorizacion = (valorizacion) => {
    return ValApi.post('/create/', valorizacion);
}

export const updateValorizacion = (id, valorizacion) => {
    console.log(id);
    console.log(valorizacion);
    return ValApi.put(`/${id}/update/`, valorizacion);
}

export const deleteValorizacion = (id) => {
    return ValApi.delete(`/${id}/delete/`);
}
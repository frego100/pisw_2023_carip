import useAxios from "../Utils/useAxios";

const ExportTareoApi = useAxios('tareo/');


export const exportarTareos = async (data) => {
    try {
      const response = await ExportTareoApi.post('/export/', data, { responseType: 'blob' });
      return response.data;
    } catch (error) {
      throw new Error(`Error al exportar tareos: ${error.message}`);
    }
  };
  
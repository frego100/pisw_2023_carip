import { getEstRegCodByDes } from "./EstadoRegistro.api";

import useAxios from "../Utils/useAxios";

const EmpApi = useAxios('empresa/');


export const getEmpresa = (empresaId) => {
    return EmpApi.get(`${empresaId}`);
}

export const getEmpresaById = (id) => {
    const res = EmpApi.get(`/${id}`);
    return res;
}

export const getAllEmpresasActivos = async () => {
    const estregId = await getEstRegCodByDes('Activo');
    const response = await EmpApi.get(`/est_reg/${estregId}`);
    return response.data;
}

export const getAllEmpresas = async () => {
    const response = await EmpApi.get('/');
    return response.data;
}

export const getNameArea = async (id) => {
    const response = await getEmpresa(id);
    const name = response.data.empnom;
    return name;
}

export const getAllNamesEmpresas = async (id) => {
    const response = await EmpApi.get();
    const nombres = response.data;
    const nombreEmpresa = nombres.map((nombre) => nombre.empnom);
    return nombreEmpresa;
};

export const createEmpresa = (empresa) => {
    return EmpApi.post('/create/', empresa);
}

export const updateEmpresa = (id, empresa) => {
    console.log(id);
    console.log(empresa);
    return EmpApi.put(`/${id}/update/`, empresa);
}

export const deleteEmpresa = (id) => {
    return EmpApi.delete(`/${id}/delete/`);
}

export const getCountEmpresa = async () => {
    const res = await EmpApi.get('/');
    const empresas = res.data;
    const cantidad = empresas.length;
    return cantidad;
}
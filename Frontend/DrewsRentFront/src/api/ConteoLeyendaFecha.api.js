import useAxios from "../Utils/useAxios";

const LeyFechApi = useAxios('conteo-leyenda/');


export const getAllConteoTareoFecha = async () => {
    const res = await LeyFechApi.get('/')
    return res.data;
}

export const getAllConteoTareoFechaPaginated = async () => {
    try{
        let buffer = []


        const res = await LeyFechApi.get('/');    
        let index = res.data;

        while(index.next){
            const data = index.results;
            buffer = [...buffer, ...data];
            const tmp = await LeyFechApi.get(index.next, LeyFechApi);
            index = tmp.data;
        }
        if(index.results){
            buffer = [...buffer, index.results];
        }
        // console.log("tOTAL LEYENDA TAREOS",buffer.length);
        return buffer;
    }catch(err){
        console.log(err);
    }
}

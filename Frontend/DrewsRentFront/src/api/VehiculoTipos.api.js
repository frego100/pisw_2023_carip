import useAxios from "../Utils/useAxios";

const RegionApi = useAxios("vehiculo-tipo/");

export const getAllVehiculoTipos = async () => {
    const res = await RegionApi.get("/");
    return res.data;
};
import useAxios from "../Utils/useAxios";

const TarApi = useAxios('tareo/');

export const getAllTareo = async () => {
    const res = await TarApi.get('/')
    return res.data;
}

export const getTareo = async (id) => {
    const res = await TarApi.get(`/${id}`);
    return res.data;
}
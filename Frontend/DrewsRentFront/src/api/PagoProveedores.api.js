import useAxios from "../Utils/useAxios";

const PagoProveedor = useAxios("pago-proveedor/");

export const deletePago = async (id) => {
    const response = await PagoProveedor.delete(`/${id}/delete/`);
    return response.data;
}

export const updatePago = async (id, pagoPro) => {
    try {
        const response = await PagoProveedor.put(`/${id}/update/`, pagoPro);
        console.error('EDICION FINAL API:', id);
        const data = response.data;
        return data;
    } catch (error) {
        console.error('Error en editar Pago:', error);
        throw error;
    }
}

export const getByIdPago =  async (id) => {
    try {
        const response = await PagoProveedor.get(`${id}`);
        const data = response.data;
        return data;
    } catch (error) {
        console.error('Error en traer Pago:', error);
        console.error('Error en id', id);
        throw error;
    }
}

export const getAllPagos = async () => {
    const response = await PagoProveedor.get('/');
    return response.data;
}

export const createPago = async (pago) => {
    try {
        const response = await PagoProveedor.post('/create/', pago);
        return response.data;
    } catch (error) {
        console.error('Error en createPago:', error.response.data);
    }
}
import useAxios from "../Utils/useAxios";

const RegionApi = useAxios('region/');

export const getAllRegiones = async () => {
    const res = await RegionApi.get('/');
    return res.data;
}

export const getAllNamesRegiones = async (id) => {
    const response = await RegionApi.get();
    const nombres = response.data;
    const nombreRegion = nombres.map((nombre) => nombre.regnom);
    return nombreRegion;
}

export const getAllRegionNames = async () => {
    const response = await RegionApi.get();
    const region = response.data;
    const regionNames = region.map((region) => region.regnom);
    return regionNames;
};
import { getEstRegCodByDes } from "./EstadoRegistro.api";

import useAxios from "../Utils/useAxios";

const AreApi = useAxios('area/');


export const getArea = (areaId) => {
    return AreApi.get(`${areaId}`);
}

export const getAllAreasActivos = async () => {
    const estregId = await getEstRegCodByDes('Activo');
    const response = await AreApi.get(`/est_reg/${estregId}`);
    return response.data;
}

export const getAllAreas = async () => {
    const response = await AreApi.get('/');
    return response.data;
}

export const getNameArea = async (id) => {
    const response = await getArea(id);
    const name = response.data.arenom;
    return name;
}

export const getAllAreaNames = async () => {
    const response = await AreApi.get();
    const areas = response.data;
    const areaNames = areas.map((area) => area.arenom);
    return areaNames;
}

export const createArea = (area) => {
    return AreApi.post('/create/', area);
}

export const updateArea = (id, area) => {
    return AreApi.put(`/${id}/update/`, area);
}

export const deleteArea = (id) => {
    return AreApi.delete(`/${id}/delete/`);
}

export const getCountArea = async () => {
    const res = await AreApi.get('/');
    const areas = res.data;
    const cantidad = areas.length;
    return cantidad;
}
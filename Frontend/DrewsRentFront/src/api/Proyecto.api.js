import { toast } from "react-toastify";
import { getEstRegCodByDes } from "./EstadoRegistro.api";

import useAxios from "../Utils/useAxios";

const ProApi = useAxios("proyecto/");

export const getProyecto = (proyectoId) => {
  return ProApi.get(`${proyectoId}`);
}

export const getAllProyectosActivos = async () => {
  const estregId = await getEstRegCodByDes('Activo');
  const response = await ProApi.get(`/est_reg/${estregId}`);
  return response.data;
}

export const getAllProyectos = async () => {
  try {
    const response = await ProApi.get('/');
    return response.data;
  } catch (error) {
    if (error.response) {
      const status = error.response.status;
      if (status === 401) {
        return toast.error('No autorizado para acceder a los proyectos');
      } else {
        return toast.error(`Error en la respuesta del servidor [Status ${status}]: ${error.response.data}`);
      }
    } else if (error.request) {
      return toast.error('No se recibió respuesta del servidor');
    } else {
      return toast.error(`Error al recuperar proyectos: ${error.message || error}`);
    }
  }
}

export const getNameProyecto = async (id) => {
  const response = await getProyecto(id);
  const name = response.data.pronom;
  return name;
}

export const getAllProjectNames = async () => {
  const response = await ProApi.get();
  const projects = response.data;
  const projectNames = projects.map((project) => project.pronom);
  return projectNames;
}

export const createProyecto = (proyecto) => {
  return ProApi.post('/create/', proyecto);
}

export const updateProyecto = (id, proyecto) => {
    return ProApi.put(`/${id}/update/`, proyecto);
}

export const deleteProyecto = (id) => {
  return ProApi.delete(`/${id}/delete/`);
}

export const getCountProyecto = async () => {
    const res = await ProApi.get('/');
    const proyectos = res.data;
    const cantidad = proyectos.length;
    return cantidad;
}
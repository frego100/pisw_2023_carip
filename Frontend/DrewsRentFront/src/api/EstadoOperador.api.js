import useAxios from "../Utils/useAxios";

const EstOpe = useAxios('estado-operador/');


export const getAllEstadosOperador = async () => {
    const res = await EstOpe.get('/');
    return res.data;
}

/*export const getAllEstadoOperador = async () => {
    const response = await EstOpe.get();
    const estados = response.data;
    const estadosNames = estados.map((estado) => estado.estado_operador);
    return estadosNames;
}*/
import { getEstRegCodByDes } from "./EstadoRegistro.api";

import useAxios from "../Utils/useAxios";

const ProApi = useAxios("proveedor/");


export const getProveedor = (id) => {
    return ProApi.get(`${id}`)
}

export const getProveedorById = (id) => {
    const res = ProApi.get(`/${id}`);
    return res;
}

export const getAllProveedoresActivos = async () => {
    const estregId = await getEstRegCodByDes('Activo');
    const response = await ProApi.get(`/est_reg/${estregId}`);
    return response.data;
}  

export const getCountProveedor = async () => {
    const res = await ProApi.get('/');
    const proveedores = res.data;
    const cantidad = proveedores.length;
    return cantidad;
}

export const createProveedor = (proveedor) => {
    return ProApi.post('/create/', proveedor);
}

export const updateProveedor = (id, proveedor) => {
    console.log(id);
    console.log(proveedor);
    return ProApi.put(`/${id}/update/`, proveedor);
}

export const deleteProveedor = (id) => {
    return ProApi.delete(`/${id}/delete/`);
}
export const getAllProveedores = async () => {
    try {
        const response = await ProApi.get('/');
        return response.data;
    } catch (error) {
        console.log('getAllProveedores API failed',error);
    }
};

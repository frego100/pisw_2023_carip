import useAxios from "../Utils/useAxios";

const CargoApi = useAxios('cargo/');


export const getAllCargos = async () => {
    const res = await CargoApi.get('/');
    return res.data;
}

export const getAllNamesCargos = async (id) => {
    const response = await CargoApi.get();
    const nombres = response.data;
    const nombreProvincia = nombres.map((nombre) => nombre.carnom);
    return nombreProvincia;
}
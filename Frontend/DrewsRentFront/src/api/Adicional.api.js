import useAxios from "../Utils/useAxios";

const Adicional = useAxios('adicional/');


export const getAllAdicional = async () => {
    const response = await Adicional.get('/');
    return response.data;
}

export const createAdicional = (adicion) => {
    return Adicional.post('/create/', adicion);
}
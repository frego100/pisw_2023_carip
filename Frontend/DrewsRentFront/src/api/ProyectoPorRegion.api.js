import useAxios from "../Utils/useAxios";

const ProPorRegApi = useAxios("proyectos-por-region/");

export const getProyectoPorRegion = async () => {
  const res = await ProPorRegApi.get("/");
  return res.data;
};

import useAxios from "../Utils/useAxios";

const EstAlqApi = useAxios('estado-alquiler/');


export const getAllEstadosAlquiler = async () => {
  const res = await EstAlqApi.get('/')
  return res.data;
}
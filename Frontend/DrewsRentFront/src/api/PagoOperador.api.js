import useAxios from "../Utils/useAxios";

const PagOpeApi = useAxios("pago-operador/");


export const getPagoOperador = (pagopeId) => {
    return PagOpeApi.get(`${pagopeId}`);
}


export const getAllPagosOperadores = async () => {
    const response = await PagOpeApi.get('/');
    return response.data;
}

export const createPagoOperador = (pagoOperador) => {
    return PagOpeApi.post('/create/', pagoOperador);
}

export const updatePagoOperador = (id, pagoOperador) => {
    return PagOpeApi.put(`/${id}/update/`, pagoOperador);
}

export const deletePagoOperador = (id) => {
    return PagOpeApi.delete(`/${id}/delete/`);
}

export const getCountPagoOperador = async () => {
    const res = await PagOpeApi.get('/');
    const pagos = res.data;
    const cantidad = pagos.length;
    return cantidad;
}
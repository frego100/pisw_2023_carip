import useAxios from "../Utils/useAxios";

const VisTarApi = useAxios('tareo-lista/');


export const getAllListaTareo = async () => {
    const res = await VisTarApi.get('/')
    return res.data;
}

export const getByIdListaTareo = async (alquilerId) => {
    const res = await VisTarApi.get(`por_alquiler/${alquilerId}`)
    return res.data;
}

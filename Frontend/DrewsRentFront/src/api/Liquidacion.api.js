import useAxios from "../Utils/useAxios";

const Liquidacion = useAxios('liquidacion/');


export const getAllLiquidacion = async () => {
    const response = await Liquidacion.get('/');
    return response.data;
}

export const createLiquidacion = (liquida) => {
    return Liquidacion.post('/create/', liquida);
}
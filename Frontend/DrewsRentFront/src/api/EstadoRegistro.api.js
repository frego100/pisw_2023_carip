import useAxios from "../Utils/useAxios";

const EstRegApi = useAxios('estado-registro/');


export const getAllEstadoRegistro = async () => {
    const res = await EstRegApi.get('/');
    return res.data;
}

export const getEstRegCodByDes = async (des) => {
    const response = await EstRegApi.get('/');
    const data = response.data;
    const estadoEncontrado = data.find(estado => estado.estregdes === des);
    const estregcod = estadoEncontrado.estregcod;
    return estregcod;
}

export const getEstadoRegistro = async (val) => {
    try {
        const response = await EstRegApi.get('/');
        const data = response.data;

        const estadoEncontrado = data.find(estado => estado.estregdes === val);

        if (estadoEncontrado) {
            return estadoEncontrado.estregcod;
        } else {
            return null; 
        }
    } catch (error) {
        console.error('Error al obtener el estado del registro del servidor:', error);
        throw error; 
    }
};
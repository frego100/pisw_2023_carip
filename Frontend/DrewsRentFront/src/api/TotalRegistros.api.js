import useAxios from "../Utils/useAxios";

const TotRegApi = useAxios('total-registros/');

export const getAllTotalRegistro = async () => {

  try {
    const res = await TotRegApi.get('/');
    return res.data;
  } catch (error) {
    console.error("Error al traer registro:", error);
    throw error;
  }
};

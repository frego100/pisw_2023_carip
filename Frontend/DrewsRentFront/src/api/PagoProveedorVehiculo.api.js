import useAxios from "../Utils/useAxios";

const PagoProveedorVehiculo = useAxios("pago-proveedor-vehiculo/");

export const deletePagoV = async (id) => {
    const response = await PagoProveedorVehiculo.delete(`/${id}/delete/`);
    return response.data;
}

export const updatePagoV = async (id, pagoPro) => {
    try {
        const response = await PagoProveedorVehiculo.put(`/${id}/update/`, pagoPro);
        console.error('EDICION FINAL API:', id);
        const data = response.data;
        return data;
    } catch (error) {
        console.error('Error en editar Pago:', error);
        throw error;
    }
}

export const getByIdPagoV =  async (id) => {
    try {
        const response = await PagoProveedorVehiculo.get(`${id}`);
        const data = response.data;
        return data;
    } catch (error) {
        console.error('Error en traer Pago:', error);
        console.error('Error en id', id);
        throw error;
    }
}

export const getAllPagosV = async () => {
    const response = await PagoProveedorVehiculo.get('/');
    return response.data;
}

export const createPagoV = async (pago) => {
    try {
        const response = await PagoProveedorVehiculo.post('/create/', pago);
        return response.data;
    } catch (error) {
        console.error('Error en createPago:', error.response.data);
    }
}
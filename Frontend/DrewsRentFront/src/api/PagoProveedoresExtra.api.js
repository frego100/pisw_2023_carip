import useAxios from "../Utils/useAxios";

const PagoProveedorExtra = useAxios("pago-proveedor-extra/");

export const deletePagoE = async (id) => {
    const response = await PagoProveedorExtra.delete(`/${id}/delete/`);
    return response.data;
}

export const updatePagoE = async (id, pagoPro) => {
    try {
        const response = await PagoProveedorExtra.put(`/${id}/update/`, pagoPro);
        console.error('EDICION FINAL API:', id);
        const data = response.data;
        return data;
    } catch (error) {
        console.error('Error en editar Pago:', error);
        throw error;
    }
}

export const getByIdPagoE =  async (id) => {
    try {
        const response = await PagoProveedorExtra.get(`${id}`);
        const data = response.data;
        return data;
    } catch (error) {
        console.error('Error en traer Pago:', error);
        console.error('Error en id', id);
        throw error;
    }
}

export const getAllPagosE = async () => {
    const response = await PagoProveedorExtra.get('/');
    return response.data;
}

export const createPagoE = async (pago) => {
    try {
        const response = await PagoProveedorExtra.post('/create/', pago);
        return response.data;
    } catch (error) {
        console.error('Error en createPago:', error.response.data);
    }
}
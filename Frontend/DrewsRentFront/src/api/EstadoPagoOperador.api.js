import useAxios from "../Utils/useAxios";

const EstPagOpeApi = useAxios('estado-pago-operador/');


export const getEstadoPagOpe = (id) => {
    return EstPagOpeApi.get(`${id}`)
}

export const getAllEstadoPagOpe = async () => {
    const response = await EstPagOpeApi.get('/');
    return response.data;
}

export const getCountEstadoPagOpe = async () => {
    const res = await EstPagOpeApi.get('/');
    const estados = res.data;
    const cantidad = estados.length;
    return cantidad;
}
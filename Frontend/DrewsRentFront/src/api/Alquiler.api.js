import useAxios from "../Utils/useAxios";

const AlqApi = useAxios('alquiler/');


export const getAllAlquileres = async () => {
    const res = await AlqApi.get('/')
    return res.data;
}

export const getAllAlquiler = () => {
    const res = AlqApi.get('/')
    return res;
}

export const getAlquilerById = (id) => {
  const res = AlqApi.get(`/${id}`);
  return res;
}

export const getCountAlquiler = async () => {
  const res = await AlqApi.get('/');
  const alquileres = res.data;
  const cantidad = alquileres.length;
  return cantidad;
}

export const createAlquiler = (alquiler) => {
  return AlqApi.post('/create/', alquiler);
}

export const updateAlquiler = (id, alquiler) => {
  return AlqApi.put(`/${id}/update/`, alquiler);
}
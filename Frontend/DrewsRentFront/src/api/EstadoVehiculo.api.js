import useAxios from "../Utils/useAxios";

const EstVeh = useAxios('estado-vehiculo/');


export const getEstadoVehiculo = (id) => {
    return EstVeh.get(`${id}`)
}

export const getAllEstadoVeh = async () => {
    const response = await EstVeh.get('/');
    return response.data;
}

export const getCountEstadoPagOpe = async () => {
    const res = await EstVeh.get('/');
    const estados = res.data;
    const cantidad = estados.length;
    return cantidad;
}
import useAxios from "../Utils/useAxios";

const ValApi = useAxios("estado-valorizacion/");

export const getAllEstadoValorizaciones = async () => {
  const response = await ValApi.get("/");
  return response.data;
};

import { getEstRegCodByDes } from "./EstadoRegistro.api";
import useAxios from "../Utils/useAxios";

const ResApi = useAxios('responsable/');

export const getResponsable = (responsableId) => {
    return ResApi.get(`${responsableId}`)
}

export const getResponsableById = (id) => {
    const res = ResApi.get(`/${id}`);
    return res;
}

export const getAllResponsablesActivos = async () => {
    const estregId = await getEstRegCodByDes('Activo');
    const response = await ResApi.get(`/est_reg/${estregId}`);
    return response.data;
}

export const getAllResponsables = async () => {
    const res = await ResApi.get('/')
    return res.data;
}

export const getNameResponsable = async (respId) => {
    const response = await getResponsable(respId);
    const respName = response.data.resnom + " " + response.data.resape;
    return respName;
};

export const getAllNamesResponsable = async () => {
    const response = await ResApi.get();
    const nombres = response.data;
    const nombresResponsable = nombres.map((nombre) => {
        const fullName = `${nombre.resnom} ${nombre.resape}`;
        return fullName;
    });
    return nombresResponsable;
};

export const createResponsable = (responsable) => {
    return ResApi.post('/create/', responsable);
}

export const updateResponsable = (id, responsable) => {
    return ResApi.put(`/${id}/update/`, responsable);
}

export const deleteResponsable = (id) => {
    return ResApi.delete(`/${id}/delete/`);
}

export const getCountResponsable = async () => {
    const res = await ResApi.get('/');
    const responsable = res.data;
    const cantidad = responsable.length;
    return cantidad;
}
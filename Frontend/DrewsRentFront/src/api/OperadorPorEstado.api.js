import useAxios from "../Utils/useAxios";

const OpePorEstApi = useAxios('operadores-por-estado/');

export const getOperadoresPorEstado = async () => {
    const res = await OpePorEstApi.get('/')
    return res.data;
}

import useAxios from "../Utils/useAxios";

const Descuentos = useAxios('descuento/');



export const getAllDescuento = async () => {
    const response = await Descuentos.get('/');
    return response.data;
}

export const createDescuento = (descue) => {
    return Descuentos.post('/create/', descue);
}
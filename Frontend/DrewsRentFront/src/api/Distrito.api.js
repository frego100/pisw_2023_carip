import useAxios from "../Utils/useAxios";

const DisApi = useAxios('distrito/');


export const getDistrito = (distritoId) => {
    return DisApi.get(`${distritoId}`);
}

export const getAllDistritos = async () => {
    const response = await DisApi.get('/');
    return response.data;
}

export const getNameDistrito = async (id) => {
    const response = await getDistrito(id);
    const name = response.data.disnom;
    return name;
}

export const getAllDistritoNames = async () => {
    const response = await DisApi.get();
    const distritos = response.data;
    const distritoNames = distritos.map((distrito) => distrito.disnom);
    return distritoNames;
}

export const createDistrito = (distrito) => {
    return DisApi.post('/create/', distrito);
}

export const updateDistrito = (id, distrito) => {
    return DisApi.put(`/${id}/update/`, distrito);
}

export const deleteDistrito = (id) => {
    return DisApi.delete(`/${id}/delete/`);
}

export const getCountDistrito = async () => {
    const res = await DisApi.get('/');
    const distritos = res.data;
    const cantidad = distritos.length;
    return cantidad;
}
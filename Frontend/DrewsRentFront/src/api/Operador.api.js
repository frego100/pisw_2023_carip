import { getEstRegCodByDes } from "./EstadoRegistro.api";

import useAxios from "../Utils/useAxios";

const OpeApi = useAxios('operador/');


export const getAllOperadores = async () => {
    const response = await OpeApi.get('/');
    return response.data;
}
export const getOperadorById = (id) => {
    const res =  OpeApi.get(`/${id}`);
    return res;
}

export const getAllOperadoresActivos = async () => {
    const estregId = await getEstRegCodByDes('Activo');
    const response = await OpeApi.get(`/est_reg/${estregId}`);
    return response.data;
}

export const getAllOperadoresNames = async () => {
    const response = await OpeApi.get();
    const names = response.data;
    const operadoresNames = names.map((name) => {
        const fullName = `${name.openom} ${name.opeape}`;
        return fullName;
    });
    return operadoresNames;
};
export const getOperador = (operadorId) => {
    return OpeApi.get(`${operadorId}`)
}
export const getCountOperadores = async () => {
    const res = await OpeApi.get('/');
    const operadores = res.data;
    const cantidad = operadores.length;
    return cantidad;
}

export const createOperador = (operador) => {
    return OpeApi.post('/create/', operador);
}

export const updateOperador = (id, operador) => {
    console.log(id);
    console.log(operador);
    return OpeApi.put(`/${id}/update/`, operador);
}

export const deleteProyecto = (id) => {
    return OpeApi.delete(`/${id}/delete/`);
}

import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';
import { getColorByEstAlq } from './ColorsByEstAlq';


export default function alquilerColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Proyecto",
                accessor: "alqprocod.pronom",
                Filter: SelectColumnFilter,
                filter: 'includes',             
            },
            {
                Header: "Empresa",
                accessor: "alqrescod.resempcod.empnom",
            },
            {
                Header: "Area",
                accessor: "alqrescod.resarecod.arenom",
            },
            {
                Header: "Responsable",
                accessor: "alqrescod.resnom",
            },
            {
                Header: "Vehiculo",
                accessor: "alqvehcod.vehpla",
            },
            {
                Header: "Operador",
                accessor: "alqopecod.openom",
            },
            {
                Header: "Est. alquiler",
                accessor: "alqestalqcod.estalqdes",
                Filter: SelectColumnFilter,
                filter: 'includes',
                Cell: ({ value }) => (
                    <div className='cell-estado_vehiculo'>
                        <p style={getColorByEstAlq(value)}>{value}</p>
                    </div>
                ),
            },
            {
                Header: "F. Ingreso",
                accessor: "alqfecini",
            },
            {
                Header: "F. Salida",
                accessor: "alqfecfin",
            },            
        ],
        []
    );
    return columns;
}
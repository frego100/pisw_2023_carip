import { useMemo } from 'react'
import { getColorByEstadoVeh } from '../EstadoVehiculo/ColorsByEstVeh';

import '../../styles/Monitoreo.css'

export default function useColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Proyecto",
                accessor: "alqprocod.pronom"
            },
            {
                Header: "Empresa",
                accessor: "alqrescod.resempcod.empnom"
            },
            {
                Header: "Area",
                accessor: "responsable_rescod.empresa_has_area.arecod.arenom"
            },
            /*{
                Header: "Responsable",
                accessor: (row) => `${row.responsable_rescod.resnom} ${row.responsable_rescod.resape}`
            },*/
            {
                Header: "F. Ingreso",
                accessor: "alqfecini"
            },
            {
                Header: "F. Salida",
                accessor: "alqfecfin"
            },
            {
                Header: "Vehiculo",
                accessor: "alqvehcod.vehpla"
            },
            {
                Header: "Estado Vehiculo",
                accessor: "alqvehcod.vehestvehcod.estvehdes",
                Cell: ({ value }) => (
                    <div className='cell-estado_vehiculo'>
                        <p style={getColorByEstadoVeh(value)}>{value}</p>
                    </div>
                ),
            },
            {
                Header: "Operador",
                accessor: ""
            },
            {
                Header: "Estado Operador",
                accessor: "alqestalqcod.estalqdes"
            },
        ],
        []
    );

    return columns;
}

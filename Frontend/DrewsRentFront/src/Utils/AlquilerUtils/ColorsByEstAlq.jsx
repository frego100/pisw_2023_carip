export const getColorByEstAlq = (state) => {
    let color = '';
    let borderColor = '';
    let textColor = '';
  
    switch (state) {
      case 'Activo':
        color = '#C5FFC3';
        borderColor = '#009A43';
        textColor = '#009A43';
        break;
      case 'Inactivo':
        color = '#E1E1E1';
        borderColor = '#555555';
        textColor = '#555555';
        break;
      case 'Cancelado':
        color = '#FFE0D5';
        borderColor = '#E04F42';
        textColor = '#E04F42';
        break;
      case 'Finalizado':
        color = '#D5E5FF';
        borderColor = '#427CE0';
        textColor = '#427CE0';
        break;
      case 'Reservado':
        color = '#F8D5FF';
        borderColor = '#A342E0';
        textColor = '#A342E0';
        break;
      case 'Eliminado':
        color = '#FFD5DF';
        borderColor = '#E0425A';
        textColor = '#E0425A';
        break;
      default:
        color = '#E1E1E1'; // Gris por defecto
        borderColor = '#555555';
        textColor = '#555555';
    }
  
    return {
      backgroundColor: color,
      border: `3px solid ${borderColor}`,
      color: textColor,
      fontSize: '12px',
      fontWeight: 'bold',
      width: '80%',
      maxWidth: '100px',
      height: '80%',
      borderRadius: '5px',
      justifyContent: 'center',
      verticalAlign: 'middle',
    };
  };
  
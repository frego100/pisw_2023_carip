export const getColorByEstadoVeh = (state) => {
  let color = '';
  let borderColor = '';
  let textColor = '';

  switch (state) {
    case 'DT':
      color = 'rgba(213, 229, 255, 1)';
      borderColor = 'rgba(107, 151, 228, 0.9)';
      textColor = 'rgba(107, 151, 228, 0.9)';
      break;
    case 'DNT':
      color = 'rgba(212, 212, 212, 1)';
      borderColor = 'rgba(162, 162, 162, 1)';
      textColor = 'rgba(85, 85, 85, 1)';
      break;
    case 'MP':
      color = 'rgba(255, 246, 218, 1)';
      borderColor = 'rgba(255, 193, 7, 1)';
      textColor = 'rgba(210, 163, 24, 1)';
      break;
    case 'MC':
      color = 'rgba(255, 228, 195, 1)';
      borderColor = 'rgba(255, 138, 0, 1)';
      textColor = 'rgba(255, 138, 0, 1)';
      break;
    case 'TSI':
      color = 'rgba(255, 234, 236, 1)'; 
      borderColor = 'rgba(220, 53, 69, 1)';
      textColor = 'rgba(220, 53, 69, 1)';
      break;
    default:
      color = 'rgba(128, 128, 128, 0.8)'; // Gris por defecto
      borderColor = 'gray';
      textColor = 'black';
  }

  return {
    backgroundColor: color,
    border: `3px solid ${borderColor}`,
    color: textColor,
    fontSize: '14px',
    fontWeight: 'bold',
    width: '80%',
    maxWidth: '100px',
    height: '80%',
    borderRadius: '5px',
    justifyContent: 'center',
    verticalAlign: 'middle',
  };
};

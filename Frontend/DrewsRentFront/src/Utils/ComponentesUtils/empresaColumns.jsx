import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function empresaColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "RUC",
                accessor: "empruc",                
            },
            {
                Header: "Razon Social",
                accessor: "emprazsoc",                
            },
            {
                Header: "Nombre",
                accessor: "empnom",                
            },
            {
                Header: "Region",
                accessor: "empubicod.disprocod.proregcod.regnom",
                Filter: SelectColumnFilter,
                filter: 'includes',                
            },
            {
                Header: "Provincia",
                accessor: "empubicod.disprocod.pronom",
                Filter: SelectColumnFilter,
                filter: 'includes',              
            },
            {
                Header: "Distrito",
                accessor: "empubicod.disnom",                
            },
        ],
        []
    );
    return columns;
}
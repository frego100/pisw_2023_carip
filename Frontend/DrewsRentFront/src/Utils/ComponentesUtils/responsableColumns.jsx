import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function responsableColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Nombres",
                accessor: "resnom",                
            },
            {
                Header: "Apellidos",
                accessor: "resape",                
            },
            {
                Header: "Cargo",
                accessor: "rescarcod.carnom",
            },
            {
                Header: "Telefono",
                accessor: "restel",                
            },
            {
                Header: "Empresa",
                accessor: "resempcod.empnom",
                Filter: SelectColumnFilter,
                filter: 'includes',                
            },
            {
                Header: "Area",
                accessor: "resarecod.arenom",            
            },
        ],
        []
    );
    return columns;
}
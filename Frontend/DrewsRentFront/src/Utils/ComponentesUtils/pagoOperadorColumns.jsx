import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function pagoOperadorColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Nombres y Apellidos",
                accessor: "pagopeopecod",
                Cell: ({ row }) => `${row.values.pagopeopecod.openom} ${row.values.pagopeopecod.opeape}`            
            },
            {
                Header: "Empresa",
                accessor: "pagopeemp",
                Filter: SelectColumnFilter,
                filter: 'includes',                   
            },
            {
                Header: "Cant. Dias",
                accessor: "pagopecandia",                
            },
            {
                Header: "Costo Dia",
                accessor: "pagopecosdia",
                Cell: ({ row }) => `S/.${row.values.pagopecosdia}`                 
            },
            {
                Header: "Fecha Inicio",
                accessor: "pagopeperini",                
            },
            {
                Header: "Fecha Fin",
                accessor: "pagopeperfin",                
            },
            {
                Header: "Neto Pago",
                accessor: "pagopenetpag",
                Cell: ({ row }) => `S/.${row.values.pagopenetpag}`
            },
            {
                Header: "Fecha Pago",
                accessor: "pagopefec",                
            },
            {
                Header: "Estado",
                accessor: "pagopeestpagope.estpagopedes",
                Filter: SelectColumnFilter,
                filter: 'includes',                
            },
        ],
        []
    );
    return columns;
}

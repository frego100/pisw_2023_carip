import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function pagosColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Razon social",
                accessor: "pagproprocod.prorazsoc",  
                Filter: SelectColumnFilter,
                filter: 'includes',              
            },
            {
                Header: "Fecha de pago",
                accessor: "pagprofec",
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
            {
                Header: "Total",
                accessor: "pagprototal",                
            },
            {
                Header: "Observacion",
                accessor: "pagproobs",                
            },
            {
                Header: "Estado",
                accessor: "pagproestpagpro.estpagprodes",
                Filter: SelectColumnFilter,
                filter: 'includes',
            },

        ],
        []
    );
    return columns;
}

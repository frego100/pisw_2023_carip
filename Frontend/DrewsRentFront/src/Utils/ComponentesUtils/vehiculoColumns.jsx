import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function vehiculoColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Placa",
                accessor: "vehpla",
            },
            {
                Header: "Tipo",
                accessor: "vehtip",
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
            {
                Header: "Marca",
                accessor: "vehmar",
            },
            {
                Header: "Modelo",
                accessor: "vehmod",
            },
            {
                Header: "FV SOAT",
                accessor: "vehfecvensoa",
            },
            {
                Header: "FR. Tecnica",
                accessor: "vehfecrevtec",
            },
            {
                Header: "Proveedor",
                accessor: "vehprocod.prorazsoc",
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
        ],
        []
    );
    return columns;
}

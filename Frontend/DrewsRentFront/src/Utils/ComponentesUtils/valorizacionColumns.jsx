import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';

export default function proveedorColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Responsable",
                accessor: "valrescod.resnom",
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
            {
                Header: "Estado de Valorizacion",
                accessor: "valestvalcod.estvaldes",
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
            {
                Header: "Cliente",
                accessor: "valcli",                
            },
            {
                Header: "RUC",
                accessor: "valruc",                
            },
            // {
            //     Header: "Direccion",
            //     accessor: "valdir",                
            // },
            {
                Header: "Telefono",
                accessor: "valtel",                
            },
            // {
            //     Header: "Email",
            //     accessor: "valema",                
            // },
            {
                Header: "Proyecto",
                accessor: "valpro",                
            },
            {
                Header: "Fecha",
                accessor: "valfec",                
            },
            {
                Header: "Forma de pago",
                accessor: "valforpag",                
            },
            // {
            //     Header: "Moneda",
            //     accessor: "valmon",                
            // },
            // {
            //     Header: "Facturable",
            //     accessor: "valfac",                
            // },
            {
                Header: "Valor de Venta",
                accessor: "valvenval",                
            },
            // {
            //     Header: "Valor Otros",
            //     accessor: "valotrval",                
            // },
            // {
            //     Header: "Valor IGV",
            //     accessor: "valigvval",                
            // },
            {
                Header: "Precio de Venta",
                accessor: "valpreven",                
            },
            
        ],
        [] // Esto se cambia por el estado que desea depender
    );
    return columns;
}
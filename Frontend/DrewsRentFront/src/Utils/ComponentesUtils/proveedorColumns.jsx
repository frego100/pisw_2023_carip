import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function proveedorColumns() {
    const columns = useMemo(
        () => [
            /*{
                Header: "Estado de Proveedor",
                accessor: "proestreg",
            },*/
            // {
            //     Header: "Razon Social",
            //     accessor: "prorazsoc",                
            // },
            // {
            //     Header: "Dirección",
            //     accessor: "prodir",                
            // },
            {
                Header: "Teléfono",
                accessor: "protel",                
            },
            {
                Header: "RUC",
                accessor: "proruc",                
            },
            {
                Header: "Razón Social",
                accessor: "prorazsoc",                
            },
            /*{
                Header: "Dirección",
                accessor: "prodir",                
            },*/
            /*{
                Header: "Email",
                accessor: "proema",                
            },*/            
            {
                Header: "Banco de Depósito",
                accessor: "probandep",                
            },
            {
                Header: "Tipo de Cuenta",
                accessor: "protipcuedep",                
            },
            {
                Header: "Nro. de Cuenta de Depósito",
                accessor: "pronumcuedep",                
            },
            {
                Header: "CCI depósito",
                accessor: "proccidep",                
            },
            /*{
                Header: "Banco determinado",
                accessor: "probandet",                
            },
            {
                Header: "Tipo de cuenta determinada",
                accessor: "protipcuedet",                
            },
            {
                Header: "Numero de cuenta determinada",
                accessor: "pronumcuedet",                
            },*/            
        ],
        [] // Esto se cambia por el estado que desea depender
    );
    return columns;
}

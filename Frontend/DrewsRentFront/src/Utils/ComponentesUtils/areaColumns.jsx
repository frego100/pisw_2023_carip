import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function areaColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Nombre",
                accessor: "arenom",                
            },
        ],
        []
    );
    return columns;
}

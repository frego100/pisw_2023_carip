import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function proyectoColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Nombre",
                accessor: "pronom",              
            },
            {
                Header: "Region",
                accessor: "proregcod.regnom",
                Filter: SelectColumnFilter,
                filter: 'includes',
            },
        ],
        []
    );
    return columns;
}

import { useMemo } from 'react';
import { SelectColumnFilter } from '../../components/Tabla/SelectColumnFilter';


export default function operadorColumns() {
    const columns = useMemo(
        () => [
            {
                Header: "Nombre",
                accessor: "openom",                
            },
            {
                Header: "Apellido",
                accessor: "opeape",                
            },
            {
                Header: "Telefono",
                accessor: "opetel",                
            },
            {
                Header: "Estado de Operador",
                accessor: "opeestopecod.estopedes",
            },
        ],
        []
    );
    return columns;
}
